<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');
    Route::resource('clientes', 'ClienteController');
    Route::resource('clienteInfos', 'ClienteInfosController');
    Route::resource('pensions', 'PensionsController');
    Route::resource('imgIngresos', 'ImgIngresosController');
    Route::resource('centralUsers', 'CentralUserController');
    Route::resource('ingresos', 'IngresosController');
    Route::resource('bancoColects', 'BancoColectController');
    Route::resource('empresaColects', 'EmpresaColectController');
    Route::resource('estacionamientos', 'EstacionamientosController');
    Route::resource('tickets', 'ticketController');
    Route::resource('facturas', 'facturasController');
    Route::get('/informe', 'InformeController@index')->name('informe');
    Route::post('/buscar/facturas', 'facturasController@busqueda')->name('busqueda');
    Route::post('/email', 'facturasController@enviarEmail')->name('email');
    Route::post('/emailespecial', 'facturasController@enviarEmailespecial')->name('emailespecial');
    Route::get('/xml/{id}', 'facturasController@downloadXML')->name('xml');
    Route::get('/pdf/{id}', 'facturasController@downloadPDF')->name('pdf');
    Route::resource('facturaPensions', 'FacturaPensionController');
    Route::resource('detalles', 'DetalleController');
    Route::get('/xmlPen/{id}', 'FacturaPensionController@downloadpenXML')->name('xmlPen');
    Route::get('/pdfPen/{id}', 'FacturaPensionController@downloadpenPdf')->name('pdfPen');
    //pensiones
    Route::get('/contp/{id}', 'DetalleController@downloadContPdf')->name('contp');
    Route::get('/scontp/{id}', 'DetalleController@downloadSContPdf')->name('scontp');
    Route::get('/ccontp/{id}', 'DetalleController@downloadComprobantedomPdf')->name('ccontp');
    Route::get('/ine/{id}', 'DetalleController@downloadIne')->name('ine');
    Route::get('/licencia/{id}', 'DetalleController@downloadLic')->name('licencia');
    Route::get('/tarjetaC/{id}', 'DetalleController@downloadTarC')->name('tarjetaC');
    Route::get('/compPago/{id}', 'DetalleController@downloadcompPago')->name('compPago');
    Route::resource('pagos', 'PagoController');
    Route::post('updatePago', 'PagoController@update');
    Route::get('/compPago/{id}', 'PagoController@downloadcompPago')->name('compPago');
    Route::get('/inactivo/{id}', 'DetalleController@inactivo')->name('inactivo');
    Route::get('/bajatemporal/{id}', 'DetalleController@temporal')->name('bajatemporal');
    Route::resource('reportes', 'ReporteController');
    Route::get('/reportpension', 'ReporteController@reportePen')->name('reportpension');
    Route::get('/pensionesActivas', 'PensionActivaController@index')->name('pensionesActivas');
    Route::resource('actualizacionesPen', 'ActualizaPenController');
    Route::post('/actualizacionesPenDel', 'ActualizaPEnController@destroy')->name('actualizacionesPenDel');
    Route::resource('historicoPen', 'HistoricoController');
    Route::get('/findBusquedaEspecifica/{id}', 'DetalleController@findBusquedaEspecifica')->name('findBusquedaEspecifica');
    //grupocarso
    Route::resource('facturacionespeciales', 'EspecialFacturaController');
    Route::resource('reportesespeciales', 'ReporteEspecialController');
    Route::post('/reportespecial', 'ReporteController@reporteEsp')->name('reportespecial');
});







Route::resource('catBancos', 'catBancoController');


Route::resource('catCfdiUsos', 'CatCfdiUsoController');

Route::resource('metodoPagos', 'MetodoPagoController');
