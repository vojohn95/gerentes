@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Pensiones</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-6">
                <button type="button" class="btn btn-default btn-lg">
                    Pensiones: <span class="counter badge badge-danger ml-4"></span>
                </button>
            </div>
            <!--<div class="col-3 text-right">
                <a type="button" class="btn btn-primary btn-rounded" href="">
                    Permisos
                </a>
            </div>-->
            <div class="col-3 text-right">
                <a type="button" class="btn btn-dark btn-rounded right-aligned" href="{!! route('pensions.create') !!}">
                    Agregar Pensionado
                </a>
            </div>
        </div>
        <br>
        @include('pensions.table')
    </section>
@endsection


