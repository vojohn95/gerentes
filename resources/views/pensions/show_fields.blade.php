<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $pensions->id !!}</p>
</div>

<!-- Id Cliente Field -->
<div class="form-group">
    {!! Form::label('id_cliente', 'Id Cliente:') !!}
    <p>{!! $pensions->id_cliente !!}</p>
</div>

<!-- Id Tipo Pen Field -->
<div class="form-group">
    {!! Form::label('id_tipo_pen', 'Id Tipo Pen:') !!}
    <p>{!! $pensions->id_tipo_pen !!}</p>
</div>

<!-- Id Tipo Pago Field -->
<div class="form-group">
    {!! Form::label('id_tipo_pago', 'Id Tipo Pago:') !!}
    <p>{!! $pensions->id_tipo_pago !!}</p>
</div>

<!-- Id Forma Pago Field -->
<div class="form-group">
    {!! Form::label('id_forma_pago', 'Id Forma Pago:') !!}
    <p>{!! $pensions->id_forma_pago !!}</p>
</div>

<!-- Id Fecha Limite Field -->
<div class="form-group">
    {!! Form::label('id_fecha_limite', 'Id Fecha Limite:') !!}
    <p>{!! $pensions->id_fecha_limite !!}</p>
</div>

<!-- Num Pen Field -->
<div class="form-group">
    {!! Form::label('num_pen', 'Num Pen:') !!}
    <p>{!! $pensions->num_pen !!}</p>
</div>

<!-- Foto Comprobante Field -->
<div class="form-group">
    {!! Form::label('foto_comprobante', 'Foto Comprobante:') !!}
    <p>{!! $pensions->foto_comprobante !!}</p>
</div>

<!-- No Est Field -->
<div class="form-group">
    {!! Form::label('no_est', 'No Est:') !!}
    <p>{!! $pensions->no_est !!}</p>
</div>

<!-- Factura Field -->
<div class="form-group">
    {!! Form::label('factura', 'Factura:') !!}
    <p>{!! $pensions->factura !!}</p>
</div>

<!-- Costo Pension Field -->
<div class="form-group">
    {!! Form::label('costo_pension', 'Costo Pension:') !!}
    <p>{!! $pensions->costo_pension !!}</p>
</div>

<!-- Recargos Field -->
<div class="form-group">
    {!! Form::label('recargos', 'Recargos:') !!}
    <p>{!! $pensions->recargos !!}</p>
</div>

<!-- Venta Tarjeta Field -->
<div class="form-group">
    {!! Form::label('venta_tarjeta', 'Venta Tarjeta:') !!}
    <p>{!! $pensions->venta_tarjeta !!}</p>
</div>

<!-- Repo Tarjeta Field -->
<div class="form-group">
    {!! Form::label('repo_tarjeta', 'Repo Tarjeta:') !!}
    <p>{!! $pensions->repo_tarjeta !!}</p>
</div>

<!-- Impor Pago Field -->
<div class="form-group">
    {!! Form::label('impor_pago', 'Impor Pago:') !!}
    <p>{!! $pensions->impor_pago !!}</p>
</div>

<!-- Mes Pago Field -->
<div class="form-group">
    {!! Form::label('mes_pago', 'Mes Pago:') !!}
    <p>{!! $pensions->mes_pago !!}</p>
</div>

<!-- Cargado Por Field -->
<div class="form-group">
    {!! Form::label('cargado_por', 'Cargado Por:') !!}
    <p>{!! $pensions->cargado_por !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $pensions->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $pensions->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $pensions->deleted_at !!}</p>
</div>

