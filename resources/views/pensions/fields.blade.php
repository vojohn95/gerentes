<!-- Id Cliente Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::hidden('id_cliente', null, ['class' => 'form-control']) !!}
</div>-->

<!-- Id Tipo Pen Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::hidden('id_tipo_pen', null, ['class' => 'form-control']) !!}
</div>-->

<!-- Id Tipo Pago Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::hidden('id_tipo_pago', null, ['class' => 'form-control']) !!}
</div>-->

<!-- Id Forma Pago Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::hidden('id_forma_pago', null, ['class' => 'form-control']) !!}
</div>-->

<!-- Id Fecha Limite Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::hidden('id_fecha_limite', null, ['class' => 'form-control']) !!}
</div>-->

<div class="md-form input-group">
    {!! Form::number('Numero_pension', null, ['min' => '1' ,'class' => 'form-control', 'placeholder' => 'Numero de pensión', 'id' => 'no_pension', 'required']) !!}
    {!! Form::number('Costo_Pension', null, ['class' => 'form-control', 'placeholder' => 'Costo/Pensión', 'id' => 'costo_pension', 'required']) !!}
    {!! Form::number('Recargos', null, ['min' => '0', 'class' => 'form-control', 'placeholder' => 'Recargos','id' => 'recargos', 'required']) !!}
    {!! Form::number('Numero_estacionamiento', null, ['min' => '1', 'class' => 'form-control', 'placeholder' => 'Numero estacionamiento','id' => 'no_estacionamiento', 'required']) !!}
</div>

<div class="md-form input-group">
    {!! Form::number('venta_tarjeta', null, ['class' => 'form-control', 'placeholder' => 'Venta de tarjeta', 'id' => 'venta_tarjeta']) !!}
    {!! Form::number('reposicion_tarjeta', null, ['min' => '0','class' => 'form-control', 'placeholder' => 'Reposición tarjeta', 'id' => 'reposicion_tarjeta']) !!}
    {!! Form::number('importe_pago', null, ['min' => '1', 'class' => 'form-control', 'placeholder' => 'Importe pago', 'id' => 'importe_pago']) !!}
</div>

<div class="md-form input-group">
    <select class="browser-default custom-select" id="t_pension" name="tipo_pension" required>
        <option selected>Tipo pensión</option>
        <option value="1">Locatario</option>
        <option value="2">Externo</option>
    </select>
    <select class="browser-default custom-select" id="t_pago" name="tipo_pago" required>
        <option selected>Tipo pago</option>
        <option value="1">Mensual</option>
        <option value="2">Trimestral</option>
        <option value="3">Semestral</option>
        <option value="4">Anual</option>
    </select>
    <select class="browser-default custom-select" id="f_pago" name="forma_pago" required>
        <option selected>Forma pago</option>
        <option value="1">Deposito</option>
        <option value="2">Transferencia</option>
    </select>
    <select class="browser-default custom-select" id="mes_pago" name="mes_pago">
        <option selected>Mes/Pago</option>
        <option value="1">Enero</option>
        <option value="2">Febrero</option>
        <option value="3">Marzo</option>
        <option value="4">Abril</option>
        <option value="5">Mayo</option>
        <option value="6">Junio</option>
        <option value="7">Julio</option>
        <option value="8">Agosto</option>
        <option value="9">Septiembre</option>
        <option value="10">Octubre</option>
        <option value="11">Noviembre</option>
        <option value="12">Diciembre</option>
    </select>
    <select class="browser-default custom-select" id="fecha_lpago" name="fecha_limite_pago" required>
        <option selected>Fecha limite de pago</option>
        <option value="5">dia 5 de cada mes</option>
        <option value="6">dia 6 de cada mes</option>
        <option value="7">dia 7 de cada mes</option>
        <option value="8">dia 8 de cada mes</option>
        <option value="9">dia 9 de cada mes</option>
        <option value="10">dia 10 de cada mes</option>
        <option value="11">dia 11 de cada mes</option>
        <option value="12">dia 12 de cada mes</option>
        <option value="13">dia 13 de cada mes</option>
        <option value="14">dia 14 de cada mes</option>
        <option value="15">dia 15 de cada mes</option>
        <option value="16">dia 16 de cada mes</option>
        <option value="17">dia 17 de cada mes</option>
        <option value="18">dia 18 de cada mes</option>
        <option value="19">dia 19 de cada mes</option>
        <option value="20">dia 20 de cada mes</option>
        <option value="21">dia 21 de cada mes</option>
        <option value="22">dia 22 de cada mes</option>
        <option value="23">dia 23 de cada mes</option>
        <option value="24">dia 24 de cada mes</option>
        <option value="25">dia 25 de cada mes</option>
        <option value="26">dia 26 de cada mes</option>
        <option value="27">dia 27 de cada mes</option>
        <option value="28">dia 28 de cada mes</option>
        <option value="29">dia 29 de cada mes</option>
        <option value="30">dia 30 de cada mes</option>
        <option value="31">dia 31 de cada mes</option>
    </select>
    <select class="browser-default custom-select" id="r_factura" name="requiere_factura">
        <option selected>Requiere factura</option>
        <option value="1">Si</option>
        <option value="0">No</option>
    </select>
</div>
<br>
<h3 class="text-center">Datos de cliente</h3>
<br>
<div class="md-form input-group">
    {!! Form::text('Nombre_pensionado', null, ['class' => 'form-control', 'id' => 'nombre_pensionado', 'placeholder' => 'Nombre pensionado']) !!}
    {!! Form::text('Rfc', null, ['class' => 'form-control', 'id' => 'rfc', 'placeholder' => 'RFC']) !!}
    {!! Form::number('Telefono', null, ['class' => 'form-control', 'id' => 'telefono', 'placeholder' => 'Teléfono', 'pattern' => '^[0-9]+']) !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Correo electronico']) !!}
    <!--<input type="number" aria-label="Costo pensión" min="1" class="form-control" id="costo_pen" name="costo_pen" placeholder="Costo Pensión">-->
</div>

<!-- Submit Field -->
<div class="md-form input-group">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary', 'onclick' => "return confirm('Are you sure?')"]) !!}
    <a href="{!! route('pensions.index') !!}" class="btn btn-default">Cancelar</a>
</div>

