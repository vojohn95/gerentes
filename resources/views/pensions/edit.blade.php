@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pensions
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($pensions, ['route' => ['pensions.update', $pensions->id], 'method' => 'patch']) !!}

                        @include('pensions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection