<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Que estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Numero Pensión</th>
            <th>No. Estacionamiento</th>
            <th>Costo Pensión</th>
            <th>Recargos</th>
            <th>Venta Tarjeta</th>
            <th>Reposición Tarjeta</th>
            <th>Importe Pago</th>
            <th>Mes Pago</th>
            <!--<th>Acción</th>-->
        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($pensions as $pensions)
            <tr>
                <td>{!! $pensions->num_pen !!}</td>
                <td>{!! $pensions->no_est !!}</td>
                <td>{!! $pensions->costo_pension !!}</td>
                <td>{!! $pensions->recargos !!}</td>
                <td>{!! $pensions->venta_tarjeta !!}</td>
                <td>{!! $pensions->repo_tarjeta !!}</td>
                <td>{!! $pensions->impor_pago !!}</td>
                <td>
                    @switch($pensions->mes_pago)
                        @case(1)
                        <span>Enero</span>
                        @break
                        @case(2)
                        <span>Febrero</span>
                        @break
                        @case(3)
                        <span>Marzo</span>
                        @break
                        @case(4)
                        <span>Abril</span>
                        @break
                        @case(5)
                        <span>Mayo</span>
                        @break
                        @case(6)
                        <span>Junio</span>
                        @break
                        @case(7)
                        <span>Julio</span>
                        @break
                        @case(8)
                        <span>Agosto</span>
                        @break
                        @case(9)
                        <span>Septiembre</span>
                        @break
                        @case(10)
                        <span>Octubre</span>
                        @break
                        @case(11)
                        <span>Noviembre</span>
                        @break
                        @case(12)
                        <span>Febrero</span>
                        @break
                        @default
                        <span style="color: red;">Error</span>
                    @endswitch
                </td>
                <!--<td>
                    {!! Form::open(['route' => ['pensions.destroy', $pensions->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('pensions.show', [$pensions->id]) !!}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('pensions.edit', [$pensions->id]) !!}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>-->
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<br>
<br>
