<div class="table-responsive">
    <table class="table" id="centralUsers-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Email</th>
        <th>Email Verified At</th>
        <th>Password</th>
        <th>Remember Token</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($centralUsers as $centralUser)
            <tr>
                <td>{!! $centralUser->name !!}</td>
            <td>{!! $centralUser->email !!}</td>
            <td>{!! $centralUser->email_verified_at !!}</td>
            <td>{!! $centralUser->password !!}</td>
            <td>{!! $centralUser->remember_token !!}</td>
                <td>
                    {!! Form::open(['route' => ['centralUsers.destroy', $centralUser->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('centralUsers.show', [$centralUser->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('centralUsers.edit', [$centralUser->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
