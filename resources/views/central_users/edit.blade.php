@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Central User
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($centralUser, ['route' => ['centralUsers.update', $centralUser->id], 'method' => 'patch']) !!}

                        @include('central_users.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection