@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <div class="view view-cascade gradient-card-header default-color">
                <h3 class="card-header-title">Reportes</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-12">
                <a href="{!! route('reportpension') !!}" class='btn btn-md btn-green'>Generar reporte de pensiones</a>
            </div>
        </div>
        <br>
    </section>
@endsection
