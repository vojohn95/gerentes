@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <div class="view view-cascade gradient-card-header default-color">
                <h3 class="card-header-title">Reportes</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <form method="POST" action="{{ route('reportespecial') }}" enctype="multipart/form-data">
                @csrf
                <div class="col-12 form-inline">
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="start">Fecha inicial:</label>
                        <input type="date" id="start" name="fechainicial" required>
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="start">Fecha final:</label>
                        <input type="date" id="start" name="fechafinal" required>
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <button type="submit" class='btn btn-md btn-green'>Generar reporte</button>
                    </div>
                </div>
            </form>
        </div>
        <br>
    </section>
@endsection
