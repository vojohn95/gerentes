@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <!-- Jumbotron -->
        <!-- Card -->
        <div class="card testimonial-card ">

            <!-- Background color -->
            <div class="card-up default-color"></div>


                <!-- Avatar -->
                <div class="avatar mx-auto white">
                    <img src="{{ asset('logo/Logo.jpg') }}" class="rounded-circle" alt="OCE">
                </div>

                <!-- Content -->
                <div class="card-body">
                    <!-- Name -->
                    <h4 class="card-title">OPERADORA CENTRAL DE ESTACIONAMIENTOS</h4>
                    <hr>
                    <!-- Quotation -->
                    <p style="font-family: 'Comic Sans MS'; border-radius: 20px;">¡Bienvenido {{ Auth::user()->name }}!
                    </p>
                </div>

        </div>
        <!-- Card -->
        <!-- Jumbotron -->
    </div>
@endsection
