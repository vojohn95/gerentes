<!-- Id Ticketaf Field -->
<div class="form-group">
    {!! Form::label('id_ticketAF', 'Id Ticketaf:') !!}
    <p>{!! $facturas->id_ticketAF !!}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{!! $facturas->serie !!}</p>
</div>

<!-- Tipo Doc Field -->
<div class="form-group">
    {!! Form::label('tipo_doc', 'Tipo Doc:') !!}
    <p>{!! $facturas->tipo_doc !!}</p>
</div>

<!-- Folio Field -->
<div class="form-group">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{!! $facturas->folio !!}</p>
</div>

<!-- Fecha Timbrado Field -->
<div class="form-group">
    {!! Form::label('fecha_timbrado', 'Fecha Timbrado:') !!}
    <p>{!! $facturas->fecha_timbrado !!}</p>
</div>

<!-- Uuid Field -->
<div class="form-group">
    {!! Form::label('uuid', 'Uuid:') !!}
    <p>{!! $facturas->uuid !!}</p>
</div>

<!-- Subtotal Factura Field -->
<div class="form-group">
    {!! Form::label('subtotal_factura', 'Subtotal Factura:') !!}
    <p>{!! $facturas->subtotal_factura !!}</p>
</div>

<!-- Iva Factura Field -->
<div class="form-group">
    {!! Form::label('iva_factura', 'Iva Factura:') !!}
    <p>{!! $facturas->iva_factura !!}</p>
</div>

<!-- Total Factura Field -->
<div class="form-group">
    {!! Form::label('total_factura', 'Total Factura:') !!}
    <p>{!! $facturas->total_factura !!}</p>
</div>

<!-- Xml Field -->
<div class="form-group">
    {!! Form::label('XML', 'Xml:') !!}
    <p>{!! $facturas->XML !!}</p>
</div>

<!-- Pdf Field -->
<div class="form-group">
    {!! Form::label('PDF', 'Pdf:') !!}
    <p>{!! $facturas->PDF !!}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{!! $facturas->estatus !!}</p>
</div>

