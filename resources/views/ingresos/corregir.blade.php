<div class="row">
    <div class="col-md-3 md-form">
        <i class="fas fa-hashtag prefix"></i>
        {!! Form::label('num', 'Número:') !!}
        {!! Form::number('num', $info[0]->no_est, ['class' => 'form-control', 'disabled']) !!}
    </div>
    <div class="col-md-9 md-form">
        <i class="fas fa-parking prefix"></i>
        {!! Form::label('nombre', 'Estacionamiento:') !!}
        {!! Form::text('nombre', $info[0]->nombre, ['class' => 'form-control', 'id' => 'nombre', 'disabled']) !!}
    </div>
    {!! Form::hidden('id',$info[0]->id) !!}
</div>


<div class="row">
    <div class="col-md-3 md-form">
        {!! Form::label('Numero_de_fichas', 'Numero de fichas') !!}
        {!! Form::number('Numero_de_fichas', $sql[0]->numero, ['min' => '1' ,'class' => 'form-control', 'placeholder' => 'Número de fichas', 'id' => 'no_fichas']) !!}
    </div>
    <div class="col-md-3 md-form">
        {!! Form::label('Importe_TH', 'Importe tarifa horaria') !!}
        {!! Form::number('Importe_TH', $sql[0]->importe, ['min' => '0', 'class' => 'form-control', 'placeholder' => 'Importe tarifa horaria:' ,'id' => 'recargos']) !!}
    </div>
    <div class="col-md-3">
        {!! Form::label('Dia_de_venta', 'Día de venta:') !!}
        {!! Form::date('Dia_de_venta', $sql[0]->venta, ['class' => 'form-control', 'placeholder' => 'Día de venta', 'id' => 'fecha']) !!}
    </div>
    <div class="col-md-3">
        {!! Form::label('Fecha_recoleccion', 'Fecha recolección:') !!}
        {!! Form::date('Fecha_recoleccion', $sql[0]->fecha_recoleccion, ['class' => 'form-control', 'placeholder' => 'Fecha recolección', 'id' => 'fecha']) !!}
    </div>

</div>

<div class="row">
    <div class="col-md-3 md-form">
        {!! Form::label('empresa_recol_past', 'Empresa recolecto antes:') !!}
        {!! Form::text('empresa', $sql[0]->empresa, ['class' => 'form-control', 'id' => 'empresa', 'disabled']) !!}
    </div>
    <div class="col-md-5 md-form">
        <span>Seleccione una empresa diferente en caso de requerirlo:</span>
    </div>

    <div class="col-md-4 md-form">
        <select class="browser-default custom-select" id="emp_traslado" name="id_empresa">
            <option value="0" selected>Empresa entrega</option>
            @forelse($empresas as $item)
                <option value="{{$item->id}}">{{$item->empresa}}</option>
            @empty
                <option value="">Sin empresas registradas</option>
            @endforelse
        </select>
    </div>

</div>

<div class="row">
    <div class="col-md-3 md-form">
        {!! Form::label('Banco_antes', 'Banco antes:') !!}
        {!! Form::text('Banco_antes', $sql[0]->banco, ['class' => 'form-control', 'id' => 'banco', 'disabled']) !!}
    </div>
    <div class="col-md-5 md-form">
        <span>Seleccione un banco diferente en caso de requerirlo:</span>
    </div>

    <div class="col-md-4 md-form">
        <select class="browser-default custom-select" id="banco_traslado" name="id_banco">
            <option value="0" selected>Banco</option>
            @forelse($bancos as $item)
                <option value="{{$item->id}}">{{$item->banco}}</option>
            @empty
                <option value="">Sin bancos registrados</option>
            @endforelse
        </select>
    </div>

</div>

<div class="md-form input-group">
    <div class="col-md-6">
        <div class="file-field">
            <div class="btn btn-primary btn-sm float-left">
                <span>Foto ficha</span>
                <input type="file" name="foto_ficha" id="file"
                       accept="image/*">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="Cargar archivo">
            </div>
        </div>
    </div>
    <!-- Fecha Recoleccion Field -->
    <div class="col-md-6">
        <div class="file-field">
            <div class="btn btn-primary btn-sm float-left">
                <span>Foto firma</span>
                <input type="file" name="foto_firma" id="file"
                       accept="image/*">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="Cargar archivo">
            </div>
        </div>
    </div>
</div>
<!-- Submit Field -->
<br>
<div class="md-form input-group">
    {!! Form::submit('Cargar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ingresos.index') !!}" class="btn btn-default">Cancelar</a>
</div>

