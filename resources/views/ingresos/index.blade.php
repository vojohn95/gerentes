@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Ingresos</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-6">
                <button type="button" class="btn btn-default btn-lg">
                    Encontrados: <span class="counter badge badge-danger ml-4"></span>
                </button>
            </div>

            <div class="col-3 text-right">
                <a type="button" class="btn btn-dark btn-rounded right-aligned" href="{!! route('ingresos.create') !!}">
                    Cargar ingreso
                </a>
            </div>
        </div>
        <br>
        @include('ingresos.table')
    </section>
@endsection


