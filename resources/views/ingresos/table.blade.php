<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Que estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Número</th>
            <th>Fecha Venta</th>
            <th>Fecha Recolección</th>
            <th>Importe</th>
            <th>Estatus</th>
            <th>Comentario</th>
            <th>Tipo</th>
            <!--<th colspan="3">Acción</th>-->
        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($ingreso as $ingresos)
            <tr>
                <td>{!! $ingresos->id !!}</td>
                <td>{!! $ingresos->venta !!}</td>
                <td>{!! $ingresos->fecha_recoleccion !!}</td>
                <td>${!! $ingresos->importe !!}</td>
                @if($ingresos->valido == "pendiente")
                    <td><h5><span class="badge badge-pill badge-primary">Pendiente</span></h5></td>
                @elseif ($ingresos->valido == "valido")
                    <td><h5><span class="badge badge-pill badge-success">Válido</span></h5></td>
                @elseif ($ingresos->valido == "rechazada")
                    <td><h5><span class="badge badge-pill badge-danger">Rechazada</span></h5></td>
                @elseif ($ingresos->valido == "corregida")
                    <td><h5><span class="badge badge-pill badge-default">Corregida</span></h5></td>
                @elseif ($ingresos->valido == "pendiente_correcion")
                    <td>
                        <h5><a href="{!! route('ingresos.edit', [$ingresos->id]) !!}"><span
                                    class="badge badge-pill badge-warning">Pendiente de correción</span></a></h5>

                    </td>
                @elseif ($ingresos->valido == "no_recolec")
                    <td><h5><span class="badge badge-pill badge-secondary">Sin recolección</span></h5></td>
                @else
                    <td><h5><span class="badge badge-pill badge-dark">Sin información</span></h5></td>
                @endif
                <td>{!! $ingresos->coment !!}</td>
                <td>{!! $ingresos->tipo !!}</td>
            <!--<td>
                    {!! Form::open(['route' => ['ingresos.destroy', $ingresos->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ingresos.show', [$ingresos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('ingresos.edit', [$ingresos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
            {!! Form::close() !!}
                </td>-->
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<br>
<br>

