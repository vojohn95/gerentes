@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Corregir ingreso del dia: {{date("d/m/Y", strtotime($sql[0]->venta))}}</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        {!! Form::model($ingresos, ['route' => ['ingresos.update', $ingresos->id], 'method' => 'patch','enctype' => 'multipart/form-data']) !!}
        @include('ingresos.corregir')
        {!! Form::close() !!}
    </section>
@endsection

