<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ingresos->id !!}</p>
</div>

<!-- Id Est Field -->
<div class="form-group">
    {!! Form::label('id_est', 'Id Est:') !!}
    <p>{!! $ingresos->id_est !!}</p>
</div>

<!-- Id Empresa Field -->
<div class="form-group">
    {!! Form::label('id_empresa', 'Id Empresa:') !!}
    <p>{!! $ingresos->id_empresa !!}</p>
</div>

<!-- Id Banco Field -->
<div class="form-group">
    {!! Form::label('id_banco', 'Id Banco:') !!}
    <p>{!! $ingresos->id_banco !!}</p>
</div>

<!-- Numero Field -->
<div class="form-group">
    {!! Form::label('numero', 'Numero:') !!}
    <p>{!! $ingresos->numero !!}</p>
</div>

<!-- Venta Field -->
<div class="form-group">
    {!! Form::label('venta', 'Venta:') !!}
    <p>{!! $ingresos->venta !!}</p>
</div>

<!-- Fecha Recoleccion Field -->
<div class="form-group">
    {!! Form::label('fecha_recoleccion', 'Fecha Recoleccion:') !!}
    <p>{!! $ingresos->fecha_recoleccion !!}</p>
</div>

<!-- Importe Field -->
<div class="form-group">
    {!! Form::label('importe', 'Importe:') !!}
    <p>{!! $ingresos->importe !!}</p>
</div>

<!-- Valido Field -->
<div class="form-group">
    {!! Form::label('valido', 'Valido:') !!}
    <p>{!! $ingresos->valido !!}</p>
</div>

<!-- Coment Field -->
<div class="form-group">
    {!! Form::label('coment', 'Coment:') !!}
    <p>{!! $ingresos->coment !!}</p>
</div>

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('tipo', 'Tipo:') !!}
    <p>{!! $ingresos->tipo !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ingresos->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ingresos->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ingresos->deleted_at !!}</p>
</div>

