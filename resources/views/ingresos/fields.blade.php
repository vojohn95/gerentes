<div class="row">
    <div class="col-md-3 md-form">
        <i class="fas fa-hashtag prefix"></i>
        {!! Form::label('num', 'Número:') !!}
        {!! Form::number('num', $info->no_est, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-9 md-form">
        <i class="fas fa-parking prefix"></i>
        {!! Form::label('nombre', 'Estacionamiento:') !!}
        {!! Form::text('nombre', $info->nombre, ['class' => 'form-control', 'id' => 'nombre']) !!}
    </div>
</div>


<div class="md-form input-group">
    {!! Form::hidden('Numero_de_fichas', 1, ['min' => '1' ,'class' => 'form-control', 'placeholder' => 'Número de fichas', 'id' => 'no_fichas']) !!}
    {!! Form::number('Importe_TH', null, ['min' => '0', 'class' => 'form-control', 'placeholder' => 'Importe tarifa horaria:' ,'id' => 'recargos']) !!}

    <select class="browser-default custom-select" id="emp_traslado" name="id_empresa" required>
        <option value="0" selected disabled>Empresa entrega</option>
        @forelse($empresas as $item)
            <option value="{{$item->id}}">{{$item->empresa}}</option>
        @empty
            <option value="">Sin empresas registradas</option>
        @endforelse
    </select>

    <select class="browser-default custom-select" id="emp_traslado" name="id_banco" required>
        <option selected value="0">Banco entrega</option>
        @forelse($bancos as $item)
            <option value="{{$item->id}}">{{$item->banco}}</option>
        @empty
            <option value="">Sin bancos registrados</option>
        @endforelse
    </select>
</div>

<!-- Venta Field -->
<div class="row">
    <div class="col-md-6">
        {!! Form::label('Dia_de_venta', 'Día de venta:') !!}
        {!! Form::date('Dia_de_venta', null, ['class' => 'form-control', 'placeholder' => 'Día de venta', 'id' => 'fecha']) !!}
    </div>
    <!-- Fecha Recoleccion Field -->
    <div class="col-md-6">
        {!! Form::label('Fecha_recoleccion', 'Fecha recolección:') !!}
        {!! Form::date('Fecha_recoleccion', null, ['class' => 'form-control', 'placeholder' => 'Fecha recolección', 'id' => 'fecha']) !!}
    </div>
</div>

<div class="md-form input-group">
    <div class="col-md-6">
        <div class="file-field">
            <div class="btn btn-primary btn-sm float-left">
                <span>Foto ficha</span>
                <input type="file" name="foto_ficha" id="file"
                       accept="image/*">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="Cargar archivo">
            </div>
        </div>
    </div>
    <!-- Fecha Recoleccion Field -->
    <div class="col-md-6">
        <div class="file-field">
            <div class="btn btn-primary btn-sm float-left">
                <span>Foto firma</span>
                <input type="file" name="foto_firma" id="file"
                       accept="image/*">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="Cargar archivo">
            </div>
        </div>
    </div>
</div>

        <!-- Submit Field -->
        <br>
        <div class="md-form input-group">
            {!! Form::submit('Cargar', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('ingresos.index') !!}" class="btn btn-default">Cancelar</a>
        </div>


        @section('scripts')
                $('#fecha').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss',
                    useCurrent: false
                })
@endsection
