@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Cargar Ingresos</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        {!! Form::open(['route' => 'ingresos.store', 'enctype' => 'multipart/form-data']) !!}
        @include('ingresos.fields')
        {!! Form::close() !!}
    </section>
@endsection
