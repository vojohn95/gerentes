@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <div class="view view-cascade gradient-card-header default-color">
                <h3 class="card-header-title">Total de pensiones al
                    {{ date('d/M/Y') }}</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-6">
                <button type="button" class="btn btn-default btn-lg">
                    Pensiones: <span class="ml-4 counter badge badge-danger"></span>
                </button>
            </div>
        </div>
        <br>
        @include('historico.table')
        <br>
    </section>
@endsection
