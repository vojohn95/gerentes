<div class="table-responsive">
    <table class="table" id="imgIngresos-table">
        <thead>
            <tr>
                <th>Id Ingreso</th>
        <th>Foto</th>
        <th>Firma</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($imgIngresos as $imgIngresos)
            <tr>
                <td>{!! $imgIngresos->id_ingreso !!}</td>
            <td>{!! $imgIngresos->foto !!}</td>
            <td>{!! $imgIngresos->firma !!}</td>
                <td>
                    {!! Form::open(['route' => ['imgIngresos.destroy', $imgIngresos->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('imgIngresos.show', [$imgIngresos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('imgIngresos.edit', [$imgIngresos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
