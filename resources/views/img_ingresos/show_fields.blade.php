<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $imgIngresos->id !!}</p>
</div>

<!-- Id Ingreso Field -->
<div class="form-group">
    {!! Form::label('id_ingreso', 'Id Ingreso:') !!}
    <p>{!! $imgIngresos->id_ingreso !!}</p>
</div>

<!-- Foto Field -->
<div class="form-group">
    {!! Form::label('foto', 'Foto:') !!}
    <p>{!! $imgIngresos->foto !!}</p>
</div>

<!-- Firma Field -->
<div class="form-group">
    {!! Form::label('firma', 'Firma:') !!}
    <p>{!! $imgIngresos->firma !!}</p>
</div>

