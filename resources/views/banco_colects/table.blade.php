<div class="table-responsive">
    <table class="table" id="bancoColects-table">
        <thead>
            <tr>
                <th>Banco</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($bancoColects as $bancoColect)
            <tr>
                <td>{!! $bancoColect->banco !!}</td>
                <td>
                    {!! Form::open(['route' => ['bancoColects.destroy', $bancoColect->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('bancoColects.show', [$bancoColect->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('bancoColects.edit', [$bancoColect->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
