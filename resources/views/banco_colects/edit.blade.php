@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Banco Colect
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($bancoColect, ['route' => ['bancoColects.update', $bancoColect->id], 'method' => 'patch']) !!}

                        @include('banco_colects.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection