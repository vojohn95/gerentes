<div>
    <div class="d-flex justify-content-between">
        {{-- <div class="form-group">
            <label for="exampleFormControlSelect1">Seleccione un estacionamiento</label>
            <select id="exampleFormControlSelect1" style="display:block!important;" wire:model="select_id">
                <option value="" selected>Seleccione una opción</option>
                <option value="all">Ver todos los estacionamientos</option>
                @foreach ($estacionamientos as $estacionamiento)
                    <option value="{{ $estacionamiento->id_parks }}">{{ $estacionamiento->id_parks }}</option>
                @endforeach
            </select>
        </div> --}}
        <a type="button" class="btn btn-default btn-lg" href="{{ route('facturacionespeciales.create') }}">
            Generar factura
        </a>
    </div>
    @include('livewire.factespecialtable')

</div>
