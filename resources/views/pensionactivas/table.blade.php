<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table text-center table-hover table-bordered results responsive-table">
        <thead>
            <tr>
                <th>Pensión</th>
                <th>Serie</th>
                <th>Folio</th>
                <th>Fecha Timbrado</th>
                <th>UUID</th>
                <th>Subtotal Factura</th>
                <th>Iva Factura</th>
                <th>Total Factura</th>
                <th>Xml</th>
                <th>Pdf</th>
                <th>Estatus</th>
                <!--<th>Usocfdi</th>
                <th>Metodo pago</th>-->
                <!-- <th colspan="3">Acción</th> -->
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
