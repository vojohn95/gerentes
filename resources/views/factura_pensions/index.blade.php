@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <div class="view view-cascade gradient-card-header default-color">
                <h3 class="card-header-title">Facturas activas</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-6">
                <button type="button" class="btn btn-default btn-lg">
                    facturas: <span class="ml-4 counter badge badge-danger"></span>
                </button>
            </div>
            <div class="text-right col-3">
                <a type="button" class="btn btn-dark btn-rounded right-aligned" href="{!! route('facturaPensions.create') !!}">
                    Solicitar factura
                </a>
            </div>
        </div>
        <br>
        @include('factura_pensions.table')
    </section>
@endsection
