<!-- Id Pen Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_pen', 'Pensión:') !!}
    <select class="browser-default custom-select" name="id_pen" required>
        <option value="" disabled selected>Escoja una opción</option>
        @foreach ($pensiones as $pension)
            <option value="{{ $pension->id }}">{{ $pension->noTarjeta }} / {{ $pension->RFC}} / {{ $pension->nombres }} {{ $pension->apellido_paterno }} {{ $pension->apellido_materno}}</option>
        @endforeach
    </select>
</div>

<!-- Serie Field -->
{{-- <div class="form-group col-sm-12">
    {!! Form::label('serie', 'Serie:') !!}
    {!! Form::text('serie', null, ['class' => 'form-control', 'maxlength' => 255, 'maxlength' => 255]) !!}
</div> --}}

<!-- Folio Field -->
{{-- <div class="form-group col-sm-12">
    {!! Form::label('folio', 'Folio:') !!}
    {!! Form::text('folio', null, ['class' => 'form-control', 'maxlength' => 255, 'maxlength' => 255]) !!}
</div> --}}

<!-- Fecha Timbrado Field -->
{{--<div class="form-group col-sm-12">
    {!! Form::label('fecha_timbrado', 'Fecha Timbrado:') !!}
    {!! Form::date('fecha_timbrado', null, ['class' => 'form-control', 'id' => 'fecha_timbrado']) !!}
</div> --}}

@push('scripts')
    <script type="text/javascript">
        $('#fecha_timbrado').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

    </script>
@endpush

<!-- Uuid Field -->
{{--<div class="form-group col-sm-12">
    {!! Form::label('uuid', 'Uuid:') !!}
    {!! Form::text('uuid', null, ['class' => 'form-control', 'maxlength' => 255, 'maxlength' => 255]) !!}
</div>--}}

<!-- Subtotal Factura Field -->
{{-- <div class="form-group col-sm-12">
    {!! Form::label('subtotal_factura', 'Subtotal Factura:') !!}
    {!! Form::number('subtotal_factura', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Iva Factura Field -->
{{--<div class="form-group col-sm-12">
    {!! Form::label('iva_factura', 'Iva Factura:') !!}
    {!! Form::number('iva_factura', null, ['class' => 'form-control']) !!}
</div>--}}

<!-- Total Factura Field -->
<div class="form-group col-sm-12">
    {!! Form::label('total_factura', 'Total Factura:') !!}
    {!! Form::number('total_factura', null, ['class' => 'form-control']) !!}
</div>

<!-- Xml Field -->

{{--<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('XML', 'Xml:') !!}
    <div class="file-field">
        <div class="float-left btn btn-primary btn-sm">
            <span>Cargar xml</span>
            <input type="file" name="XML" id="foto" accept="application/xml">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" name="XML" id="foto" accept="application/xml" type="text"
                placeholder="Cargar xml">
        </div>
    </div>
</div>

<!-- Pdf Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('PDF', 'Pdf:') !!}
    <div class="file-field">
        <div class="float-left btn btn-primary btn-sm">
            <span>Cargar pdf</span>
            <input type="file" name="PDF" id="foto" accept="application/pdf">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" name="PDF" id="foto" accept="application/pdf" type="text"
                placeholder="Cargar pdf">
        </div>
    </div>
</div>--}}

<!-- Estatus Field -->
{!! Form::hidden('estatus', 2, ['class' => 'form-control', 'maxlength' => 255, 'maxlength' => 255]) !!}

<!-- Id Usocfdi Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_usoCFDI', 'Usocfdi:') !!}
    <select class="browser-default custom-select" name="id_usoCFDI" required>
        <option value="" disabled selected>Escoja una opción</option>
        @foreach ($usocfdis as $uso)
            <option value="{{ $uso->id }}">{{ $uso->claveSat }} / {{ $uso->descricion }}</option>
        @endforeach
    </select>
</div>

<!-- Id Metodopago Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_metodoPago', 'Metodo de pago:') !!}
    <select class="browser-default custom-select" name="id_metodoPago" required>
        <option value="" disabled selected>Escoja una opción</option>
        @foreach ($metodos as $met)
            <option value="{{ $met->id }}">{{ $met->clave }} / {{ $met->Descripcion }}</option>
        @endforeach
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary', 'onclick' => "return confirm('¿La información es correcta?')"]) !!}
    <a href="{{ route('facturaPensions.index') }}" class="btn btn-default">Cancelar</a>
</div>
