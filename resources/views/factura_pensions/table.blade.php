<div class="table-responsive">
    <table id="dtBasicExample" class="table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Serie</th>
                <th>Folio</th>
                <th>Fecha Timbrado</th>
                <th>Cliente</th>
                <th>RFC</th>
                <th>SubTotal</th>
                <th>Iva</th>
                <th>Total</th>
                <th>Proyecto</th>
                <th>Xml</th>
                <th>Pdf</th>
                <!--<th>Usocfdi</th>
                <th>Metodo pago</th>-->
                <!-- <th colspan="3">Acción</th> -->
            </tr>
        </thead>
        <tbody>
            @foreach ($facturaPensions as $facturaPension)
                <tr>
                    <td>{{ $facturaPension->facturaId }}</td>
                    <td>
                        @if ($facturaPension->serie)
                            {{ $facturaPension->serie }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->folio)
                            {{ $facturaPension->folio }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->fecha_timbrado)
                            {{ $facturaPension->fecha_timbrado }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->nombre_cliente)
                            {{ $facturaPension->nombre_cliente }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->RFC)
                            {{ $facturaPension->RFC }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->subtotal_factura)
                            {{ $facturaPension->subtotal_factura }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->iva)
                            {{ $facturaPension->iva }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>{{ $facturaPension->total_factura }}</td>
                    <td>
                        @if ($facturaPension->proyecto)
                            {{ $facturaPension->numero_proyecto }} / {{ $facturaPension->proyecto }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                            <a href="{!! route('xmlPen', [$facturaPension->facturaId]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                    </td>
                    <td>
                            <a href="{!! route('pdfPen', [$facturaPension->facturaId]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                    </td>
                   {{-- <td>
                        {!! Form::open(['route' => ['facturaPensions.destroy', $facturaPension->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                             <a href="{{ route('facturaPensions.show', [$facturaPension->id]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                            <a href="{{ route('facturaPensions.edit', [$facturaPension->id]) }}"
                                class='btn btn-default btn-xs'>Editar</a>
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td> --}}
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
