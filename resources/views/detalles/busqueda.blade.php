@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <div class="view view-cascade gradient-card-header default-color">
                <h3 class="card-header-title">Resultado de búsqueda
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="d-flex justify-content-between">
            <div>

            </div>
            {{-- <div class="col-6">
                <button type="button" class="btn btn-default btn-lg">
                    Detalles: <span class="counter badge badge-danger ml-4"></span>
                </button>
            </div>
            <div class="col-3 text-right">
                <a type="button" class="btn btn-dark btn-rounded right-aligned" href="{!! route('detalles.create') !!}">
                    Cargar Pensión
                </a>
            </div>
            <div class="col-3 text-right">
                <a type="button" class="btn btn-green btn-rounded right-aligned" href="{!! route('reportpension') !!}">
                    Generar reporte
                </a>
            </div> --}}
            <div class=" pull-right">
                <a type="button" class="btn btn-green btn-rounded right-aligned btn-sm" data-toggle="modal"
                    data-target="#exampleModal">
                    Busqueda especifica
                </a>
            </div>
        </div>
        <br>
        @include('detalles.busquedatable')
    </section>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Resultado de búsqueda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ route('findBusquedaEspecifica', [1]) }}" type="button" class="btn btn-default btn-sm">Bajas Temporales</a>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ route('findBusquedaEspecifica', [2]) }}" type="button" class="btn btn-info btn-sm">Canceladas</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
