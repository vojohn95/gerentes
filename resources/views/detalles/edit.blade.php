@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Editar pensión</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                {!! Form::model($detalle, ['route' => ['detalles.update', $detalle->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                @include('detalles.fields')

                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection
