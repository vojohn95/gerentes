<!-- Id Estacionamiento Field -->
<div class="form-group">
    {!! Form::label('id_estacionamiento', 'Id Estacionamiento:') !!}
    <p>{{ $detalle->id_estacionamiento }}</p>
</div>

<!-- Montopension Field -->
<div class="form-group">
    {!! Form::label('montoPension', 'Montopension:') !!}
    <p>{{ $detalle->montoPension }}</p>
</div>

<!-- Tipopension Field -->
<div class="form-group">
    {!! Form::label('tipoPension', 'Tipopension:') !!}
    <p>{{ $detalle->tipoPension }}</p>
</div>

<!-- Contrato Field -->
<div class="form-group">
    {!! Form::label('contrato', 'Contrato:') !!}
    <p>{{ $detalle->contrato }}</p>
</div>

<!-- Solicitudcontrato Field -->
<div class="form-group">
    {!! Form::label('solicitudContrato', 'Solicitudcontrato:') !!}
    <p>{{ $detalle->solicitudContrato }}</p>
</div>

<!-- Comprobantedomicilio Field -->
<div class="form-group">
    {!! Form::label('comprobanteDomicilio', 'Comprobantedomicilio:') !!}
    <p>{{ $detalle->comprobanteDomicilio }}</p>
</div>

<!-- Ine Field -->
<div class="form-group">
    {!! Form::label('ine', 'Ine:') !!}
    <p>{{ $detalle->ine }}</p>
</div>

<!-- Licencia Field -->
<div class="form-group">
    {!! Form::label('licencia', 'Licencia:') !!}
    <p>{{ $detalle->licencia }}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('rfc', 'Rfc:') !!}
    <p>{{ $detalle->rfc }}</p>
</div>

<!-- Tarjetacirculacion Field -->
<div class="form-group">
    {!! Form::label('tarjetaCirculacion', 'Tarjetacirculacion:') !!}
    <p>{{ $detalle->tarjetaCirculacion }}</p>
</div>

<!-- Notarjeta Field -->
<div class="form-group">
    {!! Form::label('noTarjeta', 'Notarjeta:') !!}
    <p>{{ $detalle->noTarjeta }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $detalle->status }}</p>
</div>

<!-- Fecha Limite Field -->
<div class="form-group">
    {!! Form::label('fecha_limite', 'Fecha Limite:') !!}
    <p>{{ $detalle->fecha_limite }}</p>
</div>

