<div class="table-responsive">
    <table id="dtBasicExample" class="table">
        <thead>
            <tr>
                <th>Nota de venta</th>
                <th>Número de cliente</th>
                <th>Número de pensión</th>
                <th>Fecha de Alta</th>
                <th>Fecha baja o cancelación</th>
                <th>Número de proyecto</th>
                <th>Tarjetón</th>
                <th>RFC</th>
                <th>Nombre del usuario</th>
                <th>Razón social</th>
                <th>Correo electrónico</th>
                <th>Tipo de pensión</th>
                <th>Pago aplicado</th>
                <th>Mes/Periodo</th>
                <th>Tiempo</th>
                <th>Costo de la pensión</th>
                <th>Recargos</th>
                <th>Venta tarjeta</th>
                <th>Reposición tarjeta</th>
                <th>Tipo de pago</th>
                <th>Monto depositado</th>
                <th>Comprobante bancario</th>
                <th>Banco asignado</th>
                <th>Ficha Deposito</th>
                <th>Recolección de valores</th>
                <th>Tipo de factura</th>
                <th>No. factura</th>
                <th>Correo/factura</th>
                <th>Contrato</th>
                <th>Solicitud de contrato</th>
                <th>Comprobante domicilio</th>
                <th>Documento INE</th>
                <th>Documento Licencia</th>
                <th>Documento RFC</th>
                <th>Documento Tarjeta Circulación</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($detalles as $detalle)
                <tr>
                    <td>{{ $detalle->nota_venta }}</td>
                    <td>{{ $detalle->numero_cliente }}</td>
                    <td>{{ $detalle->numero_pension }}</td>
                    <td>{{ $detalle->fecha_alta }}</td>
                    <td>{{ $detalle->fecha_baja }}</td>
                    <td>{{ $detalle->numero_proyecto }}</td>
                    <td>{{ $detalle->tarjeton }}</td>
                    <td>{{ $detalle->rfc }}</td>
                    <td>{{ $detalle->nombre_usuario }}</td>
                    <td>{{ $detalle->razon_social }}</td>
                    <td>{{ $detalle->email }}</td>
                    <td>{{ $detalle->tipo_pension }}</td>
                    <td>{{ $detalle->pago_aplicado }}</td>
                    <td>{{ $detalle->mes_periodo }}</td>
                    <td>{{ $detalle->tiempo }}</td>
                    <td>{{ $detalle->costo_pension }}</td>
                    <td>{{ $detalle->recargos }}</td>
                    <td>{{ $detalle->venta_tarjeta }}</td>
                    <td>{{ $detalle->reposicion_tarjeta }}</td>
                    <td>{{ $detalle->tipo_pago }}</td>
                    <td>{{ $detalle->monto_depositado }}</td>
                    <td>{{ $detalle->comprobante_bancario }}</td>
                    <td>{{ $detalle->banco_asignado }}</td>
                    <td>{{ $detalle->comprobante_pago }}</td>
                    <td>{{ $detalle->recoleccion_valores }}</td>
                    <td>{{ $detalle->tipo_factura }}</td>
                    <td>{{ $detalle->no_factura }}</td>
                    <td>{{ $detalle->correo_factura }}</td>
                    <td>{{ $detalle->contrato }}</td>
                    <td>{{ $detalle->solicitud_contrato }}</td>
                    <td>{{ $detalle->comprobante_domicilio }}</td>
                    <td>{{ $detalle->documento_ine }}</td>
                    <td>{{ $detalle->documento_licencia }}</td>
                    <td>{{ $detalle->documento_rfc }}</td>
                    <td>{{ $detalle->documento_tarjeta_circulacion }}</td>
                    <!-- Datos de bajas en pensiones -->
                    {{-- <td>{{ $detalle->montoPension }}}}<td>
                    <td>{{ $detalle->tipoPension }}}}<td>
                    <td>
                        <a href="{!! route('contp', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>
                    </td>
                    <td>

                        <a href="{!! route('scontp', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>

                    </td>
                    <td>

                        <a href="{!! route('ccontp', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>

                    </td>
                    <td>

                        <a href="{!! route('ine', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>

                    </td>
                    <td>

                        <a href="{!! route('licencia', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>

                    </td>
                    <td>{{ $detalle->rfc }}</td>
                    <td>

                        <a href="{!! route('tarjetaC', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>

                    </td>
                    <td>{{ $detalle->noTarjeta }}</td>
                    <td>
                        @php
                            $fechaAntigua = DB::connection('mysql2')
                                ->table('pensionesDet')
                                ->select('created_at')
                                ->where('id', '=', $detalle->id)
                                ->get();

                            $dbDate = new DateTime($fechaAntigua[0]->created_at);
                            $hoyDate = new DateTime(date('Y-m-d H:i:s'));
                            $timePassed = $dbDate->diff($hoyDate);
                        @endphp
                        @if ($timePassed->days >= 31)
                            Pensión nueva
                        @else
                            @if ($detalle->status == 1)
                                Activo
                            @elseif ($detalle->status == 0)
                                Inactivo
                            @else
                                Baja temporal
                            @endif
                        @endif
                    </td>
                    <td>{{ $detalle->fecha_limite }}</td>
                    <td>
                        <a href="{!! route('inactivo', [$detalle->id]) !!}" class='btn btn-sm btn-red'>Baja</a>
                    </td>
                    <td>
                        <a href="{!! route('bajatemporal', [$detalle->id]) !!}" class='btn btn-sm btn-default'>Baja Temportal</a>
                    </td>
                    {{-- <td>
                        {!! Form::open(['route' => ['detalles.destroy', $detalle->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <!--<a href="{{ route('detalles.show', [$detalle->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i></a>-->
                            <a href="{{ route('detalles.edit', [$detalle->id]) }}" class='btn btn-default btn-xs'>Editar</a>
                            {!! Form::button('ELIMINAR', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td> --}}
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<br>

@section('datatable')

@endsection
