<div class="table-responsive">
    <table id="dtBasicExample" class="table">
        <thead>
            <tr>
                <th>Número de Proyecto</th>
                <th>Proyecto</th>
                <th>Número Pensión</th>
                <th>Número Cliente</th>
                <th>Grupo</th>
                <th>Nombre pensionado</th>
                <th>Estado</th>
                <th>Fecha baja/cancelación</th>
                <th>Contrato</th>
                <th>Solicitud contrato</th>
                <th>Comprobante domicilio</th>
                <th>INE</th>
                <th>Licencia</th>
                <th>RFC</th>
                <th>Tarjeta circulación</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($detalles as $detalle)
                <tr>
                    <td>{{ $detalle->NumeroProyecto }}</td>
                    <td>{{ $detalle->Proyecto }}</td>
                    <td>{{ $detalle->NumeroPension }}</td>
                    <td>{{ $detalle->NumeroCliente }}</td>
                    <td>{{ $detalle->grupo }}</td>
                    <td>{{ $detalle->nombrepensionado }}</td>
                    <td>{{ $detalle->estado }}</td>
                    <td>{{ $detalle->fechabajacancelacion }}</td>
                    <td>{{ $detalle->contrato }}</td>
                    <td>{{ $detalle->solicitudContrato }}</td>
                    <td>{{ $detalle->comprobanteDomicilio }}</td>
                    <td>{{ $detalle->ine }}</td>
                    <td>{{ $detalle->licencia }}</td>
                    <td>{{ $detalle->rfc }}</td>
                    <td>{{ $detalle->tarjetaCirculacion }}</td>
                    <!-- Datos de bajas en pensiones -->
                    {{-- <td>{{ $detalle->montoPension }}}}<td>
                    <td>{{ $detalle->tipoPension }}}}<td>
                    <td>
                        <a href="{!! route('contp', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>
                    </td>
                    <td>

                        <a href="{!! route('scontp', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>

                    </td>
                    <td>

                        <a href="{!! route('ccontp', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>

                    </td>
                    <td>

                        <a href="{!! route('ine', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>

                    </td>
                    <td>

                        <a href="{!! route('licencia', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>

                    </td>
                    <td>{{ $detalle->rfc }}</td>
                    <td>

                        <a href="{!! route('tarjetaC', [$detalle->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>

                    </td>
                    <td>{{ $detalle->noTarjeta }}</td>
                    <td>
                        @php
                            $fechaAntigua = DB::connection('mysql2')
                                ->table('pensionesDet')
                                ->select('created_at')
                                ->where('id', '=', $detalle->id)
                                ->get();

                            $dbDate = new DateTime($fechaAntigua[0]->created_at);
                            $hoyDate = new DateTime(date('Y-m-d H:i:s'));
                            $timePassed = $dbDate->diff($hoyDate);
                        @endphp
                        @if ($timePassed->days >= 31)
                            Pensión nueva
                        @else
                            @if ($detalle->status == 1)
                                Activo
                            @elseif ($detalle->status == 0)
                                Inactivo
                            @else
                                Baja temporal
                            @endif
                        @endif
                    </td>
                    <td>{{ $detalle->fecha_limite }}</td>
                    <td>
                        <a href="{!! route('inactivo', [$detalle->id]) !!}" class='btn btn-sm btn-red'>Baja</a>
                    </td>
                    <td>
                        <a href="{!! route('bajatemporal', [$detalle->id]) !!}" class='btn btn-sm btn-default'>Baja Temportal</a>
                    </td>
                    {{-- <td>
                        {!! Form::open(['route' => ['detalles.destroy', $detalle->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <!--<a href="{{ route('detalles.show', [$detalle->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i></a>-->
                            <a href="{{ route('detalles.edit', [$detalle->id]) }}" class='btn btn-default btn-xs'>Editar</a>
                            {!! Form::button('ELIMINAR', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td> --}}
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<br>

@section('datatable')

@endsection
