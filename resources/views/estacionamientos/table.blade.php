<div class="table-responsive">
    <table class="table" id="estacionamientos-table">
        <thead>
        <tr>
            <th>Id Marca</th>
            <th>Id Org</th>
            <th>Nombre</th>
            <th>Dist Regio</th>
            <th>Direccion</th>
            <th>Calle</th>
            <th>No Ext</th>
            <th>Colonia</th>
            <th>Municipio</th>
            <th>Estado</th>
            <th>Pais</th>
            <th>Cp</th>
            <th>Latitud</th>
            <th>Longitud</th>
            <th>Facturable</th>
            <th>Automatico</th>
            <th>Cajones</th>
            <th>Folio</th>
            <th>Serie</th>
            <th>Correo</th>
            <th>Pensiones</th>
            <th>Cortesias</th>
            <th>Observaciones</th>
            <th>Escuela</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($estacionamientos as $estacionamientos)
            <tr>
                <td>{!! $estacionamientos->id_marca !!}</td>
                <td>{!! $estacionamientos->id_org !!}</td>
                <td>{!! $estacionamientos->nombre !!}</td>
                <td>{!! $estacionamientos->dist_regio !!}</td>
                <td>{!! $estacionamientos->direccion !!}</td>
                <td>{!! $estacionamientos->calle !!}</td>
                <td>{!! $estacionamientos->no_ext !!}</td>
                <td>{!! $estacionamientos->colonia !!}</td>
                <td>{!! $estacionamientos->municipio !!}</td>
                <td>{!! $estacionamientos->estado !!}</td>
                <td>{!! $estacionamientos->pais !!}</td>
                <td>{!! $estacionamientos->cp !!}</td>
                <td>{!! $estacionamientos->latitud !!}</td>
                <td>{!! $estacionamientos->longitud !!}</td>
                <td>{!! $estacionamientos->Facturable !!}</td>
                <td>{!! $estacionamientos->Automatico !!}</td>
                <td>{!! $estacionamientos->cajones !!}</td>
                <td>{!! $estacionamientos->folio !!}</td>
                <td>{!! $estacionamientos->serie !!}</td>
                <td>{!! $estacionamientos->correo !!}</td>
                <td>{!! $estacionamientos->pensiones !!}</td>
                <td>{!! $estacionamientos->cortesias !!}</td>
                <td>{!! $estacionamientos->observaciones !!}</td>
                <td>{!! $estacionamientos->escuela !!}</td>
                <td>
                    {!! Form::open(['route' => ['estacionamientos.destroy', $estacionamientos->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('estacionamientos.show', [$estacionamientos->id]) !!}"
                           class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('estacionamientos.edit', [$estacionamientos->id]) !!}"
                           class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
