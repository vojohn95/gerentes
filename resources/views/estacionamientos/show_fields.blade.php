<!-- Id Marca Field -->
<div class="form-group">
    {!! Form::label('id_marca', 'Id Marca:') !!}
    <p>{!! $estacionamientos->id_marca !!}</p>
</div>

<!-- Id Org Field -->
<div class="form-group">
    {!! Form::label('id_org', 'Id Org:') !!}
    <p>{!! $estacionamientos->id_org !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $estacionamientos->nombre !!}</p>
</div>

<!-- Dist Regio Field -->
<div class="form-group">
    {!! Form::label('dist_regio', 'Dist Regio:') !!}
    <p>{!! $estacionamientos->dist_regio !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{!! $estacionamientos->direccion !!}</p>
</div>

<!-- Calle Field -->
<div class="form-group">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{!! $estacionamientos->calle !!}</p>
</div>

<!-- No Ext Field -->
<div class="form-group">
    {!! Form::label('no_ext', 'No Ext:') !!}
    <p>{!! $estacionamientos->no_ext !!}</p>
</div>

<!-- Colonia Field -->
<div class="form-group">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{!! $estacionamientos->colonia !!}</p>
</div>

<!-- Municipio Field -->
<div class="form-group">
    {!! Form::label('municipio', 'Municipio:') !!}
    <p>{!! $estacionamientos->municipio !!}</p>
</div>

<!-- Estado Field -->
<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{!! $estacionamientos->estado !!}</p>
</div>

<!-- Pais Field -->
<div class="form-group">
    {!! Form::label('pais', 'Pais:') !!}
    <p>{!! $estacionamientos->pais !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $estacionamientos->cp !!}</p>
</div>

<!-- Latitud Field -->
<div class="form-group">
    {!! Form::label('latitud', 'Latitud:') !!}
    <p>{!! $estacionamientos->latitud !!}</p>
</div>

<!-- Longitud Field -->
<div class="form-group">
    {!! Form::label('longitud', 'Longitud:') !!}
    <p>{!! $estacionamientos->longitud !!}</p>
</div>

<!-- Facturable Field -->
<div class="form-group">
    {!! Form::label('Facturable', 'Facturable:') !!}
    <p>{!! $estacionamientos->Facturable !!}</p>
</div>

<!-- Automatico Field -->
<div class="form-group">
    {!! Form::label('Automatico', 'Automatico:') !!}
    <p>{!! $estacionamientos->Automatico !!}</p>
</div>

<!-- Cajones Field -->
<div class="form-group">
    {!! Form::label('cajones', 'Cajones:') !!}
    <p>{!! $estacionamientos->cajones !!}</p>
</div>

<!-- Folio Field -->
<div class="form-group">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{!! $estacionamientos->folio !!}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{!! $estacionamientos->serie !!}</p>
</div>

<!-- Correo Field -->
<div class="form-group">
    {!! Form::label('correo', 'Correo:') !!}
    <p>{!! $estacionamientos->correo !!}</p>
</div>

<!-- Pensiones Field -->
<div class="form-group">
    {!! Form::label('pensiones', 'Pensiones:') !!}
    <p>{!! $estacionamientos->pensiones !!}</p>
</div>

<!-- Cortesias Field -->
<div class="form-group">
    {!! Form::label('cortesias', 'Cortesias:') !!}
    <p>{!! $estacionamientos->cortesias !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $estacionamientos->observaciones !!}</p>
</div>

<!-- Escuela Field -->
<div class="form-group">
    {!! Form::label('escuela', 'Escuela:') !!}
    <p>{!! $estacionamientos->escuela !!}</p>
</div>

