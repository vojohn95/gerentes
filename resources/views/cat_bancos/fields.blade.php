<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Banco Field -->
<div class="form-group col-sm-6">
    {!! Form::label('banco', 'Banco:') !!}
    {!! Form::text('banco', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('catBancos.index') }}" class="btn btn-default">Cancel</a>
</div>
