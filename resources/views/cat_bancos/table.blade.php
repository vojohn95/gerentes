<div class="table-responsive">
    <table class="table" id="catBancos-table">
        <thead>
            <tr>
                <th>Id</th>
        <th>Banco</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($catBancos as $catBanco)
            <tr>
                <td>{{ $catBanco->id }}</td>
            <td>{{ $catBanco->banco }}</td>
                <td>
                    {!! Form::open(['route' => ['catBancos.destroy', $catBanco->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('catBancos.show', [$catBanco->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('catBancos.edit', [$catBanco->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
