@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Empresa Colect
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($empresaColect, ['route' => ['empresaColects.update', $empresaColect->id], 'method' => 'patch']) !!}

                        @include('empresa_colects.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection