@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <div class="view view-cascade gradient-card-header default-color">
                <h3 class="card-header-title">Actualizaciones
            </div>

        </div>
        <hr>
        <div class="col-12 text-right d-flex">
            <a type="button" class="btn btn-dark btn-rounded right-aligned ml-auto" href="{!! route('actualizacionesPen.create') !!}">
                Cargar
            </a>
        </div>
        @include('layouts.errors')
        @include('flash::message')

        @include('actualizapen.table')
        <br>
    </section>

@endsection
