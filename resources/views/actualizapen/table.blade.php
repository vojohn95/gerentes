<div class="table-responsive">
    <table class="table" id="dtBasicExample">
        <thead>
            <tr>
                <!--<th>Estacionamiento</th>-->
                <th>Número Proyecto</th>
                <th>Proyecto</th>
                <th>Número Pensión</th>
                <th>Número Cliente</th>
                <th>Nombre pensionado</th>
                <th>Estado</th>
                <th>Tipo</th>
                <th>Fecha baja/cancelación</th>
                <th>Contrato</th>
                <th>Solicitud Contrato</th>
                <th>Comprobante domicilio</th>
                <th>INE</th>
                <th>Licencia</th>
                <th>RFC</th>
                <th>Tarjeta circulación</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($detalles as $detalle)
                <tr>
                    <td>{{ $detalle->NumeroProyecto }}</td>
                    <td>{{ $detalle->Proyecto }}</td>
                    <td>{{ $detalle->NumeroPension }}</td>
                    <td>{{ $detalle->NumeroCliente }}</td>
                    <td>{{ $detalle->nombrepensionado }}</td>
                    <td>{{ $detalle->estado }}</td>
                    <td>{{ $detalle->grupo }}</td>
                    <td>{{ $detalle->fechabajacancelacion }}</td>
                    <td>{{ $detalle->contrato }}</td>
                    <td>{{ $detalle->solicitudContrato }}</td>
                    <td>{{ $detalle->comprobanteDomicilio }}</td>
                    <td>{{ $detalle->ine }}</td>
                    <td>{{ $detalle->licencia }}</td>
                    <td>{{ $detalle->rfc }}</td>
                    <td>{{ $detalle->tarjetaCirculacion }}</td>
                    <td>
                        {{-- {!! Form::open(['route' => ['actualizacionesPen.destroy', $detalle->NumeroPension], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!--<a href="{{ route('actualizacionesPen.show', [$detalle->NumeroCliente, $detalle->NumeroPension]) }}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>--> --}}
                        <a href="{{ route('actualizacionesPen.edit', [$detalle->NumeroCliente, 'pension' => $detalle->NumeroPension]) }}"
                            class='btn btn-default btn-sm'>Editar</a>
                        {{-- {!! Form::button('ELIMINAR', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!} --}}
                    </td>
                    <td>
                        {!! Form::open(['route' => 'actualizacionesPenDel']) !!}
                        {{-- <a href="{{ route('actualizacionesPenDel', [$detalle->NumeroPension]) }}"
                            class='btn btn-danger btn-sm'>Eliminar</a> --}}
                        {!! Form::hidden('idpension', $detalle->NumeroPension, ['class' => 'form-control']) !!}
                        {!! Form::hidden('idcliente', $detalle->NumeroCliente, ['class' => 'form-control']) !!}
                        {!! Form::submit('Eliminar', ['class' => 'btn btn-danger btn-sm']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<br>
