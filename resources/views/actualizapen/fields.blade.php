<!-- Montopension Field -->
<div class="form-group col-sm-12">
    {!! Form::label('montoPension', 'Monto pension:') !!}
    {!! Form::number('montoPension', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Tipopension Field -->
<div class="form-group col-sm-12">
    {!! Form::label('tipoPension', 'Tipo pensión:') !!}
    {!! Form::text('tipoPension', null, ['class' => 'form-control', 'maxlength' => 15, 'maxlength' => 15, 'required']) !!}
</div>

<div class="form-group col-sm-12">
    <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="id_estacionamiento"
        name="id_estacionamiento">
        <option value="">Elija su proyecto</option>
        @foreach ($proyectos as $proyecto)
            <option value="{{ $proyecto->id }}" {{ $proyecto->id == $pensions->id_estacionamiento ? 'selected' : '' }}>
                ({{ $proyecto->no_estacionamiento }}) / {{ $proyecto->nombre_proyecto }}</option>
        @endforeach
    </select>
</div>

<div class="form-group col-sm-12">
    <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="grupo"
        name="grupo">
        <option value="">¿Es un grupo esta pensión?</option>
        <option value="Individual">No</option>
        <option value="Grupo">Si</option>
    </select>
</div>

<!-- Contrato Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('contrato', 'Contrato:') !!}
    <div class="file-field">
        <div class="btn btn-primary btn-sm float-left">
            <span>Cargar contrato</span>
            <input type="file" name="contrato" id="foto" accept="application/pdf">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" name="contrato" id="foto" accept="application/pdf" type="text"
                placeholder="Cargar contrato">
        </div>
    </div>
</div>

<!-- Solicitudcontrato Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('solicitudContrato', 'Solicitud contrato:') !!}
    <div class="file-field">
        <div class="btn btn-primary btn-sm float-left">
            <span>Cargar solicitud</span>
            <input type="file" name="solicitudContrato" id="foto" accept="application/pdf">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" name="solicitudContrato" id="foto" accept="application/pdf" type="text"
                placeholder="Cargar solicitud de contratos">
        </div>
    </div>
</div>

<!-- Comprobantedomicilio Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('comprobanteDomicilio', 'Comprobante de domicilio:') !!}
    <div class="file-field">
        <div class="btn btn-primary btn-sm float-left">
            <span>Cargar comprobante</span>
            <input type="file" name="comprobanteDomicilio" id="foto" accept="application/pdf">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" name="comprobanteDomicilio" id="foto" accept="application/pdf" type="text"
                placeholder="Cargar comprobante">
        </div>
    </div>

</div>

<!-- Ine Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('ine', 'Ine:') !!}
    <div class="file-field">
        <div class="btn btn-primary btn-sm float-left">
            <span>Cargar ine</span>
            <input type="file" name="ine" id="foto" accept="application/pdf">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" name="ine" id="foto" accept="application/pdf" type="text"
                placeholder="Cargar ine">
        </div>
    </div>
</div>

<!-- Licencia Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('licencia', 'Licencia:') !!}
    <div class="file-field">
        <div class="btn btn-primary btn-sm float-left">
            <span>Cargar licencia</span>
            <input type="file" name="licencia" id="foto" accept="application/pdf">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" name="licencia" id="foto" accept="application/pdf" type="text"
                placeholder="Cargar licencia ">
        </div>
    </div>
</div>

<!-- Rfc Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rfc', 'Rfc:') !!}
    {!! Form::text('rfc', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Tarjetacirculacion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('tarjetaCirculacion', 'Tarjeta circulacion:') !!}
    <div class="file-field">
        <div class="btn btn-primary btn-sm float-left">
            <span>Cargar tarjeta</span>
            <input type="file" name="tarjetaCirculacion" id="tarjetaCirculacion" accept="application/pdf">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" name="tarjetaCirculacion" id="foto" accept="application/pdf" type="text"
                placeholder="Cargar tarjeta de circulación">
        </div>
    </div>
</div>

<!-- Notarjeta Field -->
<div class="form-group col-sm-12">
    {!! Form::label('noTarjeta', 'No. tarjeta:') !!}
    {!! Form::number('noTarjeta', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Status Field -->
{!! Form::hidden('status', 1, ['class' => 'form-control', 'maxlength' => 2, 'maxlength' => 2, 'required']) !!}

<!-- Fecha Limite Field -->
<div class="form-group col-sm-12">
    {!! Form::label('fecha_limite', 'Fecha Limite:') !!}
    {!! Form::date('fecha_limite', null, ['class' => 'form-control', 'id' => 'fecha_limite']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha_limite').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary', 'onclick' => "return confirm('¿La información que ingreso es correcta?')"]) !!}
    <a href="{{ route('detalles.index') }}" class="btn btn-default">Cancelar</a>
</div>
