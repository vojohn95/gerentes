@extends('layouts.app')
@section('title', 'Facturas')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Facturas</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            @livewire('facturaespecialcomponent')

        </div>
        <br>
    </section>
@endsection
