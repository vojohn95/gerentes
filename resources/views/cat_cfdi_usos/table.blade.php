<div class="table-responsive">
    <table class="table" id="catCfdiUsos-table">
        <thead>
            <tr>
                <th>Clavesat</th>
        <th>Descricion</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($catCfdiUsos as $catCfdiUso)
            <tr>
                <td>{{ $catCfdiUso->claveSat }}</td>
            <td>{{ $catCfdiUso->descricion }}</td>
                <td>
                    {!! Form::open(['route' => ['catCfdiUsos.destroy', $catCfdiUso->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('catCfdiUsos.show', [$catCfdiUso->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('catCfdiUsos.edit', [$catCfdiUso->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
