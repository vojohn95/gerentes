<!-- Clavesat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('claveSat', 'Clavesat:') !!}
    {!! Form::text('claveSat', null, ['class' => 'form-control','maxlength' => 10,'maxlength' => 10]) !!}
</div>

<!-- Descricion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descricion', 'Descricion:') !!}
    {!! Form::text('descricion', null, ['class' => 'form-control','maxlength' => 30,'maxlength' => 30]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('catCfdiUsos.index') }}" class="btn btn-default">Cancel</a>
</div>
