<!-- Id Cliente Field -->
<div class="form-group">
    {!! Form::label('id_cliente', 'Id Cliente:') !!}
    <p>{!! $ticket->id_cliente !!}</p>
</div>

<!-- Id Est Field -->
<div class="form-group">
    {!! Form::label('id_est', 'Id Est:') !!}
    <p>{!! $ticket->id_est !!}</p>
</div>

<!-- Id Tipo Field -->
<div class="form-group">
    {!! Form::label('id_tipo', 'Id Tipo:') !!}
    <p>{!! $ticket->id_tipo !!}</p>
</div>

<!-- Id Centraluser Field -->
<div class="form-group">
    {!! Form::label('id_CentralUser', 'Id Centraluser:') !!}
    <p>{!! $ticket->id_CentralUser !!}</p>
</div>

<!-- Factura Field -->
<div class="form-group">
    {!! Form::label('factura', 'Factura:') !!}
    <p>{!! $ticket->factura !!}</p>
</div>

<!-- No Ticket Field -->
<div class="form-group">
    {!! Form::label('no_ticket', 'No Ticket:') !!}
    <p>{!! $ticket->no_ticket !!}</p>
</div>

<!-- Total Ticket Field -->
<div class="form-group">
    {!! Form::label('total_ticket', 'Total Ticket:') !!}
    <p>{!! $ticket->total_ticket !!}</p>
</div>

<!-- Fecha Emision Field -->
<div class="form-group">
    {!! Form::label('fecha_emision', 'Fecha Emision:') !!}
    <p>{!! $ticket->fecha_emision !!}</p>
</div>

<!-- Imagen Field -->
<div class="form-group">
    {!! Form::label('imagen', 'Imagen:') !!}
    <p>{!! $ticket->imagen !!}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{!! $ticket->estatus !!}</p>
</div>

<!-- Usocfdi Field -->
<div class="form-group">
    {!! Form::label('UsoCFDI', 'Usocfdi:') !!}
    <p>{!! $ticket->UsoCFDI !!}</p>
</div>

<!-- Metodo Pago Field -->
<div class="form-group">
    {!! Form::label('metodo_pago', 'Metodo Pago:') !!}
    <p>{!! $ticket->metodo_pago !!}</p>
</div>

<!-- Forma Pago Field -->
<div class="form-group">
    {!! Form::label('forma_pago', 'Forma Pago:') !!}
    <p>{!! $ticket->forma_pago !!}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('RFC', 'Rfc:') !!}
    <p>{!! $ticket->RFC !!}</p>
</div>

<!-- Razon Social Field -->
<div class="form-group">
    {!! Form::label('Razon_social', 'Razon Social:') !!}
    <p>{!! $ticket->Razon_social !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $ticket->email !!}</p>
</div>

<!-- Calle Field -->
<div class="form-group">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{!! $ticket->calle !!}</p>
</div>

<!-- No Ext Field -->
<div class="form-group">
    {!! Form::label('no_ext', 'No Ext:') !!}
    <p>{!! $ticket->no_ext !!}</p>
</div>

<!-- No Int Field -->
<div class="form-group">
    {!! Form::label('no_int', 'No Int:') !!}
    <p>{!! $ticket->no_int !!}</p>
</div>

<!-- Colonia Field -->
<div class="form-group">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{!! $ticket->colonia !!}</p>
</div>

<!-- Municipio Field -->
<div class="form-group">
    {!! Form::label('municipio', 'Municipio:') !!}
    <p>{!! $ticket->municipio !!}</p>
</div>

<!-- Estado Field -->
<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{!! $ticket->estado !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $ticket->cp !!}</p>
</div>

<!-- Coment Field -->
<div class="form-group">
    {!! Form::label('coment', 'Coment:') !!}
    <p>{!! $ticket->coment !!}</p>
</div>

<!-- Timb Field -->
<div class="form-group">
    {!! Form::label('timb', 'Timb:') !!}
    <p>{!! $ticket->timb !!}</p>
</div>

