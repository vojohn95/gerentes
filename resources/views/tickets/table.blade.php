<div class="table-responsive">
    <!--<div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>-->
    <table id="dtBasicExample" class="table table-hover table-bordered results responsive-table text-center">
        <thead>
            <tr>
                <th>Total</th>
                <th>Fecha de solicitud</th>
                <th>Razón Social</th>
                <th>Datos</th>
                <th>Estatus</th>
                <th>Comentario</th>
            </tr>
            <tr class="warning no-result">
                <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                    registro con la información ingresada
                </td>
            </tr>
        </thead>
        <tbody>
            @foreach ($tickets as $ticket)
                <tr>
                    <td>{!! $ticket->total_ticket !!}</td>
                    <td>{!! $ticket->fecha_emision !!}</td>
                    <td>{!! $ticket->Razon_social !!}</td>
                    <td>
                        <a type="button" class="btn-floating btn-sm default-color" data-toggle="modal"
                            data-target="#ModalUser{{ $ticket->id }}"><i class="fas fa-edit"></i></a>
                        <!-- Modal -->
                        <div class="modal fade" id="ModalUser{{ $ticket->id }}" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Usuario</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        {!! Form::open() !!}
                                        <div class="md-form">
                                            {!! Form::label('RFC', 'RFC:') !!}
                                            {!! Form::text('RFC', $ticket->RFC, ['class' => 'form-control', 'disabled']) !!}
                                        </div>
                                        <div class="md-form">
                                            {!! Form::label('RS', 'Razón Social:') !!}
                                            {!! Form::text('RS', $ticket->Razon_social, ['class' => 'form-control', 'disabled']) !!}
                                        </div>
                                        <div class="md-form">
                                            {!! Form::label('Email', 'Email:') !!}
                                            {!! Form::text('Email', $ticket->email, ['class' => 'form-control', 'disabled']) !!}
                                        </div>
                                        <div class="md-form">
                                            {!! Form::label('est', 'Estacionamientos:') !!}
                                            {!! Form::text('est', $ticket->park, ['class' => 'form-control', 'disabled']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        @switch($ticket->estatus)
                            @case(" factura_pi")
                                <h5><span class="badge badge-pill badge-primary">Pendiente</span></h5>
                            @break

                            @case(" valido")
                                <h5><span class="badge badge-pill badge-success">Timbrada</span></h5>
                            @break

                            @default
                                <h5><span class="badge badge-pill badge-danger">Rechazada</span></h5>
                        @endswitch
                    </td>
                    <td>
                        {{ $ticket->coment }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<br>
<br>

<script>
    @push('scripts')
        $(document).ready(function () {
        $('.mdb-select').materialSelect();
        });
    @endpush

</script>
