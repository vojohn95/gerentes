@if (Auth::user()->especial() > 0)
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar black">
        <div class="container smooth-scroll">
            <a class="navbar-brand " href="{{ url('/') }}"><img src="{{ asset('logo/logo/login-logo.png') }}"
                    style="width: 40px;"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="ml-auto navbar-nav">
                    <li class="nav-item">
                        <div class="dropdown nav-link">
                            <a class="btn btn-default btn-rounded btn-sm dropdown-toggle" type="button"
                                id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                    class="fas fa-chevron-circle-down"></i>Menú</a>
                            <div class="dropdown-menu dropdown-default">
                                <a class="dropdown-item" href="{{ url('facturacionespeciales') }}">Facturas</a>
                                <a class="dropdown-item" href="{{ url('reportesespeciales') }}">Reportes</a>
                            </div>
                        </div>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link"><span class="btn btn-default btn-rounded btn-sm">
                                <i class="fas fa-user" aria-hidden="true"></i>{{ Auth::user()->name }}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" type="#logout-form" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                            <span class="btn btn-default btn-rounded btn-sm">
                                <i class="fas fa-power-off" aria-hidden="true"></i>Cerrar sesión</span></a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </div>
        </div>
    </nav>
@else
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar black">
        <div class="container smooth-scroll">
            <a class="navbar-brand " href="{{ url('/') }}"><img src="{{ asset('logo/logo/login-logo.png') }}"
                    style="width: 40px;"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="ml-auto navbar-nav">
                    <li class="nav-item">
                        <div class="dropdown nav-link">
                            <a class="btn btn-default btn-rounded btn-sm dropdown-toggle" type="button"
                                id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                    class="fas fa-chevron-circle-down"></i>Tarifa Horaria</a>
                            <div class="dropdown-menu dropdown-default">
                                <!--<a class="dropdown-item" href="{{ url('/pensions') }}">Pensiones</a>-->
                                <!--<a class="dropdown-item" href="{{ url('/ingresos') }}">Diario de ingreso</a>-->
                                <a class="dropdown-item" href="{{ url('/tickets') }}">Facturas PI</a>
                                <a class="dropdown-item" href="{{ url('/facturas') }}">Buscador</a>
                                <a class="dropdown-item" href="{{ route('informe') }}">Informe</a>

                            </div>
                        </div>

                    </li>
                    <li class="nav-item">
                        <div class="dropdown nav-link">
                            <a class="btn btn-default btn-rounded btn-sm dropdown-toggle" type="button"
                                id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                    class="fas fa-chevron-circle-down"></i>Pensiones</a>
                            <div class="dropdown-menu dropdown-default">
                                <a class="dropdown-item" href="{{ route('detalles.index') }}">Pensiones</a>
                                <a class="dropdown-item" href="{{ route('facturaPensions.index') }}">Facturas</a>
                                <a class="dropdown-item" href="{{ url('/actualizacionesPen') }}">Actualizaciones</a>
                                <a class="dropdown-item" href="{{ route('pagos.index') }}">Pagos</a>
                                <a class="dropdown-item" href="{{ route('reportes.index') }}">Reportes</a>
                                <a class="dropdown-item" href="{{ url('historicoPen') }}">Histórico</a>
                            </div>
                        </div>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link"><span class="btn btn-default btn-rounded btn-sm">
                                <i class="fas fa-user" aria-hidden="true"></i>{{ Auth::user()->name }}</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" type="#logout-form" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <span class="btn btn-default btn-rounded btn-sm">
                                <i class="fas fa-power-off" aria-hidden="true"></i>Cerrar sesión</span></a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </div>
        </div>
    </nav>
@endif
