<li class="{{ Request::is('clientes*') ? 'active' : '' }}">
    <a href="{!! route('clientes.index') !!}"><i class="fa fa-edit"></i><span>Clientes</span></a>
</li>

<li class="{{ Request::is('clienteInfos*') ? 'active' : '' }}">
    <a href="{!! route('clienteInfos.index') !!}"><i class="fa fa-edit"></i><span>Cliente Infos</span></a>
</li>

<li class="{{ Request::is('pensions*') ? 'active' : '' }}">
    <a href="{!! route('pensions.index') !!}"><i class="fa fa-edit"></i><span>Pensions</span></a>
</li>

<li class="{{ Request::is('parks*') ? 'active' : '' }}">
    <a href="{!! route('parks.index') !!}"><i class="fa fa-edit"></i><span>Parks</span></a>
</li>

<li class="{{ Request::is('ingresos*') ? 'active' : '' }}">
    <a href="{!! route('ingresos.index') !!}"><i class="fa fa-edit"></i><span>Ingresos</span></a>
</li>

<li class="{{ Request::is('imgIngresos*') ? 'active' : '' }}">
    <a href="{!! route('imgIngresos.index') !!}"><i class="fa fa-edit"></i><span>Img Ingresos</span></a>
</li>

<li class="{{ Request::is('centralUsers*') ? 'active' : '' }}">
    <a href="{!! route('centralUsers.index') !!}"><i class="fa fa-edit"></i><span>Central Users</span></a>
</li>

<li class="{{ Request::is('ingresos*') ? 'active' : '' }}">
    <a href="{!! route('ingresos.index') !!}"><i class="fa fa-edit"></i><span>Ingresos</span></a>
</li>

<li class="{{ Request::is('bancoColects*') ? 'active' : '' }}">
    <a href="{!! route('bancoColects.index') !!}"><i class="fa fa-edit"></i><span>Banco Colects</span></a>
</li>

<li class="{{ Request::is('empresaColects*') ? 'active' : '' }}">
    <a href="{!! route('empresaColects.index') !!}"><i class="fa fa-edit"></i><span>Empresa Colects</span></a>
</li>

<li class="{{ Request::is('tiposPens*') ? 'active' : '' }}">
    <a href="{!! route('tiposPens.index') !!}"><i class="fa fa-edit"></i><span>Tipos Pens</span></a>
</li>

<li class="{{ Request::is('tipoPagos*') ? 'active' : '' }}">
    <a href="{!! route('tipoPagos.index') !!}"><i class="fa fa-edit"></i><span>Tipo Pagos</span></a>
</li>

<li class="{{ Request::is('formaPagos*') ? 'active' : '' }}">
    <a href="{!! route('formaPagos.index') !!}"><i class="fa fa-edit"></i><span>Forma Pagos</span></a>
</li>

<li class="{{ Request::is('fechaLimites*') ? 'active' : '' }}">
    <a href="{!! route('fechaLimites.index') !!}"><i class="fa fa-edit"></i><span>Fecha Limites</span></a>
</li>

<li class="{{ Request::is('autofTickets*') ? 'active' : '' }}">
    <a href="{!! route('autofTickets.index') !!}"><i class="fa fa-edit"></i><span>Autof Tickets</span></a>
</li>

<li class="{{ Request::is('autoFacts*') ? 'active' : '' }}">
    <a href="{!! route('autoFacts.index') !!}"><i class="fa fa-edit"></i><span>Auto Facts</span></a>
</li>

<li class="{{ Request::is('estacionamientos*') ? 'active' : '' }}">
    <a href="{!! route('estacionamientos.index') !!}"><i class="fa fa-edit"></i><span>Estacionamientos</span></a>
</li>

<li class="{{ Request::is('tickets*') ? 'active' : '' }}">
    <a href="{!! route('tickets.index') !!}"><i class="fa fa-edit"></i><span>Tickets</span></a>
</li>

<li class="{{ Request::is('facturas*') ? 'active' : '' }}">
    <a href="{!! route('facturas.index') !!}"><i class="fa fa-edit"></i><span>Facturas</span></a>
</li>

<li class="{{ Request::is('facturaPensions*') ? 'active' : '' }}">
    <a href="{{ route('facturaPensions.index') }}"><i class="fa fa-edit"></i><span>Factura Pensions</span></a>
</li>

<li class="{{ Request::is('detalles*') ? 'active' : '' }}">
    <a href="{{ route('detalles.index') }}"><i class="fa fa-edit"></i><span>Detalles</span></a>
</li>

<li class="{{ Request::is('pagos*') ? 'active' : '' }}">
    <a href="{{ route('pagos.index') }}"><i class="fa fa-edit"></i><span>Pagos</span></a>
</li>

<li class="{{ Request::is('catBancos*') ? 'active' : '' }}">
    <a href="{{ route('catBancos.index') }}"><i class="fa fa-edit"></i><span>Cat Bancos</span></a>
</li>

<li class="{{ Request::is('catCfdiUsos*') ? 'active' : '' }}">
    <a href="{{ route('catCfdiUsos.index') }}"><i class="fa fa-edit"></i><span>Cat Cfdi Usos</span></a>
</li>

<li class="{{ Request::is('metodoPagos*') ? 'active' : '' }}">
    <a href="{{ route('metodoPagos.index') }}"><i class="fa fa-edit"></i><span>Metodo Pagos</span></a>
</li>

