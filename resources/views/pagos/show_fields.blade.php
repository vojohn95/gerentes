<!-- Total Field -->
<div class="form-group">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $pago->total }}</p>
</div>

<!-- Subtotal Field -->
<div class="form-group">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    <p>{{ $pago->subtotal }}</p>
</div>

<!-- Iva Field -->
<div class="form-group">
    {!! Form::label('iva', 'Iva:') !!}
    <p>{{ $pago->iva }}</p>
</div>

<!-- Tipopago Field -->
<div class="form-group">
    {!! Form::label('tipoPago', 'Tipopago:') !!}
    <p>{{ $pago->tipoPago }}</p>
</div>

<!-- Idpension Field -->
<div class="form-group">
    {!! Form::label('idPension', 'Idpension:') !!}
    <p>{{ $pago->idPension }}</p>
</div>

<!-- Fechapago Field -->
<div class="form-group">
    {!! Form::label('fechaPago', 'Fechapago:') !!}
    <p>{{ $pago->fechaPago }}</p>
</div>

<!-- Id Bancos Field -->
<div class="form-group">
    {!! Form::label('id_bancos', 'Id Bancos:') !!}
    <p>{{ $pago->id_bancos }}</p>
</div>

<!-- Mes Field -->
<div class="form-group">
    {!! Form::label('mes', 'Mes:') !!}
    <p>{{ $pago->mes }}</p>
</div>

<!-- Año Field -->
<div class="form-group">
    {!! Form::label('año', 'Año:') !!}
    <p>{{ $pago->año }}</p>
</div>

<!-- Comprobante Pago Field -->
<div class="form-group">
    {!! Form::label('comprobante_pago', 'Comprobante Pago:') !!}
    <p>{{ $pago->comprobante_pago }}</p>
</div>

<!-- Estatus Factura Field -->
<div class="form-group">
    {!! Form::label('estatus_factura', 'Estatus Factura:') !!}
    <p>{{ $pago->estatus_factura }}</p>
</div>

