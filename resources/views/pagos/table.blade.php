<div class="table-responsive">
    <table id="dtBasicExample" class="table table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Proyecto</th>
                <th>Número Pensión</th>
                <th>Cliente</th>
                <th>RFC</th>
                <th>Factura (S/N)</th>
                <th>Fecha Pago</th>
                <th>Forma Pago</th>
                <th>Total Pago</th>
                <th>Comprobante Pago</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pagos as $pago)
                <tr>
                    <td>{{ $pago->proyecto }}</td>
                    <td>{{ $pago->numero_pension }}</td>
                    <td>{{ $pago->cliente }}</td>
                    <td>{{ $pago->RFC }}</td>
                    <td>{{ $pago->factura_o_no }}</td>
                    <td>
                        {{ date('d/m/Y', strtotime($pago->FechaPago)) }}
                    </td>
                    <td>{{ $pago->FormaPago }}</td>
                    <td>{{ $pago->TotalPago }}</td>
                    <td>
                        <a href="{!! route('compPago', [$pago->idPago]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                class="fas fa-download"></i></a>
                    </td>
                    <td>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                            data-target="#basicExampleModal{{ $pago->idPago }}">
                            Cargar
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="basicExampleModal{{ $pago->idPago }}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="md-form">
                                        {!! Form::model($pago, ['route' => ['pagos.update', $pago->idPago], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Cargar comprobante</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="file-field">
                                                <div class="btn btn-primary btn-sm float-left">
                                                    <span>Elegir archivo</span>
                                                    <input type="file" name="comprobante_pago"
                                                        accept="image/png, image/jpeg" required>
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text"
                                                        placeholder="Seleccione un archivo...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger"
                                                data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-primary">Cargar</button>
                                        </div>
                                        {!! Form::close() !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    {{-- <td>
                        {!! Form::open(['route' => ['pagos.destroy', $pago->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <!--<a href="{{ route('pagos.show', [$pago->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                            <a href="{{ route('pagos.edit', [$pago->id]) }}"
                                class='btn btn-default btn-xs'>Editar</a>
                            {{-- {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td> --}}
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@section('datatable')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                }
            });
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
@endsection
