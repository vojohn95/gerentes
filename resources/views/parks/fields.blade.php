<!-- Id Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_marca', 'Id Marca:') !!}
    {!! Form::number('id_marca', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Org Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_org', 'Id Org:') !!}
    {!! Form::number('id_org', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Dist Regio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dist_regio', 'Dist Regio:') !!}
    {!! Form::text('dist_regio', null, ['class' => 'form-control']) !!}
</div>

<!-- Direccion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('direccion', 'Direccion:') !!}
    {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
</div>

<!-- Calle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('calle', 'Calle:') !!}
    {!! Form::text('calle', null, ['class' => 'form-control']) !!}
</div>

<!-- No Ext Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_ext', 'No Ext:') !!}
    {!! Form::text('no_ext', null, ['class' => 'form-control']) !!}
</div>

<!-- Colonia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('colonia', 'Colonia:') !!}
    {!! Form::text('colonia', null, ['class' => 'form-control']) !!}
</div>

<!-- Municipio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('municipio', 'Municipio:') !!}
    {!! Form::text('municipio', null, ['class' => 'form-control']) !!}
</div>

<!-- Estado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estado', 'Estado:') !!}
    {!! Form::text('estado', null, ['class' => 'form-control']) !!}
</div>

<!-- Cp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cp', 'Cp:') !!}
    {!! Form::text('cp', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitud Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitud', 'Latitud:') !!}
    {!! Form::text('latitud', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitud Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitud', 'Longitud:') !!}
    {!! Form::text('longitud', null, ['class' => 'form-control']) !!}
</div>

<!-- Facturable Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Facturable', 'Facturable:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('Facturable', 0) !!}
        {!! Form::checkbox('Facturable', '1', null) !!} 1
    </label>
</div>

<!-- Automatico Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Automatico', 'Automatico:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('Automatico', 0) !!}
        {!! Form::checkbox('Automatico', '1', null) !!} 1
    </label>
</div>

<!-- Cajones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cajones', 'Cajones:') !!}
    {!! Form::number('cajones', null, ['class' => 'form-control']) !!}
</div>

<!-- Folio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('folio', 'Folio:') !!}
    {!! Form::number('folio', null, ['class' => 'form-control']) !!}
</div>

<!-- Serie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('serie', 'Serie:') !!}
    {!! Form::text('serie', null, ['class' => 'form-control']) !!}
</div>

<!-- Correo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('correo', 'Correo:') !!}
    {!! Form::text('correo', null, ['class' => 'form-control']) !!}
</div>

<!-- Pensiones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pensiones', 'Pensiones:') !!}
    {!! Form::number('pensiones', null, ['class' => 'form-control']) !!}
</div>

<!-- Cortesias Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cortesias', 'Cortesias:') !!}
    {!! Form::number('cortesias', null, ['class' => 'form-control']) !!}
</div>

<!-- Cobro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cobro', 'Cobro:') !!}
    {!! Form::number('cobro', null, ['class' => 'form-control']) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('parks.index') !!}" class="btn btn-default">Cancel</a>
</div>
