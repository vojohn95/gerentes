<!-- No Est Field -->
<div class="form-group">
    {!! Form::label('no_est', 'No Est:') !!}
    <p>{!! $parks->no_est !!}</p>
</div>

<!-- Id Marca Field -->
<div class="form-group">
    {!! Form::label('id_marca', 'Id Marca:') !!}
    <p>{!! $parks->id_marca !!}</p>
</div>

<!-- Id Org Field -->
<div class="form-group">
    {!! Form::label('id_org', 'Id Org:') !!}
    <p>{!! $parks->id_org !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $parks->nombre !!}</p>
</div>

<!-- Dist Regio Field -->
<div class="form-group">
    {!! Form::label('dist_regio', 'Dist Regio:') !!}
    <p>{!! $parks->dist_regio !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{!! $parks->direccion !!}</p>
</div>

<!-- Calle Field -->
<div class="form-group">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{!! $parks->calle !!}</p>
</div>

<!-- No Ext Field -->
<div class="form-group">
    {!! Form::label('no_ext', 'No Ext:') !!}
    <p>{!! $parks->no_ext !!}</p>
</div>

<!-- Colonia Field -->
<div class="form-group">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{!! $parks->colonia !!}</p>
</div>

<!-- Municipio Field -->
<div class="form-group">
    {!! Form::label('municipio', 'Municipio:') !!}
    <p>{!! $parks->municipio !!}</p>
</div>

<!-- Estado Field -->
<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{!! $parks->estado !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $parks->cp !!}</p>
</div>

<!-- Latitud Field -->
<div class="form-group">
    {!! Form::label('latitud', 'Latitud:') !!}
    <p>{!! $parks->latitud !!}</p>
</div>

<!-- Longitud Field -->
<div class="form-group">
    {!! Form::label('longitud', 'Longitud:') !!}
    <p>{!! $parks->longitud !!}</p>
</div>

<!-- Facturable Field -->
<div class="form-group">
    {!! Form::label('Facturable', 'Facturable:') !!}
    <p>{!! $parks->Facturable !!}</p>
</div>

<!-- Automatico Field -->
<div class="form-group">
    {!! Form::label('Automatico', 'Automatico:') !!}
    <p>{!! $parks->Automatico !!}</p>
</div>

<!-- Cajones Field -->
<div class="form-group">
    {!! Form::label('cajones', 'Cajones:') !!}
    <p>{!! $parks->cajones !!}</p>
</div>

<!-- Folio Field -->
<div class="form-group">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{!! $parks->folio !!}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{!! $parks->serie !!}</p>
</div>

<!-- Correo Field -->
<div class="form-group">
    {!! Form::label('correo', 'Correo:') !!}
    <p>{!! $parks->correo !!}</p>
</div>

<!-- Pensiones Field -->
<div class="form-group">
    {!! Form::label('pensiones', 'Pensiones:') !!}
    <p>{!! $parks->pensiones !!}</p>
</div>

<!-- Cortesias Field -->
<div class="form-group">
    {!! Form::label('cortesias', 'Cortesias:') !!}
    <p>{!! $parks->cortesias !!}</p>
</div>

<!-- Cobro Field -->
<div class="form-group">
    {!! Form::label('cobro', 'Cobro:') !!}
    <p>{!! $parks->cobro !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $parks->observaciones !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $parks->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $parks->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $parks->updated_at !!}</p>
</div>

