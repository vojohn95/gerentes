<div class="table-responsive">
    <table class="table" id="parks-table">
        <thead>
            <tr>
                <th>Id Marca</th>
        <th>Id Org</th>
        <th>Nombre</th>
        <th>Dist Regio</th>
        <th>Direccion</th>
        <th>Calle</th>
        <th>No Ext</th>
        <th>Colonia</th>
        <th>Municipio</th>
        <th>Estado</th>
        <th>Cp</th>
        <th>Latitud</th>
        <th>Longitud</th>
        <th>Facturable</th>
        <th>Automatico</th>
        <th>Cajones</th>
        <th>Folio</th>
        <th>Serie</th>
        <th>Correo</th>
        <th>Pensiones</th>
        <th>Cortesias</th>
        <th>Cobro</th>
        <th>Observaciones</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($parks as $parks)
            <tr>
                <td>{!! $parks->id_marca !!}</td>
            <td>{!! $parks->id_org !!}</td>
            <td>{!! $parks->nombre !!}</td>
            <td>{!! $parks->dist_regio !!}</td>
            <td>{!! $parks->direccion !!}</td>
            <td>{!! $parks->calle !!}</td>
            <td>{!! $parks->no_ext !!}</td>
            <td>{!! $parks->colonia !!}</td>
            <td>{!! $parks->municipio !!}</td>
            <td>{!! $parks->estado !!}</td>
            <td>{!! $parks->cp !!}</td>
            <td>{!! $parks->latitud !!}</td>
            <td>{!! $parks->longitud !!}</td>
            <td>{!! $parks->Facturable !!}</td>
            <td>{!! $parks->Automatico !!}</td>
            <td>{!! $parks->cajones !!}</td>
            <td>{!! $parks->folio !!}</td>
            <td>{!! $parks->serie !!}</td>
            <td>{!! $parks->correo !!}</td>
            <td>{!! $parks->pensiones !!}</td>
            <td>{!! $parks->cortesias !!}</td>
            <td>{!! $parks->cobro !!}</td>
            <td>{!! $parks->observaciones !!}</td>
                <td>
                    {!! Form::open(['route' => ['parks.destroy', $parks->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('parks.show', [$parks->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('parks.edit', [$parks->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
