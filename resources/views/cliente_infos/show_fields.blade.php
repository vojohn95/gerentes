<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $clienteInfos->id !!}</p>
</div>

<!-- Id Cliente Field -->
<div class="form-group">
    {!! Form::label('id_cliente', 'Id Cliente:') !!}
    <p>{!! $clienteInfos->id_cliente !!}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('RFC', 'Rfc:') !!}
    <p>{!! $clienteInfos->RFC !!}</p>
</div>

<!-- Razon Social Field -->
<div class="form-group">
    {!! Form::label('Razon_social', 'Razon Social:') !!}
    <p>{!! $clienteInfos->Razon_social !!}</p>
</div>

<!-- Calle Field -->
<div class="form-group">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{!! $clienteInfos->calle !!}</p>
</div>

<!-- No Ext Field -->
<div class="form-group">
    {!! Form::label('no_ext', 'No Ext:') !!}
    <p>{!! $clienteInfos->no_ext !!}</p>
</div>

<!-- No Int Field -->
<div class="form-group">
    {!! Form::label('no_int', 'No Int:') !!}
    <p>{!! $clienteInfos->no_int !!}</p>
</div>

<!-- Colonia Field -->
<div class="form-group">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{!! $clienteInfos->colonia !!}</p>
</div>

<!-- Municipio Field -->
<div class="form-group">
    {!! Form::label('municipio', 'Municipio:') !!}
    <p>{!! $clienteInfos->municipio !!}</p>
</div>

<!-- Estado Field -->
<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{!! $clienteInfos->estado !!}</p>
</div>

<!-- Pais Field -->
<div class="form-group">
    {!! Form::label('pais', 'Pais:') !!}
    <p>{!! $clienteInfos->pais !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $clienteInfos->cp !!}</p>
</div>

<!-- Act Na Field -->
<div class="form-group">
    {!! Form::label('act_na', 'Act Na:') !!}
    <p>{!! $clienteInfos->act_na !!}</p>
</div>

<!-- Ine Field -->
<div class="form-group">
    {!! Form::label('ine', 'Ine:') !!}
    <p>{!! $clienteInfos->ine !!}</p>
</div>

<!-- Tar Circ Field -->
<div class="form-group">
    {!! Form::label('tar_circ', 'Tar Circ:') !!}
    <p>{!! $clienteInfos->tar_circ !!}</p>
</div>

<!-- Placas Field -->
<div class="form-group">
    {!! Form::label('placas', 'Placas:') !!}
    <p>{!! $clienteInfos->placas !!}</p>
</div>

<!-- Tel Field -->
<div class="form-group">
    {!! Form::label('tel', 'Tel:') !!}
    <p>{!! $clienteInfos->tel !!}</p>
</div>

<!-- Pension Field -->
<div class="form-group">
    {!! Form::label('pension', 'Pension:') !!}
    <p>{!! $clienteInfos->pension !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $clienteInfos->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $clienteInfos->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $clienteInfos->updated_at !!}</p>
</div>

