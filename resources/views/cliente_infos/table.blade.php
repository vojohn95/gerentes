<div class="table-responsive">
    <table class="table" id="clienteInfos-table">
        <thead>
            <tr>
                <th>Id Cliente</th>
        <th>Rfc</th>
        <th>Razon Social</th>
        <th>Calle</th>
        <th>No Ext</th>
        <th>No Int</th>
        <th>Colonia</th>
        <th>Municipio</th>
        <th>Estado</th>
        <th>Pais</th>
        <th>Cp</th>
        <th>Act Na</th>
        <th>Ine</th>
        <th>Tar Circ</th>
        <th>Placas</th>
        <th>Tel</th>
        <th>Pension</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($clienteInfos as $clienteInfos)
            <tr>
                <td>{!! $clienteInfos->id_cliente !!}</td>
            <td>{!! $clienteInfos->RFC !!}</td>
            <td>{!! $clienteInfos->Razon_social !!}</td>
            <td>{!! $clienteInfos->calle !!}</td>
            <td>{!! $clienteInfos->no_ext !!}</td>
            <td>{!! $clienteInfos->no_int !!}</td>
            <td>{!! $clienteInfos->colonia !!}</td>
            <td>{!! $clienteInfos->municipio !!}</td>
            <td>{!! $clienteInfos->estado !!}</td>
            <td>{!! $clienteInfos->pais !!}</td>
            <td>{!! $clienteInfos->cp !!}</td>
            <td>{!! $clienteInfos->act_na !!}</td>
            <td>{!! $clienteInfos->ine !!}</td>
            <td>{!! $clienteInfos->tar_circ !!}</td>
            <td>{!! $clienteInfos->placas !!}</td>
            <td>{!! $clienteInfos->tel !!}</td>
            <td>{!! $clienteInfos->pension !!}</td>
                <td>
                    {!! Form::open(['route' => ['clienteInfos.destroy', $clienteInfos->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('clienteInfos.show', [$clienteInfos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('clienteInfos.edit', [$clienteInfos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
