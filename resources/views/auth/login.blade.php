@extends('layouts.app')

@section('title', 'Inicio de sesión')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <div class="card">

                <h5 class="card-header default-color white-text text-center py-4">
                    <strong>Inicio de sesión</strong>
                </h5>

                <!--Card content-->
                <div class="card-body px-lg-5 pt-0">

                    <!-- Form -->
                    <form class="text-center" style="color: #757575;" action="{{url('/login')}}" method="post">
                    {!! csrf_field() !!}

                    <!-- Email -->
                        <div class="md-form {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" id="materialLoginFormEmail" class="form-control" name="email"
                                   value="{{ old('email') }}">
                            <label for="materialLoginFormEmail">Usuario</label>
                        </div>

                        <!-- Password -->
                        <div class="md-form {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" id="materialLoginFormPassword" class="form-control" name="password">
                            <label for="materialLoginFormPassword">Password</label>
                        </div>

                        <div class="d-flex justify-content-around">
                            <div>
                                <!-- Remember me -->
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="materialLoginFormRemember">
                                    <!--<label class="form-check-label" for="materialLoginFormRemember">Remember me</label>-->
                                </div>
                            </div>
                            <div>
                                <!-- Forgot password -->
                                <!--<a href="">Forgot password?</a>-->
                            </div>
                        </div>

                        <!-- Sign in button -->
                        <button class="btn btn-rounded btn-default my-4 waves-effect z-depth-0"
                                type="submit">Entrar
                        </button>

                        @include('flash::message')
                        @include('layouts.errors')

                    </form>
                    <!-- Form -->

                </div>

            </div>
        </div>
    </div>
@endsection()
