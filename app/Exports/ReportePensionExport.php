<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;


class ReportePensionExport implements FromArray, WithHeadings, ShouldAutoSize, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public function __construct(int $estacionamiento)
    {
        $this->estacionamiento = $estacionamiento;
    }

    public function array(): array
    {

        $reporte = "select dis.clave_distrito as distrito,
        dis.nombre as distrital,
        grt.name as gerente,
        grt.email as \"correo de gerente\",
        est.no_estacionamiento as \"Número de Proyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"Número de Pensión\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombre de pensionado\",
        case when cl.estatus = 'A' then 'Activa'
        when cl.estatus = 'I' then 'Inactiva'
        end as estado,
        if(pd.contrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as contrato,
        if(pd.solicitudContrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as solicitudContrato,
        if(pd.comprobanteDomicilio is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as comprobanteDomicilio,
        if(pd.ine is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as ine,
        if(pd.licencia is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as licencia,
        if(pd.rfc is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as rfc,
        if(pd.tarjetaCirculacion is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as tarjetaCirculacion
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id_cliente = cl.id inner join distritales dis on dis.id = grt.id_distritales
        where  grt.id = 79";
        #where  grt.id = $this->estacionamiento";
        $result1 = DB::connection('mysql2')->SELECT($reporte);

        return $result1;
    }

    public function headings(): array
    {
        return [
            'Distrito',
            'Distrital',
            'Gerente',
            'Correo de gerente',
            'Numero de proyecto',
            'Proyecto',
            'Numero de pensión',
            'Nombre de pensionado',
            'Estado',
            'Contrato',
            'Solicitud Contrato',
            'Comprobante domicilio',
            'Ine',
            'Licencia',
            'RFC',
            'Tarjeta circulación',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
