<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ReportEspecialExport implements FromArray, WithHeadings, ShouldAutoSize, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public function __construct(string $fechaInicio, string $fechaFin)
    {
        $this->fechaInicio = $fechaInicio;
        $this->fechaFin = $fechaFin;
        $this->id_est = Auth::user()->especialData()['0']->id_parks;
    }

    public function array(): array
    {
        /*$reporte = 'SELECT distinct(B.folio), B.serie,
        B.uuid,
       date_format(B.FECHA_TIMBRADO, "%Y-%m-%d") fecha,
       A.Razon_social,
       A.RFC,
       round((B.total_factura / 1.16),2) subtotal_factura,
       round(((B.total_factura / 1.16)*0.16),2)iva_factura,
       B.total_factura, B.total_factura
       FROM central_aplicativo.autof_tickets  A
       right join
       central_aplicativo.auto_facts  B
       on (A.id = B.id_ticketAF)
       where A.id_est = "926"
       and B.created_at BETWEEN ".$this." AND "2021-08-26 23:59:59"
       order by fecha, folio ASC';*/
        $reporte = 'SELECT B.serie,B.folio,
       #B.uuid,
      date_format(B.FECHA_TIMBRADO, "%Y-%m-%d") fecha,
      A.no_ticket,
          A.RFC,
      A.Razon_social,
      round((B.total_factura / 1.16),2) subtotal_factura,
      round(((B.total_factura / 1.16)*0.16),2)iva_factura,
      B.total_factura, B.total_factura
      FROM central_aplicativo.autof_tickets  A
      right join
      central_aplicativo.auto_facts  B
      on (A.id = B.id_ticketAF)
      where A.id_est = ' . $this->id_est . '
      and B.created_at BETWEEN "' . $this->fechaInicio . ' 00:00:00" AND "' . $this->fechaFin . ' 23:59:59"
      order by fecha, folio ASC';
        #where  grt.id = $this->estacionamiento";
        $result1 = DB::SELECT($reporte);

        /*foreach($result1 as $key => $value){
            echo $value->total;
        }

        dd($value->total);*/

        return $result1;
    }

    public function headings(): array
    {
        return [
            'Serie',
            'Folio',
            //'UUID',
            'Fecha',
            'No. Ticket',
            'RFC',
            'Razón social',
            'Subtotal',
            'IVA',
            'Total',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
