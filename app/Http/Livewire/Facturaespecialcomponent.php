<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\facturas;
use App\Models\GerentesPark;
use Illuminate\Support\Facades\Auth;

class Facturaespecialcomponent extends Component
{
    public $relacion_fact;
    public $select_id = "";

    public function updatedSelectId($value)
    {
        $this->select_id = $value;
    }

    public function render()
    {
        $relacion_fact = GerentesPark::select('id_parks')->where('id_gerentes', Auth::user()->id)->get();
        if ($this->select_id == "") {
            $facturas = facturas::join('autof_tickets', 'autof_tickets.id', '=', 'auto_facts.id_ticketAF')
                ->select([
                    'auto_facts.id',
                    'auto_facts.serie',
                    'auto_facts.folio',
                    'auto_facts.fecha_timbrado',
                    'auto_facts.subtotal_factura',
                    'auto_facts.iva_factura',
                    'auto_facts.total_factura',
                    'autof_tickets.email',
                    'autof_tickets.Razon_social',
                    'autof_tickets.RFC',
                    'autof_tickets.no_ticket',
                ])
                //->where('autof_tickets.id_est','=',Auth::user()->id_est)
                ->whereIn('autof_tickets.id_est', $relacion_fact)
                ->orderBy('auto_facts.created_at', 'desc')
                ->get();
        } elseif ($this->select_id == "all") {
            $facturas = facturas::join('autof_tickets', 'autof_tickets.id', '=', 'auto_facts.id_ticketAF')
                ->select([
                    'auto_facts.id',
                    'auto_facts.serie',
                    'auto_facts.folio',
                    'auto_facts.fecha_timbrado',
                    'auto_facts.subtotal_factura',
                    'auto_facts.iva_factura',
                    'auto_facts.total_factura',
                    'autof_tickets.email',
                    'autof_tickets.Razon_social',
                    'autof_tickets.RFC',
                    'autof_tickets.no_ticket',
                ])
                //->where('autof_tickets.id_est','=',Auth::user()->id_est)
                ->whereIn('autof_tickets.id_est', $relacion_fact)
                ->orderBy('auto_facts.created_at', 'desc')
                ->get();
        } else {
            $facturas = facturas::join('autof_tickets', 'autof_tickets.id', '=', 'auto_facts.id_ticketAF')
                ->select([
                    'auto_facts.id',
                    'auto_facts.serie',
                    'auto_facts.folio',
                    'auto_facts.fecha_timbrado',
                    'auto_facts.subtotal_factura',
                    'auto_facts.iva_factura',
                    'auto_facts.total_factura',
                    'autof_tickets.email',
                    'autof_tickets.Razon_social',
                    'autof_tickets.RFC',
                ])
                ->where('autof_tickets.id_est', '=', $this->select_id)
                //->whereIn('autof_tickets.id_est', $this->select_id)
                ->orderBy('auto_facts.created_at', 'desc')
                ->get();
        }

        return view('livewire.facturaespecialcomponent', [
            'facturas' => $facturas,
            'estacionamientos' => $relacion_fact,
        ]);
    }

    public function mount()
    {
    }
}
