<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateImgIngresosRequest;
use App\Http\Requests\UpdateImgIngresosRequest;
use App\Repositories\ImgIngresosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ImgIngresosController extends AppBaseController
{
    /** @var  ImgIngresosRepository */
    private $imgIngresosRepository;

    public function __construct(ImgIngresosRepository $imgIngresosRepo)
    {
        $this->imgIngresosRepository = $imgIngresosRepo;
    }

    /**
     * Display a listing of the ImgIngresos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $imgIngresos = $this->imgIngresosRepository->all();

        return view('img_ingresos.index')
            ->with('imgIngresos', $imgIngresos);
    }

    /**
     * Show the form for creating a new ImgIngresos.
     *
     * @return Response
     */
    public function create()
    {
        return view('img_ingresos.create');
    }

    /**
     * Store a newly created ImgIngresos in storage.
     *
     * @param CreateImgIngresosRequest $request
     *
     * @return Response
     */
    public function store(CreateImgIngresosRequest $request)
    {
        $input = $request->all();

        $imgIngresos = $this->imgIngresosRepository->create($input);

        Flash::success('Img Ingresos saved successfully.');

        return redirect(route('imgIngresos.index'));
    }

    /**
     * Display the specified ImgIngresos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $imgIngresos = $this->imgIngresosRepository->find($id);

        if (empty($imgIngresos)) {
            Flash::error('Img Ingresos not found');

            return redirect(route('imgIngresos.index'));
        }

        return view('img_ingresos.show')->with('imgIngresos', $imgIngresos);
    }

    /**
     * Show the form for editing the specified ImgIngresos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $imgIngresos = $this->imgIngresosRepository->find($id);

        if (empty($imgIngresos)) {
            Flash::error('Img Ingresos not found');

            return redirect(route('imgIngresos.index'));
        }

        return view('img_ingresos.edit')->with('imgIngresos', $imgIngresos);
    }

    /**
     * Update the specified ImgIngresos in storage.
     *
     * @param int $id
     * @param UpdateImgIngresosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImgIngresosRequest $request)
    {
        $imgIngresos = $this->imgIngresosRepository->find($id);

        if (empty($imgIngresos)) {
            Flash::error('Img Ingresos not found');

            return redirect(route('imgIngresos.index'));
        }

        $imgIngresos = $this->imgIngresosRepository->update($request->all(), $id);

        Flash::success('Img Ingresos updated successfully.');

        return redirect(route('imgIngresos.index'));
    }

    /**
     * Remove the specified ImgIngresos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $imgIngresos = $this->imgIngresosRepository->find($id);

        if (empty($imgIngresos)) {
            Flash::error('Img Ingresos not found');

            return redirect(route('imgIngresos.index'));
        }

        $this->imgIngresosRepository->delete($id);

        Flash::success('Img Ingresos deleted successfully.');

        return redirect(route('imgIngresos.index'));
    }
}
