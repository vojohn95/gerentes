<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecatBancoRequest;
use App\Http\Requests\UpdatecatBancoRequest;
use App\Repositories\catBancoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class catBancoController extends AppBaseController
{
    /** @var  catBancoRepository */
    private $catBancoRepository;

    public function __construct(catBancoRepository $catBancoRepo)
    {
        $this->catBancoRepository = $catBancoRepo;
    }

    /**
     * Display a listing of the catBanco.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $catBancos = $this->catBancoRepository->all();

        return view('cat_bancos.index')
            ->with('catBancos', $catBancos);
    }

    /**
     * Show the form for creating a new catBanco.
     *
     * @return Response
     */
    public function create()
    {
        return view('cat_bancos.create');
    }

    /**
     * Store a newly created catBanco in storage.
     *
     * @param CreatecatBancoRequest $request
     *
     * @return Response
     */
    public function store(CreatecatBancoRequest $request)
    {
        $input = $request->all();

        $catBanco = $this->catBancoRepository->create($input);

        Flash::success('Cat Banco saved successfully.');

        return redirect(route('catBancos.index'));
    }

    /**
     * Display the specified catBanco.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $catBanco = $this->catBancoRepository->find($id);

        if (empty($catBanco)) {
            Flash::error('Cat Banco not found');

            return redirect(route('catBancos.index'));
        }

        return view('cat_bancos.show')->with('catBanco', $catBanco);
    }

    /**
     * Show the form for editing the specified catBanco.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $catBanco = $this->catBancoRepository->find($id);

        if (empty($catBanco)) {
            Flash::error('Cat Banco not found');

            return redirect(route('catBancos.index'));
        }

        return view('cat_bancos.edit')->with('catBanco', $catBanco);
    }

    /**
     * Update the specified catBanco in storage.
     *
     * @param int $id
     * @param UpdatecatBancoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecatBancoRequest $request)
    {
        $catBanco = $this->catBancoRepository->find($id);

        if (empty($catBanco)) {
            Flash::error('Cat Banco not found');

            return redirect(route('catBancos.index'));
        }

        $catBanco = $this->catBancoRepository->update($request->all(), $id);

        Flash::success('Cat Banco updated successfully.');

        return redirect(route('catBancos.index'));
    }

    /**
     * Remove the specified catBanco from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $catBanco = $this->catBancoRepository->find($id);

        if (empty($catBanco)) {
            Flash::error('Cat Banco not found');

            return redirect(route('catBancos.index'));
        }

        $this->catBancoRepository->delete($id);

        Flash::success('Cat Banco deleted successfully.');

        return redirect(route('catBancos.index'));
    }
}
