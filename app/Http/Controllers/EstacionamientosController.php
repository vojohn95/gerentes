<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEstacionamientosRequest;
use App\Http\Requests\UpdateEstacionamientosRequest;
use App\Repositories\EstacionamientosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class EstacionamientosController extends AppBaseController
{
    /** @var  EstacionamientosRepository */
    private $estacionamientosRepository;

    public function __construct(EstacionamientosRepository $estacionamientosRepo)
    {
        $this->estacionamientosRepository = $estacionamientosRepo;
    }

    /**
     * Display a listing of the Estacionamientos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $estacionamientos = $this->estacionamientosRepository->all();

        return view('estacionamientos.index')
            ->with('estacionamientos', $estacionamientos);
    }

    /**
     * Show the form for creating a new Estacionamientos.
     *
     * @return Response
     */
    public function create()
    {
        return view('estacionamientos.create');
    }

    /**
     * Store a newly created Estacionamientos in storage.
     *
     * @param CreateEstacionamientosRequest $request
     *
     * @return Response
     */
    public function store(CreateEstacionamientosRequest $request)
    {
        $input = $request->all();

        $estacionamientos = $this->estacionamientosRepository->create($input);

        Flash::success('Estacionamientos saved successfully.');

        return redirect(route('estacionamientos.index'));
    }

    /**
     * Display the specified Estacionamientos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $estacionamientos = $this->estacionamientosRepository->find($id);

        if (empty($estacionamientos)) {
            Flash::error('Estacionamientos not found');

            return redirect(route('estacionamientos.index'));
        }

        return view('estacionamientos.show')->with('estacionamientos', $estacionamientos);
    }

    /**
     * Show the form for editing the specified Estacionamientos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estacionamientos = $this->estacionamientosRepository->find($id);

        if (empty($estacionamientos)) {
            Flash::error('Estacionamientos not found');

            return redirect(route('estacionamientos.index'));
        }

        return view('estacionamientos.edit')->with('estacionamientos', $estacionamientos);
    }

    /**
     * Update the specified Estacionamientos in storage.
     *
     * @param int $id
     * @param UpdateEstacionamientosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEstacionamientosRequest $request)
    {
        $estacionamientos = $this->estacionamientosRepository->find($id);

        if (empty($estacionamientos)) {
            Flash::error('Estacionamientos not found');

            return redirect(route('estacionamientos.index'));
        }

        $estacionamientos = $this->estacionamientosRepository->update($request->all(), $id);

        Flash::success('Estacionamientos updated successfully.');

        return redirect(route('estacionamientos.index'));
    }

    /**
     * Remove the specified Estacionamientos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $estacionamientos = $this->estacionamientosRepository->find($id);

        if (empty($estacionamientos)) {
            Flash::error('Estacionamientos not found');

            return redirect(route('estacionamientos.index'));
        }

        $this->estacionamientosRepository->delete($id);

        Flash::success('Estacionamientos deleted successfully.');

        return redirect(route('estacionamientos.index'));
    }
}
