<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePensionsRequest;
use App\Http\Requests\UpdatePensionsRequest;
use App\Models\ClienteInfos;
use App\Models\Fecha_limite;
use App\Models\FormaPago;
use App\Models\Pensions;
use App\Models\TipoPago;
use App\Models\TiposPen;
use App\Repositories\PensionsRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Cliente;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;

class PensionsController extends AppBaseController
{
    /** @var  PensionsRepository */
    private $pensionsRepository;

    public function __construct(PensionsRepository $pensionsRepo)
    {
        $this->pensionsRepository = $pensionsRepo;
    }

    /**
     * Display a listing of the Pensions.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pensions = $this->pensionsRepository->all();

        return view('pensions.index')
            ->with('pensions', $pensions);
    }

    /**
     * Show the form for creating a new Pensions.
     *
     * @return Response
     */
    public function create()
    {
        $fechas = Fecha_limite::all();
        $formasPago = FormaPago::all();
        $tiposPago = TipoPago::all();
        $tiposPen = TiposPen::all();
        return view('pensions.create')
            ->with('fechas',$fechas)
            ->with('tiposPago',$tiposPago)
            ->with('tiposPen',$tiposPen)
            ->with('formaPago',$formasPago);
    }

    /**
     * Store a newly created Pensions in storage.
     *
     * @param CreatePensionsRequest $request
     *
     * @return Response
     */
    public function store()
    {
        $input = \request()->all();
        $input = request()->validate([
            "Numero_pension" => ['integer','required'],
            "Costo_Pension" => ['integer','required',],
            "Recargos" => ['integer','required'],
            "Numero_estacionamiento" => ['integer','required'],
            "venta_tarjeta" => ['integer','required'],
            "reposicion_tarjeta" => ['integer','required'],
            "importe_pago" => ['numeric','required'],
            "tipo_pension" => ['integer', 'required'],
            "tipo_pago" => ['integer', 'required'],
            "forma_pago" => ['integer', 'required'],
            "mes_pago" => ['integer', 'required'],
            "fecha_limite_pago" => ['integer', 'required'],
            "requiere_factura" => ['integer', 'required'],
            "Nombre_pensionado" => ['string', 'required'],
            "Rfc" => ['string','required','max:13'],
            "Telefono" => ['integer', 'required'],
            "email" => ['email', 'required']
        ]);

        $upper_rfc = strtoupper($input['Rfc']);


        $usuarios = DB::table('users')
            ->where('email', '=', $input['email'])
            ->get();

        if ($usuarios->isEmpty()) {
            $user = new Cliente();
            $user->fill([
                'name' => $input['Nombre_pensionado'],
                'password' => 'sin_contrasena',
                'email' => $input['email']
            ]);
            $user->save();
            $user_info = new ClienteInfos();
            $user_info->fill([
                'id_cliente' => $user->id,
                'RFC' => $upper_rfc,
                'Razon_social' => $input['Nombre_pensionado'],
                'pension' => True,
            ]);
            $user_info->save();
            $pension = new Pensions();
            $pension->fill([
                'id_cliente'=> $user->id,
                'id_tipo_pen' => $input['tipo_pension'],
                'id_tipo_pago' => $input['tipo_pago'],
                'id_forma_pago' => $input['forma_pago'],
                'id_fecha_limite' => $input['fecha_limite_pago'],
                'num_pen' => $input['Numero_pension'],
                'no_est' => $input['Numero_estacionamiento'],
                'factura' => $input['requiere_factura'],
                'costo_pension' => $input['Costo_Pension'],
                'recargos' => $input['Recargos'],
                'venta_tarjeta' => $input['venta_tarjeta'],
                'repo_tarjeta' => $input['reposicion_tarjeta'],
                'impor_pago' => $input['importe_pago'],
                'mes_pago' => $input['mes_pago'],
                'cargado_por' => Auth::user()->id,
            ]);
            $pension->save();
        }else{
            $InfoCliente = ClienteInfos::find($usuarios[0]->id);
            $InfoCliente->pension = true;
            $InfoCliente->save();
            $pension = new Pensions();
            $pension->fill([
                'id_cliente'=> $usuarios[0]->id,
                'id_tipo_pen' => $input['tipo_pension'],
                'id_tipo_pago' => $input['tipo_pago'],
                'id_forma_pago' => $input['forma_pago'],
                'id_fecha_limite' => $input['fecha_limite_pago'],
                'num_pen' => $input['Numero_pension'],
                'no_est' => $input['Numero_estacionamiento'],
                'factura' => $input['requiere_factura'],
                'costo_pension' => $input['Costo_Pension'],
                'recargos' => $input['Recargos'],
                'venta_tarjeta' => $input['venta_tarjeta'],
                'repo_tarjeta' => $input['reposicion_tarjeta'],
                'impor_pago' => $input['importe_pago'],
                'mes_pago' => $input['mes_pago'],
                'cargado_por' => Auth::user()->id,
            ]);
            $pension->save();
        }
        //$pensions = $this->pensionsRepository->create($input);
        Flash::success('Pensionado guardado satisfactoriamente.');
        return redirect(route('pensions.index'));
    }

    /**
     * Display the specified Pensions.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pensions = $this->pensionsRepository->find($id);

        if (empty($pensions)) {
            Flash::error('Pensions not found');

            return redirect(route('pensions.index'));
        }

        return view('pensions.show')->with('pensions', $pensions);
    }

    /**
     * Show the form for editing the specified Pensions.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pensions = $this->pensionsRepository->find($id);

        if (empty($pensions)) {
            Flash::error('Pensions not found');

            return redirect(route('pensions.index'));
        }

        return view('pensions.edit')->with('pensions', $pensions);
    }

    /**
     * Update the specified Pensions in storage.
     *
     * @param int $id
     * @param UpdatePensionsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePensionsRequest $request)
    {
        $pensions = $this->pensionsRepository->find($id);

        if (empty($pensions)) {
            Flash::error('Pensions not found');

            return redirect(route('pensions.index'));
        }

        $pensions = $this->pensionsRepository->update($request->all(), $id);

        Flash::success('Pensions updated successfully.');

        return redirect(route('pensions.index'));
    }

    /**
     * Remove the specified Pensions from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pensions = $this->pensionsRepository->find($id);

        if (empty($pensions)) {
            Flash::error('Pensions not found');

            return redirect(route('pensions.index'));
        }

        $this->pensionsRepository->delete($id);

        Flash::success('Pensions deleted successfully.');

        return redirect(route('pensions.index'));
    }
}
