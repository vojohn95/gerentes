<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Models\historico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HistoricoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_est = Auth::user()->id_est;
        $date = Carbon::now();
        $hoy = $date->format('Y-m-d');

        $pensionActiva = "
        select dis.clave_distrito as distrito,
        dis.nombre as distrital,
        grt.name as gerente,
        grt.email as \"correogerente\",
        est.no_estacionamiento as \"NumeroProyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"NumeroPension\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombrepensionado\",
        'Inactiva' as \"Estado\",
        pd.fechaInactivacion as \"fechabajacancelacion\",
        if(pd.contrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoContrato\",
        if(pd.solicitudContrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoSolicitud\",
        if(pd.comprobanteDomicilio is null,  \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"ComprobanteDomicilio\",
        if(pd.ine is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoINE\",
        if(pd.licencia is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoLicencia\",
        if(pd.rfc is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoRFC\",
        if(pd.tarjetaCirculacion is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoTarjetaCirculacion\"
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id_cliente = cl.id inner join distritales dis on dis.id = grt.id_distritales
        where  est.id = 8 #id de estacionamiento a consultar
        and pd.status = 0 #cambiar a 0 cuando inactivas
        and pd.fechaInactivacion is not null
        and date_format(pd.created_at, '%Y-%m-%d') between '2020-01-01' and '$hoy'
        UNION
        #Reporte de pensiones canceladas por ESTACIONAMIENTO
        select dis.clave_distrito as distrito,
        dis.nombre as distrital,
        grt.name as gerente,
        grt.email as \"correogerente\",
        est.no_estacionamiento as \"NumeroProyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"NumeroPension\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombrepensionado\",
        'Cancelada' as \"Estado\",
        pd.fechaCancelacion as \"fechabajacancelacion\",
        if(pd.contrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoContrato\",
        if(pd.solicitudContrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoSolicitud\",
        if(pd.comprobanteDomicilio is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"ComprobanteDomicilio\",
        if(pd.ine is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoINE\",
        if(pd.licencia is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoLicencia\",
        if(pd.rfc is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoRFC\",
        if(pd.tarjetaCirculacion is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoTarjetaCirculacion\"
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id_cliente = cl.id inner join distritales dis on dis.id = grt.id_distritales
        where  est.id = 8  #id de estacionamiento a consultar
        and pd.status = 0 #cambiar a 0 cuando inactivas
        and fechaCancelacion is not null
        and date_format(pd.created_at, '%Y-%m-%d')  between '2020-01-01' and '$hoy'
        union
        #Reporte de pensiones activas por estacionamiento
        select dis.clave_distrito as distrito,
        dis.nombre as distrital,
        grt.name as gerente,
        grt.email as \"correogerente\",
        est.no_estacionamiento as \"NumeroProyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"NumeroPension\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombrepensionado\",
        if( cl.estatus = 1 && (date_format(pd.created_at, '%Y-%m')) = (date_format(now(), '%Y-%m')), \"Nueva\", \"Activa\") as \"estado\",
        pd.fechaCancelacion as \"fechabajacancelacion\",
        if(pd.contrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoContrato\",
        if(pd.solicitudContrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoSolicitud\",
        if(pd.comprobanteDomicilio is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"ComprobanteDomicilio\",
        if(pd.ine is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoINE\",
        if(pd.licencia is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoLicencia\",
        if(pd.rfc is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoRFC\",
        if(pd.tarjetaCirculacion is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoTarjetaCirculacion\"
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id_cliente = cl.id  inner join distritales dis on dis.id = grt.id_distritales
        where  est.id = 2 #id de estacionamiento a consultar
        and pd.status = 1 #estatus de pensión al momento de la consulta
        and fechaCancelacion is null and pd.fechaInactivacion is null
        and date_format(pd.created_at, '%Y-%m-%d') between '2020-01-01' and '$hoy'
        ";

        $result1 = DB::connection('mysql2')->SELECT($pensionActiva);
        //dd($result1);

        return view('historico.index')->with('detalles', $result1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function show(historico $historico)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function edit(historico $historico)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, historico $historico)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function destroy(historico $historico)
    {
        //
    }
}
