<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClienteInfosRequest;
use App\Http\Requests\UpdateClienteInfosRequest;
use App\Repositories\ClienteInfosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ClienteInfosController extends AppBaseController
{
    /** @var  ClienteInfosRepository */
    private $clienteInfosRepository;

    public function __construct(ClienteInfosRepository $clienteInfosRepo)
    {
        $this->clienteInfosRepository = $clienteInfosRepo;
    }

    /**
     * Display a listing of the ClienteInfos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $clienteInfos = $this->clienteInfosRepository->all();

        return view('cliente_infos.index')
            ->with('clienteInfos', $clienteInfos);
    }

    /**
     * Show the form for creating a new ClienteInfos.
     *
     * @return Response
     */
    public function create()
    {
        return view('cliente_infos.create');
    }

    /**
     * Store a newly created ClienteInfos in storage.
     *
     * @param CreateClienteInfosRequest $request
     *
     * @return Response
     */
    public function store(CreateClienteInfosRequest $request)
    {
        $input = $request->all();

        $clienteInfos = $this->clienteInfosRepository->create($input);

        Flash::success('Cliente Infos saved successfully.');

        return redirect(route('clienteInfos.index'));
    }

    /**
     * Display the specified ClienteInfos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $clienteInfos = $this->clienteInfosRepository->find($id);

        if (empty($clienteInfos)) {
            Flash::error('Cliente Infos not found');

            return redirect(route('clienteInfos.index'));
        }

        return view('cliente_infos.show')->with('clienteInfos', $clienteInfos);
    }

    /**
     * Show the form for editing the specified ClienteInfos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $clienteInfos = $this->clienteInfosRepository->find($id);

        if (empty($clienteInfos)) {
            Flash::error('Cliente Infos not found');

            return redirect(route('clienteInfos.index'));
        }

        return view('cliente_infos.edit')->with('clienteInfos', $clienteInfos);
    }

    /**
     * Update the specified ClienteInfos in storage.
     *
     * @param int $id
     * @param UpdateClienteInfosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClienteInfosRequest $request)
    {
        $clienteInfos = $this->clienteInfosRepository->find($id);

        if (empty($clienteInfos)) {
            Flash::error('Cliente Infos not found');

            return redirect(route('clienteInfos.index'));
        }

        $clienteInfos = $this->clienteInfosRepository->update($request->all(), $id);

        Flash::success('Cliente Infos updated successfully.');

        return redirect(route('clienteInfos.index'));
    }

    /**
     * Remove the specified ClienteInfos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clienteInfos = $this->clienteInfosRepository->find($id);

        if (empty($clienteInfos)) {
            Flash::error('Cliente Infos not found');

            return redirect(route('clienteInfos.index'));
        }

        $this->clienteInfosRepository->delete($id);

        Flash::success('Cliente Infos deleted successfully.');

        return redirect(route('clienteInfos.index'));
    }
}
