<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateticketRequest;
use App\Http\Requests\UpdateticketRequest;
use App\Models\Estacionamiento;
use App\Repositories\ticketRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Estacionamientos;
use App\Models\ticket;
use Flash;
use Illuminate\Support\Facades\DB;
use Response;

class ticketController extends AppBaseController
{
    /** @var  ticketRepository */
    private $ticketRepository;

    public function __construct(ticketRepository $ticketRepo)
    {
        $this->ticketRepository = $ticketRepo;
    }

    /**
     * Display a listing of the ticket.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //DB::enableQueryLog();
        $tickets = DB::table('autof_tickets')
            ->join('parks', 'parks.no_est', '=', 'autof_tickets.id_est')
            ->where('pi','=',Auth::user()->id_est)
            ->select('autof_tickets.id','autof_tickets.total_ticket', 'autof_tickets.fecha_emision', 'autof_tickets.Razon_social', 'autof_tickets.estatus', 'autof_tickets.coment', 'autof_tickets.RFC', 'autof_tickets.email', 'parks.nombre as park')
            ->orderBy('id','desc')
            ->get();
        //dd(DB::getQueryLog());
        return view('tickets.index')
            ->with('tickets', $tickets);
    }

    /**
     * Show the form for creating a new ticket.
     *
     * @return Response
     */
    public function create()
    {
        $estacionamiento = Auth::user()->id_est;
        //dd($estacionamiento);
        $estacionamiento_info = Estacionamientos::join("orgs","parks.id_org","=","orgs.id")
            ->where('no_est','=',$estacionamiento)
            ->select(['orgs.*','parks.no_est', 'parks.nombre'])
            ->get();

        return view('tickets.create')
            ->with('estacionamiento' , $estacionamiento_info);
    }


    public function store()
    {
        $img ='';
        $fact = request()->validate([
            'no_est' => ['required'],
            'razonsocial' => ['required', 'string'],
            'rfc_empresa' => ['required', 'string'],
            'regimen_empresa' => ['required', 'string'],
            'Nombre_cliente' => ['required', 'string'],
            'RFC' => ['required', 'string', 'max:13'],
            'Correo' => ['required', 'email'],
            'Uso_CFDI' => ['required'],
            'concepto' => ['required'],
            'metodo_pago' => ['required'],
            'forma_pago' => ['required'],
            'Cantidad' => ['required', 'string'],
            'Precio' => ['required','string'],
            'Comentario' => ['required','string'],
        ]);

        $cadena = $fact['RFC'];
        $RFC = strtoupper($cadena);
        $foto = request()->file('foto');
        if($foto != null){
            $dataImg = $foto->get();
            $img = base64_encode($dataImg);
            //dd($img);
        }
        //dd(request()->all());

        //dd($foto);

        $fecha = date('Y-m-d');
        $fecha2 = date('H:i:s');

        $numfact = ticket::max('factura');
        //dd($numfact);
        $ticket = new ticket();
        $ticket->fill([
            'id_cliente' => 1,
            'id_est' => $fact['no_est'],
            'id_tipo' => 3,
            'id_CentralUser' => 1,
            'factura' => $numfact + 1,
            'total_ticket' => $fact['Precio'],
            'fecha_emision' => $fecha,
            'estatus' => 'factura_pi',
            'UsoCFDI' => $fact['Uso_CFDI'],
            'metodo_pago' => $fact['metodo_pago'],
            'forma_pago' => $fact['forma_pago'],
            'RFC' => $RFC,
            'Razon_social' => $fact['Nombre_cliente'],
            'email' => $fact['Correo'],
            'imagen' => $img,
            'pi' => Auth::user()->id_est
        ]);
        $ticket->save();

        Flash::success('Solicitud enviada');

        return redirect(route('tickets.index'));
    }

    /**
     * Display the specified ticket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ticket = $this->ticketRepository->find($id);
        if (empty($ticket)) {
            Flash::error('Ticket not found');
            return redirect(route('tickets.index'));
        }
        return view('tickets.show')->with('ticket', $ticket);
    }

    /**
     * Show the form for editing the specified ticket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticket = $this->ticketRepository->find($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');
            return redirect(route('tickets.index'));
        }
        return view('tickets.edit')->with('ticket', $ticket);
    }

    /**
     * Update the specified ticket in storage.
     *
     * @param int $id
     * @param UpdateticketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateticketRequest $request)
    {
        $ticket = $this->ticketRepository->find($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        $ticket = $this->ticketRepository->update($request->all(), $id);

        Flash::success('Ticket updated successfully.');

        return redirect(route('tickets.index'));
    }

    /**
     * Remove the specified ticket from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ticket = $this->ticketRepository->find($id);
        if (empty($ticket)) {
            Flash::error('Ticket not found');
            return redirect(route('tickets.index'));
        }
        $this->ticketRepository->delete($id);
        Flash::success('Ticket deleted successfully.');
        return redirect(route('tickets.index'));
    }
}
