<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Models\Detalle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Repositories\DetalleRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateDetalleRequest;
use App\Http\Requests\UpdateDetalleRequest;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DetalleController extends AppBaseController
{
    /** @var  DetalleRepository */
    private $detalleRepository;

    public function __construct(DetalleRepository $detalleRepo)
    {
        $this->detalleRepository = $detalleRepo;
    }

    /**
     * Display a listing of the Detalle.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /*$detalles = Detalle::select('id', 'id_estacionamiento', 'montoPension', 'tipoPension', 'rfc', 'noTarjeta', 'status', 'fecha_limite')
            ->where('id_estacionamiento', Auth::user()->id_est)
            ->get();*/

        $id_est = Auth::user()->id_est;
        #Reporte de pensiones activas por gerente
        $pensionActiva = "
        #Reporte de pensiones activas e inactivas por estacionamiento y fechas
        #este query muestra todas las pensiones activas e inactivas por estacionamiento, cuentra con dos union de consulta


        select dis.clave_distrito as distrito,
        dis.nombre as distrital,
        grt.name as gerente,
        grt.email as \"correogerente\",
        est.no_estacionamiento as \"NumeroProyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"NumeroPension\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombrepensionado\",
        'Inactiva' as \"Estado\",
        pd.fechaInactivacion as \"fechabajacancelacion\",
        if(pd.contrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoContrato\",
        if(pd.solicitudContrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoSolicitud\",
        if(pd.comprobanteDomicilio is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"ComprobanteDomicilio\",
        if(pd.ine is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoINE\",
        if(pd.licencia is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoLicencia\",
        if(pd.rfc is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"Documento RFC\",
        if(pd.tarjetaCirculacion is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoTarjetaCirculacion\"
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id_cliente = cl.id inner join distritales dis on dis.id = grt.id_distritales
        where  est.id = @no_estacionamiento #id de estacionamiento a consultar
        and pd.status = 0 #cambiar a 0 cuando inactivas
        and pd.fechaInactivacion is not null
        and date_format(pd.created_at, '%Y-%m-%d') between @fecha_inicio and @fecha_final
        UNION
        #Reporte de pensiones canceladas por ESTACIONAMIENTO
        select dis.clave_distrito as distrito,
        dis.nombre as distrital,
        grt.name as gerente,
        grt.email as \"correogerente\",
        est.no_estacionamiento as \"NumeroProyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"NumeroPension\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombrepensionado\",
        'Cancelada' as \"Estado\",
        pd.fechaCancelacion as \"fechabajacancelacion\",
        if(pd.contrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoContrato\",
        if(pd.solicitudContrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoSolicitud\",
        if(pd.comprobanteDomicilio is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"ComprobanteDomicilio\",
        if(pd.ine is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoINE\",
        if(pd.licencia is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoLicencia\",
        if(pd.rfc is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"Documento RFC\",
        if(pd.tarjetaCirculacion is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoTarjetaCirculacion\"
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id_cliente = cl.id inner join distritales dis on dis.id = grt.id_distritales
        where  est.id = @no_estacionamiento  #id de estacionamiento a consultar
        and pd.status = 0 #cambiar a 0 cuando inactivas
        and fechaCancelacion is not null
        and date_format(pd.created_at, '%Y-%m-%d') between @fecha_inicio and @fecha_final
        union
        #Reporte de pensiones activas por estacionamiento
        select dis.clave_distrito as distrito,
        dis.nombre as distrital,
        grt.name as gerente,
        grt.email as \"correogerente\",
        est.no_estacionamiento as \"NumeroProyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"NumeroPension\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombrepensionado\",
        if( cl.estatus = 1 && (date_format(pd.created_at, '%Y-%m')) = (date_format(now(), '%Y-%m')), \"Nueva\", \"Activa\") as \"estado\",
        pd.fechaCancelacion as \"fechabajacancelacion\",
        if(pd.contrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoContrato\",
        if(pd.solicitudContrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoSolicitud\",
        if(pd.comprobanteDomicilio is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"ComprobanteDomicilio\",
        if(pd.ine is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoINE\",
        if(pd.licencia is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoLicencia\",
        if(pd.rfc is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoLicencia\",
        if(pd.tarjetaCirculacion is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"DocumentoTarjetaCirculacion\"
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id_cliente = cl.id  inner join distritales dis on dis.id = grt.id_distritales
        where  est.id = @no_estacionamiento #id de estacionamiento a consultar
        and pd.status = 1 #estatus de pensión al momento de la consulta
        and fechaCancelacion is null and pd.fechaInactivacion is null
        and date_format(pd.created_at, '%Y-%m-%d') between @fecha_inicio and @fecha_final;
        ";

        $consulta = "SELECT * FROM moduloPensiones_v";
        $result1 = DB::connection('mysql2')->SELECT($consulta);


        return view('detalles.index')
            ->with('detalles', $result1);
    }

    /**
     * Show the form for creating a new Detalle.
     *
     * @return Response
     */
    public function create()
    {
        $proyecto = Auth::user()->id_est;
        return view('detalles.create')->with('proyectos', $proyecto);
    }

    /**
     * Store a newly created Detalle in storage.
     *
     * @param CreateDetalleRequest $request
     *
     * @return Response
     */
    public function store(CreateDetalleRequest $request)
    {
        $input = $request->all();

        $contrato = '';
        $Solicitudcontrato = '';
        $comprobanteDomicilio = '';
        $ine = '';
        $licencia = '';
        $tarjetaCirculacion = '';

        $contrato = request()->file('contrato');
        if ($contrato != null) {
            $datacont = $contrato->get();
            $nombre_contrato = $contrato->getBasename();
            $cont = file_get_contents($contrato);
            $contdata = base64_encode($cont);
            $input['contrato'] = $contdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $Solicitudcontrato = request()->file('solicitudContrato');
        if ($Solicitudcontrato != null) {
            $datascont = $Solicitudcontrato->get();
            $nombre_scontrato = $Solicitudcontrato->getBasename();
            $scont = file_get_contents($Solicitudcontrato);
            $scontdata = base64_encode($scont);
            $input['solicitudContrato'] = $scontdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $comprobanteDomicilio = request()->file('comprobanteDomicilio');
        if ($comprobanteDomicilio != null) {
            $datascomp = $comprobanteDomicilio->get();
            $nombre_cdomici = $comprobanteDomicilio->getBasename();
            $cdom = file_get_contents($comprobanteDomicilio);
            $cdomdata = base64_encode($cdom);
            $input['comprobanteDomicilio'] = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $ine = request()->file('ine');
        if ($ine != null) {
            $datasine = $ine->get();
            $nombre_ine = $ine->getBasename();
            $cine = file_get_contents($ine);
            $cinedata = base64_encode($cine);
            $input['ine'] = $cinedata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $licencia = request()->file('licencia');
        if ($licencia != null) {
            $dataslic = $licencia->get();
            $nombre_lic = $licencia->getBasename();
            $line = file_get_contents($licencia);
            $linedata = base64_encode($line);
            $input['licencia'] = $linedata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $tarjetaCirculacion = request()->file('tarjetaCirculacion');
        if ($tarjetaCirculacion != null) {
            $datastar = $tarjetaCirculacion->get();
            $nombre_tar = $tarjetaCirculacion->getBasename();
            $tarin = file_get_contents($tarjetaCirculacion);
            $tarindata = base64_encode($tarin);
            $input['tarjetaCirculacion'] = $tarindata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $pension = $this->detalleRepository->create($input);

        //dd("contrato");
        Flash::success('Pensión añadida satisfactoriamente.');

        return redirect(route('detalles.index'));
    }

    /**
     * Display the specified Detalle.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $detalle = $this->detalleRepository->find($id);

        if (empty($detalle)) {
            Flash::error('Detalle not found');

            return redirect(route('detalles.index'));
        }

        return view('detalles.show')->with('detalle', $detalle);
    }

    /**
     * Show the form for editing the specified Detalle.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $detalle = $this->detalleRepository->find($id);

        if (empty($detalle)) {
            Flash::error('Detalle not found');

            return redirect(route('detalles.index'));
        }

        $proyecto = Auth::user()->id_est;
        return view('detalles.edit')->with('detalle', $detalle)->with('proyectos', $proyecto);
    }

    /**
     * Update the specified Detalle in storage.
     *
     * @param int $id
     * @param UpdateDetalleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetalleRequest $request)
    {
        $detalle = $this->detalleRepository->find($id);

        if (empty($detalle)) {
            Flash::error('Detalle not found');

            return redirect(route('detalles.index'));
        }

        $detalle = $this->detalleRepository->update($request->all(), $id);

        Flash::success('Pensión actualizada.');

        return redirect(route('detalles.index'));
    }

    /**
     * Remove the specified Detalle from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $detalle = $this->detalleRepository->find($id);

        if (empty($detalle)) {
            Flash::error('Detalle not found');

            return redirect(route('detalles.index'));
        }

        $this->detalleRepository->delete($id);

        Flash::success('Pensión eliminada.');

        return redirect(route('detalles.index'));
    }

    public function downloadContPdf($id)
    {
        $pdf = Detalle::find($id);

        if (empty($pdf->contrato)) {
            Flash::warning('Archivo no encontrado: Por favor, cargue el documento faltante');

            return redirect(route('detalles.index'));
        }

        $archivo = $pdf->contrato;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=contrato-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadSContPdf($id)
    {
        $pdf = Detalle::find($id);

        if (empty($pdf->solicitudContrato)) {
            Flash::warning('Archivo no encontrado: Por favor, cargue el documento faltante');

            return redirect(route('detalles.index'));
        }

        $archivo = $pdf->solicitudContrato;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=solicitudcontrato-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadComprobantedomPdf($id)
    {
        $pdf = Detalle::find($id);

        if (empty($pdf->comprobanteDomicilio)) {
            Flash::warning('Archivo no encontrado: Por favor, cargue el documento faltante');

            return redirect(route('detalles.index'));
        }

        $archivo = $pdf->comprobanteDomicilio;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=comprobantedomicilio-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadIne($id)
    {
        $pdf = Detalle::find($id);

        if (empty($pdf->ine)) {
            Flash::warning('Archivo no encontrado: Por favor, cargue el documento faltante');

            return redirect(route('detalles.index'));
        }

        $archivo = $pdf->ine;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=ine-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadLic($id)
    {
        $pdf = Detalle::find($id);

        if (empty($pdf->licencia)) {
            Flash::warning('Archivo no encontrado: Por favor, cargue el documento faltante');

            return redirect(route('detalles.index'));
        }

        $archivo = $pdf->licencia;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=licencia-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadTarC($id)
    {
        $pdf = Detalle::find($id);

        if (empty($pdf->tarjetaCirculacion)) {
            Flash::warning('Archivo no encontrado: Por favor, cargue el documento faltante');

            return redirect(route('detalles.index'));
        }

        $archivo = $pdf->tarjetaCirculacion;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=tarjetacirculacion-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function inactivo($id)
    {


        $detalle = Detalle::find($id);

        $detalle->status = 0;

        $detalle->save();

        Flash::success('Pensión dada de baja.');

        return redirect(route('detalles.index'));
    }

    public function temporal($id)
    {
        $detalle = Detalle::find($id);

        $detalle->status = 3;

        $detalle->save();

        Flash::success('Pensión dada de baja temporal.');

        return redirect(route('detalles.index'));
    }

    public function findBusquedaEspecifica($id)
    {
        if ($id == 1) {
            $consulta_find = "
            select
            est.no_estacionamiento as \"NumeroProyecto\",
            est.nombre_proyecto as \"Proyecto\",
            pd.id as \"NumeroPension\",
            cl.id as \"NumeroCliente\",
            cl.grupo as \"grupo\",
            concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombrepensionado\",
            if( cl.estatus = 1 && (date_format(pd.created_at, '%Y-%m')) = (date_format(now(), '%Y-%m')), \"Nueva\", \"Activa\") as \"estado\",
            pd.fechaCancelacion as \"fechabajacancelacion\",
            if(pd.contrato is null, \"No\", \"Sí\") as \"contrato\",
            if(pd.solicitudContrato is null, \"No\", \"Sí\") as \"solicitudContrato\",
            if(pd.comprobanteDomicilio is null, \"No\", \"Sí\") as \"comprobanteDomicilio\",
            if(pd.ine is null, \"No\", \"Sí\") as \"ine\",
            if(pd.licencia is null, \"No\", \"Sí\") as \"licencia\",
            if(pd.rfc is null, \"No\", \"Sí\") as \"rfc\",
            if(pd.tarjetaCirculacion is null, \"No\", \"Sí\") as \"tarjetaCirculacion\"
            from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
            inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
            pd.id_cliente = cl.id  inner join distritales dis on dis.id = grt.id_distritales
            where  est.id = 7 #id de estacionamiento a consultar
            and pd.status = 0 #cambiar a 0 cuando inactivas
            and pd.fechaInactivacion is not null
            and date_format(pd.created_at, '%Y-%m-%d') between '2020-01-01' and '2021-07-22'
            ";

            $result1 = DB::connection('mysql2')->SELECT($consulta_find);
        } elseif ($id == 2) {
            $consulta_find = "
            select
            est.no_estacionamiento as \"NumeroProyecto\",
            est.nombre_proyecto as \"Proyecto\",
            pd.id as \"NumeroPension\",
            cl.id as \"NumeroCliente\",
            cl.grupo as \"grupo\",
            concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombrepensionado\",
            if( cl.estatus = 1 && (date_format(pd.created_at, '%Y-%m')) = (date_format(now(), '%Y-%m')), \"Nueva\", \"Activa\") as \"estado\",
            pd.fechaCancelacion as \"fechabajacancelacion\",
            if(pd.contrato is null, \"No\", \"Sí\") as \"contrato\",
            if(pd.solicitudContrato is null, \"No\", \"Sí\") as \"solicitudContrato\",
            if(pd.comprobanteDomicilio is null, \"No\", \"Sí\") as \"comprobanteDomicilio\",
            if(pd.ine is null, \"No\", \"Sí\") as \"ine\",
            if(pd.licencia is null, \"No\", \"Sí\") as \"licencia\",
            if(pd.rfc is null, \"No\", \"Sí\") as \"rfc\",
            if(pd.tarjetaCirculacion is null, \"No\", \"Sí\") as \"tarjetaCirculacion\"
            from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
            inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
            pd.id_cliente = cl.id  inner join distritales dis on dis.id = grt.id_distritales
            where  est.id = 2 #id de estacionamiento a consultar
            and pd.status = 0 #cambiar a 0 cuando inactivas
            and pd.fechaCancelacion is not null
            and date_format(pd.created_at, '%Y-%m-%d') between '2020-01-01' and '2021-07-22'
            ";

            $result1 = DB::connection('mysql2')->SELECT($consulta_find);
        } else {
            Flash::error('Detalle not found');

            return redirect(route('detalles.index'));
        }

        return view('detalles.busqueda')
            ->with('detalles', $result1);
    }
}
