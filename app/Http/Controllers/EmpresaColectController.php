<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEmpresaColectRequest;
use App\Http\Requests\UpdateEmpresaColectRequest;
use App\Repositories\EmpresaColectRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class EmpresaColectController extends AppBaseController
{
    /** @var  EmpresaColectRepository */
    private $empresaColectRepository;

    public function __construct(EmpresaColectRepository $empresaColectRepo)
    {
        $this->empresaColectRepository = $empresaColectRepo;
    }

    /**
     * Display a listing of the EmpresaColect.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $empresaColects = $this->empresaColectRepository->all();

        return view('empresa_colects.index')
            ->with('empresaColects', $empresaColects);
    }

    /**
     * Show the form for creating a new EmpresaColect.
     *
     * @return Response
     */
    public function create()
    {
        return view('empresa_colects.create');
    }

    /**
     * Store a newly created EmpresaColect in storage.
     *
     * @param CreateEmpresaColectRequest $request
     *
     * @return Response
     */
    public function store(CreateEmpresaColectRequest $request)
    {
        $input = $request->all();

        $empresaColect = $this->empresaColectRepository->create($input);

        Flash::success('Empresa Colect saved successfully.');

        return redirect(route('empresaColects.index'));
    }

    /**
     * Display the specified EmpresaColect.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $empresaColect = $this->empresaColectRepository->find($id);

        if (empty($empresaColect)) {
            Flash::error('Empresa Colect not found');

            return redirect(route('empresaColects.index'));
        }

        return view('empresa_colects.show')->with('empresaColect', $empresaColect);
    }

    /**
     * Show the form for editing the specified EmpresaColect.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $empresaColect = $this->empresaColectRepository->find($id);

        if (empty($empresaColect)) {
            Flash::error('Empresa Colect not found');

            return redirect(route('empresaColects.index'));
        }

        return view('empresa_colects.edit')->with('empresaColect', $empresaColect);
    }

    /**
     * Update the specified EmpresaColect in storage.
     *
     * @param int $id
     * @param UpdateEmpresaColectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmpresaColectRequest $request)
    {
        $empresaColect = $this->empresaColectRepository->find($id);

        if (empty($empresaColect)) {
            Flash::error('Empresa Colect not found');

            return redirect(route('empresaColects.index'));
        }

        $empresaColect = $this->empresaColectRepository->update($request->all(), $id);

        Flash::success('Empresa Colect updated successfully.');

        return redirect(route('empresaColects.index'));
    }

    /**
     * Remove the specified EmpresaColect from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $empresaColect = $this->empresaColectRepository->find($id);

        if (empty($empresaColect)) {
            Flash::error('Empresa Colect not found');

            return redirect(route('empresaColects.index'));
        }

        $this->empresaColectRepository->delete($id);

        Flash::success('Empresa Colect deleted successfully.');

        return redirect(route('empresaColects.index'));
    }
}
