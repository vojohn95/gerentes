<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCentralUserRequest;
use App\Http\Requests\UpdateCentralUserRequest;
use App\Repositories\CentralUserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CentralUserController extends AppBaseController
{
    /** @var  CentralUserRepository */
    private $centralUserRepository;

    public function __construct(CentralUserRepository $centralUserRepo)
    {
        $this->centralUserRepository = $centralUserRepo;
    }

    /**
     * Display a listing of the CentralUser.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $centralUsers = $this->centralUserRepository->all();

        return view('central_users.index')
            ->with('centralUsers', $centralUsers);
    }

    /**
     * Show the form for creating a new CentralUser.
     *
     * @return Response
     */
    public function create()
    {
        return view('central_users.create');
    }

    /**
     * Store a newly created CentralUser in storage.
     *
     * @param CreateCentralUserRequest $request
     *
     * @return Response
     */
    public function store(CreateCentralUserRequest $request)
    {
        $input = $request->all();

        $centralUser = $this->centralUserRepository->create($input);

        Flash::success('Central User saved successfully.');

        return redirect(route('centralUsers.index'));
    }

    /**
     * Display the specified CentralUser.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $centralUser = $this->centralUserRepository->find($id);

        if (empty($centralUser)) {
            Flash::error('Central User not found');

            return redirect(route('centralUsers.index'));
        }

        return view('central_users.show')->with('centralUser', $centralUser);
    }

    /**
     * Show the form for editing the specified CentralUser.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $centralUser = $this->centralUserRepository->find($id);

        if (empty($centralUser)) {
            Flash::error('Central User not found');

            return redirect(route('centralUsers.index'));
        }

        return view('central_users.edit')->with('centralUser', $centralUser);
    }

    /**
     * Update the specified CentralUser in storage.
     *
     * @param int $id
     * @param UpdateCentralUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCentralUserRequest $request)
    {
        $centralUser = $this->centralUserRepository->find($id);

        if (empty($centralUser)) {
            Flash::error('Central User not found');

            return redirect(route('centralUsers.index'));
        }

        $centralUser = $this->centralUserRepository->update($request->all(), $id);

        Flash::success('Central User updated successfully.');

        return redirect(route('centralUsers.index'));
    }

    /**
     * Remove the specified CentralUser from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $centralUser = $this->centralUserRepository->find($id);

        if (empty($centralUser)) {
            Flash::error('Central User not found');

            return redirect(route('centralUsers.index'));
        }

        $this->centralUserRepository->delete($id);

        Flash::success('Central User deleted successfully.');

        return redirect(route('centralUsers.index'));
    }
}
