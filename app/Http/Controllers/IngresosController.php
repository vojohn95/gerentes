<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIngresosRequest;
use App\Http\Requests\UpdateIngresosRequest;
use App\Models\BancoColect;
use App\Models\CentralUser;
use App\Models\Cliente;
use App\Models\EmpresaColect;
use App\Models\ImgIngresos;
use App\Models\Ingresos;
use App\Repositories\IngresosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;

class IngresosController extends AppBaseController
{
    /** @var  IngresosRepository */
    private $ingresosRepository;

    public function __construct(IngresosRepository $ingresosRepo)
    {
        $this->ingresosRepository = $ingresosRepo;
    }

    /**
     * Display a listing of the Ingresos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sql = Ingresos::join('gerente', 'gerente.id', '=', 'ingresos.id_gerente')
            ->join('parks', 'gerente.id_est', '=', 'parks.no_est')
            ->select(['ingresos.*', 'parks.no_est', 'parks.nombre'])
            ->where('valido','=','pendiente')
            ->orwhere('valido','=','pendiente_correcion')
            ->get();
        return view('ingresos.index')
            ->with('ingreso', $sql);

    }

    /**
     * Show the form for creating a new Ingresos.
     *
     * @return Response
     */
    public function create()
    {
        $info = CentralUser::join('parks', 'parks.no_est', '=', 'gerente.id_est')
            ->where('gerente.id', '=', Auth::user()->id)
            ->select('nombre', 'no_est')
            ->get();
        $empresas = EmpresaColect::all();
        $bancos = BancoColect::all();
        return view('ingresos.create')
            ->with('info', $info[0])
            ->with('empresas', $empresas)
            ->with('bancos', $bancos);
    }

    public function store()
    {
        //$input = \request()->all();
        //dd($input);

        $input = request()->validate([
            "Numero_de_fichas" => ['integer', 'required'],
            "Importe_TH" => ['numeric', 'required',],
            "id_empresa" => ['integer', 'required'],
            "id_banco" => ['integer', 'required'],
            "Dia_de_venta" => ['date', 'required'],
            "Fecha_recoleccion" => ['date', 'required'],
            "foto_ficha" => ['required'],
            "foto_firma" => ['required']
        ]);


        $no_est = Auth::user()->id;

        if ($input['id_empresa'] == "0") {
            Flash::warning('Debe seleccionar una empresa de traslado');

            return redirect(route('ingresos.create'));
        }

        if ($input['id_banco'] == "0") {
            Flash::warning('Debe seleccionar un banco');

            return redirect(route('ingresos.create'));
        }

        $foto = request()->file('foto_ficha');
        $dataImg = $foto->get();
        $img = base64_encode($dataImg);

        $foto2 = request()->file('foto_firma');
        $dataImg2 = $foto2->get();
        $img2 = base64_encode($dataImg2);

        $Ingreso = new Ingresos();
        $Ingreso->fill([
            'id_gerente' => Auth::user()->id,
            'id_empresa' => $input['id_empresa'],
            'id_banco' => $input['id_banco'],
            'numero' => $input['Numero_de_fichas'],
            'venta' => $input['Dia_de_venta'],
            'fecha_recoleccion' => $input['Fecha_recoleccion'],
            'importe' => $input['Importe_TH'],
        ]);
        $Ingreso->save();

        $id_ingreso = $Ingreso->id;

        $img_ingreso = new ImgIngresos();
        $img_ingreso->fill([
            'id_ingreso' => $id_ingreso,
            'foto' => $img,
            'firma' => $img2
        ]);
        $img_ingreso->save();

        Flash::success('Ingresos cargado satisfactoriamente.');

        return redirect(route('ingresos.index'));
    }

    /**
     * Display the specified Ingresos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ingresos = $this->ingresosRepository->find($id);

        if (empty($ingresos)) {
            Flash::error('Ingresos not found');

            return redirect(route('ingresos.index'));
        }

        return view('ingresos.show')->with('ingresos', $ingresos);
    }

    /**
     * Show the form for editing the specified Ingresos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $sql = Ingresos::join('gerente', 'gerente.id', '=', 'ingresos.id_gerente')
            ->join('parks', 'gerente.id_est', '=', 'parks.no_est')
            ->join('empresa_colect','empresa_colect.id','=','ingresos.id_empresa')
            ->join('banco_colect','banco_colect.id','=','ingresos.id_banco')
            ->join('img_ingresos','img_ingresos.id_ingreso','=','ingresos.id')
            ->select(['ingresos.*', 'parks.no_est', 'parks.nombre', 'banco_colect.banco', 'empresa_colect.empresa'])
            ->where('ingresos.id', $id)
            ->get();

        $ingresos = $this->ingresosRepository->find($id);

        if (empty($ingresos)) {
            Flash::error('Ingresos not found');

            return redirect(route('ingresos.index'));
        }

        $info = CentralUser::join('parks', 'parks.no_est', '=', 'gerente.id_est')
            ->where('gerente.id', '=', Auth::user()->id)
            ->select('nombre', 'no_est')
            ->get();
        $empresas = EmpresaColect::all();
        $bancos = BancoColect::all();

        return view('ingresos.edit')
            ->with('ingresos', $ingresos)
            ->with('info', $info)
            ->with('empresas', $empresas)
            ->with('bancos', $bancos)
            ->with('sql', $sql);
    }

    /**
     * Update the specified Ingresos in storage.
     *
     * @param int $id
     * @param UpdateIngresosRequest $request
     *
     * @return Response
     */
    public function update()
    {
        $input = request()->validate([
            "id" => ['integer','require'],
            "Numero_de_fichas" => ['integer', 'required'],
            "Importe_TH" => ['numeric', 'required',],
            "id_empresa" => ['integer', 'required'],
            "id_banco" => ['integer', 'required'],
            "Dia_de_venta" => ['date', 'required'],
            "Fecha_recoleccion" => ['date', 'required'],
            "foto_ficha" => ['required'],
            "foto_firma" => ['required']
        ]);
        dd($input);
        if (empty($Ingreso)) {
            Flash::error('Ingresos no encontrado');
            return redirect(route('ingresos.index'));
        }
        $Ingreso = Ingresos::find($input['id']);
        $Ingreso->fill([
            'id_gerente' => Auth::user()->id,
            'id_empresa' => $input['id_empresa'],
            'id_banco' => $input['id_banco'],
            'numero' => $input['Numero_de_fichas'],
            'venta' => $input['Dia_de_venta'],
            'fecha_recoleccion' => $input['Fecha_recoleccion'],
            'importe' => $input['Importe_TH'],
        ]);
        $Ingreso->save();

        $foto = request()->file('foto_ficha');
        $dataImg = $foto->get();
        $img = base64_encode($dataImg);

        $foto2 = request()->file('foto_firma');
        $dataImg2 = $foto2->get();
        $img2 = base64_encode($dataImg2);

        //Flash::success('Ingresos updated successfully.');

        //return redirect(route('ingresos.index'));
    }

    /**
     * Remove the specified Ingresos from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $ingresos = $this->ingresosRepository->find($id);

        if (empty($ingresos)) {
            Flash::error('Ingresos not found');

            return redirect(route('ingresos.index'));
        }

        $this->ingresosRepository->delete($id);

        Flash::success('Ingresos deleted successfully.');

        return redirect(route('ingresos.index'));
    }
}
