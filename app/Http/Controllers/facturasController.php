<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Models\facturas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Support\Facades\Auth;
use App\Repositories\facturasRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreatefacturasRequest;
use App\Http\Requests\UpdatefacturasRequest;

class facturasController extends AppBaseController
{
    /** @var  facturasRepository */
    private $facturasRepository;

    public function __construct(facturasRepository $facturasRepo)
    {
        $this->facturasRepository = $facturasRepo;
    }

    /**
     * Display a listing of the facturas.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $facturas = facturas::join('autof_tickets', 'autof_tickets.id', '=', 'auto_facts.id_ticketAF')
            ->select([
                'auto_facts.id',
                'auto_facts.serie',
                'auto_facts.folio',
                'auto_facts.fecha_timbrado',
                'auto_facts.subtotal_factura',
                'auto_facts.iva_factura',
                'auto_facts.total_factura',
                'autof_tickets.email',
                'autof_tickets.Razon_social'
            ])
            ->where('autof_tickets.id_est', '=', Auth::user()->id_est)
            ->orderBy('auto_facts.created_at', 'desc')
            ->get();
        return view('facturas.index')
            ->with('facturas', $facturas)
            ->with('bus', True);
    }

    public function busqueda()
    {
        $input = request()->validate([
            "rs" => [''],
            "rfc" => [''],
            "total" => [''],
        ]);
        $facturas = facturas::query();
        $facturas = $facturas->join('autof_tickets', 'autof_tickets.id', '=', 'auto_facts.id_ticketAF')
            ->select([
                'auto_facts.id',
                'auto_facts.serie',
                'auto_facts.folio',
                'auto_facts.fecha_timbrado',
                'auto_facts.subtotal_factura',
                'auto_facts.iva_factura',
                'auto_facts.total_factura',
                'autof_tickets.email',
                'autof_tickets.Razon_social'
            ])
            ->orderBy('auto_facts.created_at', 'desc');
        if ($input['rs'] != null) {
            $facturas = $facturas->where('Razon_social', '=', $input['rs']);
        }
        if ($input['rfc'] != null) {
            $facturas = $facturas->where('RFC', '=', $input['rfc']);
        }
        if ($input['total'] != null) {
            $facturas = $facturas->where('total_ticket', '=', $input['total']);
        }
        if ($facturas->count() == 0) {
            Flash::error('No se encontraron registros');
            return redirect(url('facturas'));
        }
        $facturas = $facturas->get();
        return view('facturas.index')
            ->with('facturas', $facturas)
            ->with('bus', false);
    }

    /**
     * Show the form for creating a new facturas.
     *
     * @return Response
     */
    public function create()
    {
        return view('facturas.create');
    }

    /**
     * Store a newly created facturas in storage.
     *
     * @param CreatefacturasRequest $request
     *
     * @return Response
     */
    public function store(CreatefacturasRequest $request)
    {
        $input = $request->all();

        $facturas = $this->facturasRepository->create($input);

        Flash::success('Facturas saved successfully.');

        return redirect(route('facturas.index'));
    }

    /**
     * Display the specified facturas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $facturas = $this->facturasRepository->find($id);

        if (empty($facturas)) {
            Flash::error('Facturas not found');

            return redirect(route('facturas.index'));
        }

        return view('facturas.show')->with('facturas', $facturas);
    }

    /**
     * Show the form for editing the specified facturas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $facturas = $this->facturasRepository->find($id);

        if (empty($facturas)) {
            Flash::error('Facturas not found');

            return redirect(route('facturas.index'));
        }

        return view('facturas.edit')->with('facturas', $facturas);
    }

    /**
     * Update the specified facturas in storage.
     *
     * @param int $id
     * @param UpdatefacturasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatefacturasRequest $request)
    {
        $facturas = $this->facturasRepository->find($id);

        if (empty($facturas)) {
            Flash::error('Facturas not found');

            return redirect(route('facturas.index'));
        }

        $facturas = $this->facturasRepository->update($request->all(), $id);

        Flash::success('Facturas updated successfully.');

        return redirect(route('facturas.index'));
    }

    /**
     * Remove the specified facturas from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $facturas = $this->facturasRepository->find($id);

        if (empty($facturas)) {
            Flash::error('Facturas not found');

            return redirect(route('facturas.index'));
        }

        $this->facturasRepository->delete($id);

        Flash::success('Facturas deleted successfully.');

        return redirect(route('facturas.index'));
    }

    public function downloadXML($id)
    {
        //$id = \request()->request;
        //dd($id);
        $xml = facturas::find($id);
        $archivo = $xml->XML;
        $uuid = $xml->uuid;
        $path = "xml_down/" . $uuid . ".xml";
        //dd($uuid);
        file_put_contents($path, $archivo);
        header("Content-disposition: attachment; filename=$uuid.xml");
        header("Content-type: application/xml");
        readfile($path);
        unlink($path);

        //Flash::success('Factura descargada');

        //return redirect(route('facturas.index'));
    }

    public function downloadPDF($id)
    {
        //$id = \request()->request;
        //dd($id);
        $pdf = facturas::find($id);
        $archivo = $pdf->PDF;
        $uuid = $pdf->uuid;

        $decoded = base64_decode($archivo);
        $path = "xml_down/" . $uuid . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=$uuid.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);

        //Flash::success('Factura descargada');

        //return redirect(route('facturas.index'));
    }

    public function enviarEmail()
    {
        $input = request()->validate([
            "tag" => ['integer', 'required'],
            "correo" => ['email', 'required'],
        ]);

        $correo = $input['correo'];
        $id = $input['tag'];

        $xml = facturas::find($id);
        $archivo = $xml->XML;
        $uuid = $xml->uuid;
        $path1 = "xml_down/" . $uuid . ".xml";
        //dd($uuid);
        file_put_contents($path1, $archivo);

        $pdf = facturas::find($id);
        $archivo = $pdf->PDF;
        $uuid = $pdf->uuid;
        $decoded = base64_decode($archivo);
        $path2 = "xml_down/" . $uuid . ".pdf";
        file_put_contents($path2, $decoded);

        $serie = $pdf->serie;

        $mail = new PHPMailer(true);
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.office365.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "facturacion-oce@central-mx.com";
        $mail->Password = "1t3gr4d0r2020*";
        $mail->Charset = 'UTF-8';
        $mail->setFrom('facturacion-oce@central-mx.com', 'Operadora central de estacionamientos');
        //$mail->From = "no-reply@central-mx.com";
        //$mail->FromName = "Central operadora de estacionamientos";
        $mail->Subject = "Factura de estacionamiento";
        $mail->Body = "<html>
                    <meta charset='utf-8'>
                    <h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
                    <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: " . $uuid . " , Serie: " . $serie . ". Así como su representación impresa.</p>
                    <p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
                    <p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $correo . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p>
                    <p align='center;'>Estimado usuario le recomendamos realizar sus facturas en un lapso no mayor a 5 días hábiles</p>
                    </html>";
        $mail->AddAddress($correo, $uuid);
        $archivo = $path1;
        $pdf1 = $path2;
        $mail->AddAttachment($archivo);
        $mail->AddAttachment($pdf1);
        $mail->IsHTML(true);
        $mail->Send();

        unlink($path1);
        unlink($path2);


        Flash::success('Correo enviado satisfactoriamente.');

        return redirect(route('facturas.index'));
    }

    public function enviarEmailespecial()
    {
        $input = request()->validate([
            "tag" => ['integer', 'required'],
            "correo" => ['email', 'required'],
        ]);

        $correo = $input['correo'];
        $id = $input['tag'];

        $xml = facturas::find($id);
        $archivo = $xml->XML;
        $uuid = $xml->uuid;
        $path1 = "xml_down/" . $uuid . ".xml";
        //dd($uuid);
        file_put_contents($path1, $archivo);

        $pdf = facturas::find($id);
        $archivo = $pdf->PDF;
        $uuid = $pdf->uuid;
        $decoded = base64_decode($archivo);
        $path2 = "xml_down/" . $uuid . ".pdf";
        file_put_contents($path2, $decoded);

        $serie = $pdf->serie;

        $mail = new PHPMailer(true);
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.office365.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "facturacion-oce@central-mx.com";
        $mail->Password = "1t3gr4d0r2020*";
        $mail->Charset = 'UTF-8';
        $mail->setFrom('facturacion-oce@central-mx.com', 'Operadora central de estacionamientos');
        //$mail->From = "no-reply@central-mx.com";
        //$mail->FromName = "Central operadora de estacionamientos";
        $mail->Subject = "Factura de estacionamiento";
        $mail->Body = "<html>
                    <meta charset='utf-8'>
                    <h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
                    <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: " . $uuid . " , Serie: " . $serie . ". Así como su representación impresa.</p>
                    <p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
                    <p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $correo . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p>
                    <p align='center;'>Estimado usuario le recomendamos realizar sus facturas en un lapso no mayor a 5 días hábiles</p>
                    </html>";
        $mail->AddAddress($correo, $uuid);
        $archivo = $path1;
        $pdf1 = $path2;
        $mail->AddAttachment($archivo);
        $mail->AddAttachment($pdf1);
        $mail->IsHTML(true);
        $mail->Send();

        unlink($path1);
        unlink($path2);


        Flash::success('Correo enviado satisfactoriamente.');

        return redirect(route('facturacionespeciales.index'));
    }
}
