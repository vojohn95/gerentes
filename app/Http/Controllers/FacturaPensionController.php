<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Models\Detalle;
use App\Models\catBanco;
use App\Models\CatCfdiUso;
use Illuminate\Http\Request;
use App\Models\FacturaPension;
use App\Models\MetodoPago;
use App\Http\Controllers\AppBaseController;
use App\Repositories\FacturaPensionRepository;
use App\Http\Requests\CreateFacturaPensionRequest;
use App\Http\Requests\UpdateFacturaPensionRequest;
use Auth;
use DB;

class FacturaPensionController extends AppBaseController
{
    /** @var  FacturaPensionRepository */
    private $facturaPensionRepository;

    public function __construct(FacturaPensionRepository $facturaPensionRepo)
    {
        $this->facturaPensionRepository = $facturaPensionRepo;
    }

    /**
     * Display a listing of the FacturaPension.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $facturaPensions = $this->facturaPensionRepository->all();

        $pensionActiva = "
        select
        facturaPensiones.id as facturaId,
        facturaPensiones.serie as serie,
        facturaPensiones.folio as folio,
        facturaPensiones.fecha_timbrado as fecha_timbrado,
        pensionesDet.id,
        concat(Clientes.nombres, ' ', Clientes.apellido_paterno, ' ', Clientes.apellido_materno) as nombre_cliente,
        Clientes.rfc as RFC,
        facturaPensiones.subtotal_factura as subtotal_factura,
        facturaPensiones.iva_factura as iva,
        facturaPensiones.total_factura as total_factura,
        facturaPensiones.xml as XML_Factura,
        facturaPensiones.pdf as PDF_factura,
        estacionamientos.nombre_proyecto as proyecto,
        estacionamientos.no_estacionamiento as numero_proyecto
        from facturaPensiones inner join pensionesDet on facturaPensiones.id_pen = pensionesDet.id
        inner join estacionamientos on pensionesDet.id_estacionamiento = estacionamientos.id
        inner join  Clientes on pensionesDet.id_cliente = Clientes.id
        where estacionamientos.no_estacionamiento = 7
        order by fecha_timbrado desc
        ";

        $result1 = DB::connection('mysql2')->SELECT($pensionActiva);
        //dd($result1);

        return view('factura_pensions.index')
            ->with('facturaPensions', $result1);
    }

    /**
     * Show the form for creating a new FacturaPension.
     *
     * @return Response
     */
    public function create()
    {
        $pensiones = Detalle::select('pensionesDet.*', 'Clientes.*')
        ->join('Clientes', 'pensionesDet.id_Cliente', '=', 'Clientes.id')
        ->get();


        $bancos = catBanco::get();
        $usocfdi = catCfdiUso::get();
        $metodo = MetodoPago::get();

        return view('factura_pensions.create')->with('pensiones', $pensiones)->with('bancos', $bancos)
        ->with('usocfdis', $usocfdi)->with('metodos', $metodo);
    }

    /**
     * Store a newly created FacturaPension in storage.
     *
     * @param CreateFacturaPensionRequest $request
     *
     * @return Response
     */
    public function store(CreateFacturaPensionRequest $request)
    {
        $input = $request->all();

        $xml = '';
        $pdf = '';

        $xml = request()->file('XML');
        if ($xml != null) {
            $datacont = $xml->get();
            $nombre_contrato = $xml->getBasename();
            $cont = file_get_contents($xml);
            //$contdata = base64_encode($cont);
            $input['XML'] = $cont;
            //$contrato = $this->pensionRepository->create($input);
        }

        $pdf = request()->file('PDF');
        if ($pdf != null) {
            $datascont = $pdf->get();
            $nombre_scontrato = $pdf->getBasename();
            $scont = file_get_contents($pdf);
            $scontdata = base64_encode($scont);
            $input['PDF'] = $scontdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $facturaPension = $this->facturaPensionRepository->create($input);

        return redirect(route('facturaPensions.index'));
    }

    /**
     * Display the specified FacturaPension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            Flash::error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        return view('factura_pensions.show')->with('facturaPension', $facturaPension);
    }

    /**
     * Show the form for editing the specified FacturaPension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            Flash::error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        $pensiones = Detalle::select('pensionesDet.*', 'Clientes.*')
        ->join('Clientes', 'pensionesDet.id', '=', 'Clientes.id_pension')
        ->get();


        $bancos = catBanco::get();
        $usocfdi = catCfdiUso::get();
        $metodo = MetodoPago::get();

        return view('factura_pensions.edit')->with('facturaPension', $facturaPension)->with('pensiones', $pensiones)->with('bancos', $bancos)
        ->with('usocfdis', $usocfdi)->with('metodos', $metodo);
    }

    /**
     * Update the specified FacturaPension in storage.
     *
     * @param int $id
     * @param UpdateFacturaPensionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFacturaPensionRequest $request)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            Flash::error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        $facturaPension = $this->facturaPensionRepository->update($request->all(), $id);

        Flash::success('Factura Pension updated successfully.');

        return redirect(route('facturaPensions.index'));
    }

    /**
     * Remove the specified FacturaPension from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            Flash::error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        $this->facturaPensionRepository->delete($id);

        Flash::success('Factura Pension deleted successfully.');

        return redirect(route('facturaPensions.index'));
    }

    public function downloadpenPdf($id)
    {
        $pdf = FacturaPension::find($id);
        $archivo = $pdf->PDF;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $pdf->id . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=contrato-fact$pdf->id.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadpenXML($id)
    {
        //$id = \request()->request;
        //dd($id);
        $xml = FacturaPension::find($id);
        $archivo = $xml->XML;
        $path = "xml_down/" . $xml->id . ".xml";
        //dd($uuid);
        file_put_contents($path, $archivo);
        header("Content-disposition: attachment; filename=fact$xml->id.xml");
        header("Content-type: application/xml");
        readfile($path);
        unlink($path);

        //Flash::success('Factura descargada');

        //return redirect(route('facturas.index'));
    }
}
