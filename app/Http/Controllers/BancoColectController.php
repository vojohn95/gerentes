<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBancoColectRequest;
use App\Http\Requests\UpdateBancoColectRequest;
use App\Repositories\BancoColectRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class BancoColectController extends AppBaseController
{
    /** @var  BancoColectRepository */
    private $bancoColectRepository;

    public function __construct(BancoColectRepository $bancoColectRepo)
    {
        $this->bancoColectRepository = $bancoColectRepo;
    }

    /**
     * Display a listing of the BancoColect.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $bancoColects = $this->bancoColectRepository->all();

        return view('banco_colects.index')
            ->with('bancoColects', $bancoColects);
    }

    /**
     * Show the form for creating a new BancoColect.
     *
     * @return Response
     */
    public function create()
    {
        return view('banco_colects.create');
    }

    /**
     * Store a newly created BancoColect in storage.
     *
     * @param CreateBancoColectRequest $request
     *
     * @return Response
     */
    public function store(CreateBancoColectRequest $request)
    {
        $input = $request->all();

        $bancoColect = $this->bancoColectRepository->create($input);

        Flash::success('Banco Colect saved successfully.');

        return redirect(route('bancoColects.index'));
    }

    /**
     * Display the specified BancoColect.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bancoColect = $this->bancoColectRepository->find($id);

        if (empty($bancoColect)) {
            Flash::error('Banco Colect not found');

            return redirect(route('bancoColects.index'));
        }

        return view('banco_colects.show')->with('bancoColect', $bancoColect);
    }

    /**
     * Show the form for editing the specified BancoColect.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bancoColect = $this->bancoColectRepository->find($id);

        if (empty($bancoColect)) {
            Flash::error('Banco Colect not found');

            return redirect(route('bancoColects.index'));
        }

        return view('banco_colects.edit')->with('bancoColect', $bancoColect);
    }

    /**
     * Update the specified BancoColect in storage.
     *
     * @param int $id
     * @param UpdateBancoColectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBancoColectRequest $request)
    {
        $bancoColect = $this->bancoColectRepository->find($id);

        if (empty($bancoColect)) {
            Flash::error('Banco Colect not found');

            return redirect(route('bancoColects.index'));
        }

        $bancoColect = $this->bancoColectRepository->update($request->all(), $id);

        Flash::success('Banco Colect updated successfully.');

        return redirect(route('bancoColects.index'));
    }

    /**
     * Remove the specified BancoColect from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bancoColect = $this->bancoColectRepository->find($id);

        if (empty($bancoColect)) {
            Flash::error('Banco Colect not found');

            return redirect(route('bancoColects.index'));
        }

        $this->bancoColectRepository->delete($id);

        Flash::success('Banco Colect deleted successfully.');

        return redirect(route('bancoColects.index'));
    }
}
