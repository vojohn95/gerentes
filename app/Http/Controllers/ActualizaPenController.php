<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Models\Cliente;
use App\Models\Detalle;
use Laracasts\Flash\Flash;
use App\Models\ActualizaPen;
use Illuminate\Http\Request;
use App\Models\Estacionamientos;
use Carbon\Carbon;

class ActualizaPenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_est = Auth::user()->id_est;
        $date = Carbon::now();
        $hoy = $date->format('Y-m-d');

        $pensionActiva = "
        #Query actualizaciones
        select
        est.no_estacionamiento as \"NumeroProyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"NumeroPension\",
        cl.id as \"NumeroCliente\",
        cl.grupo as \"grupo\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombrepensionado\",
        if( cl.estatus = 1 && (date_format(pd.created_at, '%Y-%m')) = (date_format(now(), '%Y-%m')), \"Nueva\", \"Activa\") as \"estado\",
        pd.fechaCancelacion as \"fechabajacancelacion\",
        if(pd.contrato is null, \"No\", \"Sí\") as \"contrato\",
        if(pd.solicitudContrato is null, \"No\", \"Sí\") as \"solicitudContrato\",
        if(pd.comprobanteDomicilio is null, \"No\", \"Sí\") as \"comprobanteDomicilio\",
        if(pd.ine is null, \"No\", \"Sí\") as \"ine\",
        if(pd.licencia is null, \"No\", \"Sí\") as \"licencia\",
        if(pd.rfc is null, \"No\", \"Sí\") as \"rfc\",
        if(pd.tarjetaCirculacion is null, \"No\", \"Sí\") as \"tarjetaCirculacion\"
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id_cliente = cl.id  inner join distritales dis on dis.id = grt.id_distritales
        where  est.id = 2 #id de estacionamiento a consultar
        and date_format(pd.created_at, '%Y-%m-%d') between '2020-01-01' and '$hoy' ;
        ";

        $result1 = DB::connection('mysql2')->SELECT($pensionActiva);

        return view('actualizapen.index')->with('detalles', $result1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = DB::connection('mysql2')->table('Clientes')->get();

        $proyectos = DB::connection('mysql2')->table('estacionamientos')->get();

        return view('actualizaPen.create', compact('proyectos', 'clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $detalle = Detalle::create([
            'montoPension' => $request->montoPension,
            'tipoPension' => $request->tipoPension,
            'id_estacionamiento' => $request->id_estacionamiento,
            'id_Cliente' => $request->id_Cliente,
            'rfc' => $request->rfc,
            'noTarjeta' => $request->noTarjeta,
            'fecha_limite' => $request->fecha_limite,
        ]);

        $contrato = '';
        $contrato = request()->file('contrato');
        if ($contrato != null) {
            $datascontrato = $contrato->get();
            $nombre_contrato = $contrato->getBasename();
            $cdom = file_get_contents($contrato);
            $cdomdata = base64_encode($cdom);
            $detalle->contrato = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $detalle_actualiza = Detalle::find($detalle->id);

        $solicitudContrato = '';
        $solicitudContrato = request()->file('solicitudContrato');
        if ($solicitudContrato != null) {
            $datassolicitudContrato = $solicitudContrato->get();
            $nombre_solicitudContrato = $solicitudContrato->getBasename();
            $cdom = file_get_contents($solicitudContrato);
            $cdomdata = base64_encode($cdom);
            $detalle->solicitudContrato = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $comprobanteDomicilio = '';
        $comprobanteDomicilio = request()->file('comprobanteDomicilio');
        if ($comprobanteDomicilio != null) {
            $datassolicitudContrato = $comprobanteDomicilio->get();
            $nombre_solicitudContrato = $comprobanteDomicilio->getBasename();
            $cdom = file_get_contents($comprobanteDomicilio);
            $cdomdata = base64_encode($cdom);
            $detalle->comprobanteDomicilio = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $ine = '';
        $ine = request()->file('ine');
        if ($ine != null) {
            $datassolicitudContrato = $ine->get();
            $nombre_solicitudContrato = $ine->getBasename();
            $cdom = file_get_contents($ine);
            $cdomdata = base64_encode($cdom);
            $detalle->ine = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $licencia = '';
        $licencia = request()->file('licencia');
        if ($licencia != null) {
            $datassolicitudContrato = $licencia->get();
            $nombre_solicitudContrato = $licencia->getBasename();
            $cdom = file_get_contents($licencia);
            $cdomdata = base64_encode($cdom);
            $detalle->licencia = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $tarjetaCirculacion = '';
        $tarjetaCirculacion = request()->file('tarjetaCirculacion');
        if ($tarjetaCirculacion != null) {
            $datassolicitudContrato = $tarjetaCirculacion->get();
            $nombre_solicitudContrato = $tarjetaCirculacion->getBasename();
            $cdom = file_get_contents($tarjetaCirculacion);
            $cdomdata = base64_encode($cdom);
            $detalle->tarjetaCirculacion = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $detalle_actualiza->save();

        Flash::success('Registro actualizado satisfactoriamente.');

        if ($request->grupo != null) {
            $clientes = DB::connection('mysql2')->table('Clientes')
                ->where('id', '=', $request->id_Cliente)
                ->update(['Grupo' => $request->grupo]);
        }

        return redirect(route('actualizacionesPen.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ActualizaPen  $actualizaPen
     * @return \Illuminate\Http\Response
     */
    public function show(ActualizaPen $actualizaPen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ActualizaPen  $actualizaPen
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Detalle::select('id', 'id_estacionamiento', 'montoPension', 'tipoPension', 'contrato', 'solicitudContrato', 'comprobanteDomicilio', 'ine', 'licencia', 'rfc', 'tarjetaCirculacion', 'noTarjeta', 'status', 'fecha_limite', 'horas_pension', 'placas', 'fechaInactivacion', 'fechaCancelacion')
            ->where('id', '=', request()->all()['pension'])
            ->where('id_Cliente', '=', $id)
            ->get();

        //$cliente_info = DB::connection('mysql2')->table('Clientes')->select('grupo')->where('id', '=', $id)->first();
        $proyectos = DB::connection('mysql2')->table('estacionamientos')->get();

        return view('actualizaPen.edit', compact('proyectos'))->with('pensions', $cliente[0]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ActualizaPen  $actualizaPen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActualizaPen $actualizaPen, $id)
    {
        $input = request()->all();
        //dd($actualizaPen);

        $detalle = Detalle::find($id);
        //dd($input['grupo'], $detalle);

        if (empty($detalle)) {
            Flash::error('Detalle not found');

            return redirect(route('actualizaPen.index'));
        }

        if ($input['id_estacionamiento']) {
            $detalle->id_estacionamiento = $input['id_estacionamiento'];
        }

        if ($input['montoPension']) {
            $detalle->montoPension = $input['montoPension'];
        }

        if ($input['tipoPension']) {
            $detalle->tipoPension = $input['tipoPension'];
        }

        if ($input['rfc']) {
            $detalle->rfc = $input['rfc'];
        }

        if ($input['tarjetaCirculacion']) {
            $detalle->tarjetaCirculacion = $input['tarjetaCirculacion'];
        }

        if ($input['noTarjeta']) {
            $detalle->noTarjeta = $input['noTarjeta'];
        }

        if ($input['status']) {
            $detalle->status = $input['status'];
        }

        if ($input['fecha_limite']) {
            $detalle->fecha_limite = $input['fecha_limite'];
        }

        $contrato = '';
        $contrato = request()->file('contrato');
        if ($contrato != null) {
            $datascontrato = $contrato->get();
            $nombre_contrato = $contrato->getBasename();
            $cdom = file_get_contents($contrato);
            $cdomdata = base64_encode($cdom);
            $detalle->contrato = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $solicitudContrato = '';
        $solicitudContrato = request()->file('solicitudContrato');
        if ($solicitudContrato != null) {
            $datassolicitudContrato = $solicitudContrato->get();
            $nombre_solicitudContrato = $solicitudContrato->getBasename();
            $cdom = file_get_contents($solicitudContrato);
            $cdomdata = base64_encode($cdom);
            $detalle->solicitudContrato = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $comprobanteDomicilio = '';
        $comprobanteDomicilio = request()->file('comprobanteDomicilio');
        if ($comprobanteDomicilio != null) {
            $datassolicitudContrato = $comprobanteDomicilio->get();
            $nombre_solicitudContrato = $comprobanteDomicilio->getBasename();
            $cdom = file_get_contents($comprobanteDomicilio);
            $cdomdata = base64_encode($cdom);
            $detalle->comprobanteDomicilio = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $ine = '';
        $ine = request()->file('ine');
        if ($ine != null) {
            $datassolicitudContrato = $ine->get();
            $nombre_solicitudContrato = $ine->getBasename();
            $cdom = file_get_contents($ine);
            $cdomdata = base64_encode($cdom);
            $detalle->ine = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $licencia = '';
        $licencia = request()->file('licencia');
        if ($licencia != null) {
            $datassolicitudContrato = $licencia->get();
            $nombre_solicitudContrato = $licencia->getBasename();
            $cdom = file_get_contents($licencia);
            $cdomdata = base64_encode($cdom);
            $detalle->licencia = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $tarjetaCirculacion = '';
        $tarjetaCirculacion = request()->file('tarjetaCirculacion');
        if ($tarjetaCirculacion != null) {
            $datassolicitudContrato = $tarjetaCirculacion->get();
            $nombre_solicitudContrato = $tarjetaCirculacion->getBasename();
            $cdom = file_get_contents($tarjetaCirculacion);
            $cdomdata = base64_encode($cdom);
            $detalle->tarjetaCirculacion = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        Flash::success('Registro actualizado satisfactoriamente.');

        $detalle->save();

        if ($input['grupo'] != null) {
            $clientes = DB::connection('mysql2')->table('Clientes')
                ->where('id', '=', $detalle->id_Cliente)
                ->update(['Grupo' => $input['grupo']]);
        }

        return redirect(route('actualizacionesPen.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ActualizaPen  $actualizaPen
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActualizaPen $actualizaPen, Request  $request)
    {
        /*$cliente = Detalle::select('id', 'id_estacionamiento', 'montoPension', 'tipoPension', 'contrato', 'solicitudContrato', 'comprobanteDomicilio', 'ine', 'licencia', 'rfc', 'tarjetaCirculacion', 'noTarjeta', 'status', 'fecha_limite', 'horas_pension', 'placas', 'fechaInactivacion', 'fechaCancelacion')
            ->where('id', '=', $request->idpension)
            ->where('id_Cliente', '=', $request->idcliente)->get();*/

        $consulta = Detalle::find($request->idpension);

        if (empty($consulta)) {
            Flash::error('Registro not found');

            return redirect(route('actualizacionesPen.index'));
        }

        $consulta->delete();

        Flash::success('Pago eliminado satisfactoriamente.');

        return redirect(route('actualizacionesPen.index'));
    }
}
