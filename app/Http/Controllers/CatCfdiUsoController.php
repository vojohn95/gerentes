<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCatCfdiUsoRequest;
use App\Http\Requests\UpdateCatCfdiUsoRequest;
use App\Repositories\CatCfdiUsoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CatCfdiUsoController extends AppBaseController
{
    /** @var  CatCfdiUsoRepository */
    private $catCfdiUsoRepository;

    public function __construct(CatCfdiUsoRepository $catCfdiUsoRepo)
    {
        $this->catCfdiUsoRepository = $catCfdiUsoRepo;
    }

    /**
     * Display a listing of the CatCfdiUso.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $catCfdiUsos = $this->catCfdiUsoRepository->all();

        return view('cat_cfdi_usos.index')
            ->with('catCfdiUsos', $catCfdiUsos);
    }

    /**
     * Show the form for creating a new CatCfdiUso.
     *
     * @return Response
     */
    public function create()
    {
        return view('cat_cfdi_usos.create');
    }

    /**
     * Store a newly created CatCfdiUso in storage.
     *
     * @param CreateCatCfdiUsoRequest $request
     *
     * @return Response
     */
    public function store(CreateCatCfdiUsoRequest $request)
    {
        $input = $request->all();

        $catCfdiUso = $this->catCfdiUsoRepository->create($input);

        Flash::success('Cat Cfdi Uso saved successfully.');

        return redirect(route('catCfdiUsos.index'));
    }

    /**
     * Display the specified CatCfdiUso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $catCfdiUso = $this->catCfdiUsoRepository->find($id);

        if (empty($catCfdiUso)) {
            Flash::error('Cat Cfdi Uso not found');

            return redirect(route('catCfdiUsos.index'));
        }

        return view('cat_cfdi_usos.show')->with('catCfdiUso', $catCfdiUso);
    }

    /**
     * Show the form for editing the specified CatCfdiUso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $catCfdiUso = $this->catCfdiUsoRepository->find($id);

        if (empty($catCfdiUso)) {
            Flash::error('Cat Cfdi Uso not found');

            return redirect(route('catCfdiUsos.index'));
        }

        return view('cat_cfdi_usos.edit')->with('catCfdiUso', $catCfdiUso);
    }

    /**
     * Update the specified CatCfdiUso in storage.
     *
     * @param int $id
     * @param UpdateCatCfdiUsoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCatCfdiUsoRequest $request)
    {
        $catCfdiUso = $this->catCfdiUsoRepository->find($id);

        if (empty($catCfdiUso)) {
            Flash::error('Cat Cfdi Uso not found');

            return redirect(route('catCfdiUsos.index'));
        }

        $catCfdiUso = $this->catCfdiUsoRepository->update($request->all(), $id);

        Flash::success('Cat Cfdi Uso updated successfully.');

        return redirect(route('catCfdiUsos.index'));
    }

    /**
     * Remove the specified CatCfdiUso from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $catCfdiUso = $this->catCfdiUsoRepository->find($id);

        if (empty($catCfdiUso)) {
            Flash::error('Cat Cfdi Uso not found');

            return redirect(route('catCfdiUsos.index'));
        }

        $this->catCfdiUsoRepository->delete($id);

        Flash::success('Cat Cfdi Uso deleted successfully.');

        return redirect(route('catCfdiUsos.index'));
    }
}
