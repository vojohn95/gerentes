<?php

namespace App\Http\Controllers;

use App\Models\Estacionamientos;
use App\Models\facturas;
use App\Models\ticket;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

class InformeController extends Controller
{
    public function index(){
        $hoy = new DateTime();
        $id = Auth::user()->id_est;
        $ticket_mes = Ticket::whereMonth('created_at', '=', $hoy->format('m'))
            ->where('id_est', '=', $id)
            ->count();
        $rechazo_mes = Ticket::whereMonth('created_at', '=', $hoy->format('m'))
            ->where('id_est', '=', $id)
            ->where('estatus', '=', 'Rechazo')
            ->count();
        $facturas_mes = Ticket::whereMonth('created_at', '=', $hoy->format('m'))
            ->where('id_est', '=', $id)
            ->where('estatus', '=', 'valido')
            ->count();
        $facturado = facturas::whereMonth('fecha_timbrado', '=', $hoy->format('m'))
            ->join('autof_tickets','autof_tickets.id','=','auto_facts.id_ticketAF')
            ->where('autof_tickets.id_est', '=', $id)
            ->sum('auto_facts.total_factura');
        $graf_mes = $ticket_mes - $rechazo_mes - $facturas_mes;
        $rfc = DB::table('autof_tickets')
            ->select('RFC', DB::raw('count(*) as fact'))
            ->where('estatus', '=', 'valido')
            ->where('id_est', '=', $id)
            ->groupBy('RFC')
            ->orderBy('fact', 'desc')
            ->take(5)
            ->get();
        //dd($busqueda);
        $estacionamiento = Estacionamientos::where('no_est','=',$id)->get();
        if (empty($estacionamiento)) {
            Flash::error('Estacionamiento no encontrado');
            return redirect(url('/'));
        }
        return view('informe.dashboard')
            ->with('estacionamiento', $estacionamiento[0])
            ->with('rfc', $rfc)
            ->with('factura', $facturas_mes)
            ->with('facturado', $facturado)
            ->with('rechazo', $rechazo_mes)
            ->with('TotalGrafica', $graf_mes)
            ->with('facturas',$facturas_mes)
            ->with('rechazos',$rechazo_mes)
            ->with('tickets',$ticket_mes);
    }
}
