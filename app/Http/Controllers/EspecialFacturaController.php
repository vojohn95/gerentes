<?php

namespace App\Http\Controllers;

use App\Models\ticket;
use App\Funciones\Timbrar;
use Laracasts\Flash\Flash;
use App\Models\GerentesPark;
use Illuminate\Http\Request;
use App\Models\EspecialFactura;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EspecialFacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('facturasespeciales.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relacion_fact = GerentesPark::select('id_parks')->where('id_gerentes', Auth::user()->id)->get();

        return view('facturasespeciales.create')->with('estacionamientos', $relacion_fact);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orgs = DB::table('orgs')->get();

        $img = '';
        $fact = request()->validate([
            'no_est' => ['required'],
            'Nombre_cliente' => ['required', 'string'],
            'RFC' => ['required', 'string', 'max:13'],
            'Correo' => ['required', 'email'],
            'Uso_CFDI' => ['required'],
            'concepto' => ['required'],
            'metodo_pago' => ['required'],
            'forma_pago' => ['required'],
            'Cantidad' => ['required', 'string'],
            'Precio' => ['required', 'string'],
            //'Comentario' => ['required', 'string'],
        ]);

        $cadena = $orgs[0]->RFC;
        $RFC = strtoupper($cadena);
        $foto = request()->file('foto');
        if ($foto != null) {
            $dataImg = $foto->get();
            $img = base64_encode($dataImg);
            //dd($img);
        }

        $fecha = date('Y-m-d');
        $fecha2 = date('H:i:s');

        $numfact = ticket::max('factura');
        //dd($numfact);

        $ticket = new ticket();
        $ticket->fill([
            'id_cliente' => 1,
            'id_est' => $request->no_est,
            'id_tipo' => 3,
            'id_CentralUser' => 1,
            'factura' => $numfact + 1,
            'no_ticket' => $request->no_ticket,
            'total_ticket' => $request->Precio,
            'fecha_emision' => $fecha,
            'estatus' => 'validar',
            'UsoCFDI' => $request->Uso_CFDI,
            'metodo_pago' => $request->metodo_pago,
            'forma_pago' => $request->forma_pago,
            'RFC' => $fact['RFC'],
            'Razon_social' => $request->Nombre_cliente,
            'email' => $request->Correo,
            'coment' => $request->comment,
            'imagen' => $img,
            'pi' => Auth::user()->id_est
        ]);
        $ticket->save();

        /*$validar = DB::table('auto_facts')->where('id_ticketAF', '=', $ticket->id)->get();
        if ($validar->isNotEmpty()) {
            Flash::error('Ticket timbrado previamente');
            return redirect(route('facturacionespeciales.index'));
        }*/
        /*  Linea para que no se timbre estacionamiento 1
         * if ($id->id_est == 1) {
            Flash::error('Ticket sin actualizar estacionamiento');
            return redirect(route('tickets.index'));
        }*/
        $timbrar = new Timbrar();
        $timbrar->Factura($ticket);


        return redirect(route('facturacionespeciales.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EspecialFactura  $especialFactura
     * @return \Illuminate\Http\Response
     */
    public function show(EspecialFactura $especialFactura)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EspecialFactura  $especialFactura
     * @return \Illuminate\Http\Response
     */
    public function edit(EspecialFactura $especialFactura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EspecialFactura  $especialFactura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EspecialFactura $especialFactura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EspecialFactura  $especialFactura
     * @return \Illuminate\Http\Response
     */
    public function destroy(EspecialFactura $especialFactura)
    {
        //
    }
}
