<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Models\Pago;
use App\Models\Banco;
use App\Models\Detalle;
use App\Models\catBanco;
use App\Models\Pensions;
use App\Models\FormaPago;
use Illuminate\Http\Request;
use App\Repositories\PagoRepository;
use App\Http\Requests\CreatePagoRequest;
use App\Http\Requests\UpdatePagoRequest;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;


class PagoController extends AppBaseController
{
    /** @var  PagoRepository */
    private $pagoRepository;

    public function __construct(PagoRepository $pagoRepo)
    {
        $this->pagoRepository = $pagoRepo;
    }

    /**
     * Display a listing of the Pago.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pagos = $this->pagoRepository->all();
        //$pagos = Pago::all();
        $pagos = "
        select est.nombre_proyecto as proyecto,  pd.id as numero_pension,
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as cliente,
        cl.rfc as RFC, cl.factura as factura_o_no,
        pg.fechaPago as \"FechaPago\", tipoPago as \"FormaPago\", pg.total as \"TotalPago\", pg.id as \"idPago\"
        from pensionesDet pd inner join pagos pg
        on pg.idPension = pd.id inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id_cliente = cl.id
        where est.no_estacionamiento = 7
        and pg.mes = 'Mayo'
        and pg.año = '2021'
        ";
        $result1 = DB::connection('mysql2')->SELECT($pagos);
        //dd($result1);

        return view('pagos.index')
            ->with('pagos', $result1);
    }

    /**
     * Show the form for creating a new Pago.
     *
     * @return Response
     */
    public function create()
    {
        $pensiones = Detalle::get();
        $bancos = catBanco::get();
        $formapagos = DB::connection('mysql2')->table('cat_formaPago')
            ->select('*')
            ->get();

        $metodopagos = DB::connection('mysql2')->table('cat_Metodopago')
            ->select('*')
            ->get();
        //dd($formapagos);

        return view('pagos.create')->with('pensiones', $pensiones)->with('bancos', $bancos)->with('formaPagos', $formapagos)
            ->with('metodoPagos', $metodopagos);
    }

    /**
     * Store a newly created Pago in storage.
     *
     * @param CreatePagoRequest $request
     *
     * @return Response
     */
    public function store(CreatePagoRequest $request)
    {
        $input = $request->all();

        $comprobante_pago = '';
        $comprobante_pago = request()->file('comprobante_pago');
        if ($comprobante_pago != null) {
            $datacont = $comprobante_pago->get();
            $nombre_contrato = $comprobante_pago->getBasename();
            $cont = file_get_contents($comprobante_pago);
            $contdata = base64_encode($cont);
            $input['comprobante_pago'] = $contdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $pago = $this->pagoRepository->create($input);

        //dd($pago);

        Flash::success('Pago añadido satisfactoriamente.');

        return redirect(route('pagos.index'));
    }

    /**
     * Display the specified Pago.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pago = $this->pagoRepository->find($id);

        if (empty($pago)) {
            Flash::error('Pago not found');

            return redirect(route('pagos.index'));
        }

        return view('pagos.show')->with('pago', $pago);
    }

    /**
     * Show the form for editing the specified Pago.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pago = $this->pagoRepository->find($id);

        if (empty($pago)) {
            Flash::error('Pago not found');

            return redirect(route('pagos.index'));
        }

        $pensiones = Detalle::get();
        $bancos = catBanco::get();
        $formapagos = DB::connection('mysql2')->table('cat_formaPago')
            ->select('*')
            ->get();

        $metodopagos = DB::connection('mysql2')->table('cat_Metodopago')
            ->select('*')
            ->get();

        return view('pagos.edit')->with('pago', $pago)->with('pensiones', $pensiones)->with('bancos', $bancos)->with('formaPagos', $formapagos)
            ->with('metodoPagos', $metodopagos)
            ->with('idPension', $pago->idPension)->with('id_bancos', $pago->id_bancos)->with('id_metodoPago', $pago->id_metodoPago)
            ->with('id_formaPago', $pago->id_formaPago);
    }

    /**
     * Update the specified Pago in storage.
     *
     * @param int $id
     * @param UpdatePagoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePagoRequest $request)
    {
        $input = request()->all();
        //dd($input);

        $pago = Pago::find($id);
        //dd($pago);

        if (empty($pago)) {
            Flash::error('Pago not found');

            return redirect(route('pagos.index'));
        }

        /*if($input['total']){
            $pago->total = $input['total'];
        }

        if($input['subtotal']){
            $pago->subtotal = $input['subtotal'];
        }

        if($input['iva']){
            $pago->iva = $input['iva'];
        }

        if($input['tipoPago']){
            $pago->tipoPago = $input['tipoPago'];
        }

        if($input['idPension']){
            $pago->idPension = $input['idPension'];
        }

        if($input['fechaPago']){
            $pago->fechaPago = $input['fechaPago'];
        }

        if($input['id_bancos']){
            $pago->id_bancos = $input['id_bancos'];
        }

        if($input['mes']){
            $pago->mes = $input['mes'];
        }

        if($input['año']){
            $pago->año = $input['año'];
        }*/

        $comprobantePago = '';
        $comprobantePago = request()->file('comprobante_pago');
        if ($comprobantePago != null) {
            $datascomp = $comprobantePago->get();
            $nombre_cdomici = $comprobantePago->getBasename();
            $cdom = file_get_contents($comprobantePago);
            $cdomdata = base64_encode($cdom);
            $pago->comprobante_pago = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $pago->save();

        Flash::success('Pago actualizado satisfactoriamente.');

        return redirect(route('pagos.index'));
    }

    /**
     * Remove the specified Pago from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pago = $this->pagoRepository->find($id);

        if (empty($pago)) {
            Flash::error('Pago not found');

            return redirect(route('pagos.index'));
        }

        $this->pagoRepository->delete($id);

        Flash::success('Pago eliminado satisfactoriamente.');

        return redirect(route('pagos.index'));
    }

    public function downloadcompPago($id)
    {
        $pdf = Pago::find($id);
        $bin = base64_decode($pdf->comprobante_pago);
        $path = "xml_down/" . $id . ".png";
        //dd($path);
        // Obtain the original content (usually binary data)
        // Load GD resource from binary data
        $im = imageCreateFromString($bin);
        //dd($im);

        // Make sure that the GD library was able to load the image
        // This is important, because you should not miss corrupted or unsupported images
        if (!$im) {
            die('Base64 value is not a valid image');
        }

        file_put_contents($path, $bin);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($path) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        flush(); // Flush system output buffer
        readfile($path);
        unlink($path);
    }
}
