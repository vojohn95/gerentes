<?php

namespace App\Http\Controllers;

use App\Models\PensionActiva;
use Illuminate\Http\Request;
use Auth;
use DB;

class PensionActivaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_est = Auth::user()->id_est;
        $pensionActiva = "select dis.clave_distrito as distrito,
        dis.nombre as distrital,
        grt.name as gerente,
        grt.email as \"correo de gerente\",
        est.no_estacionamiento as \"Número de Proyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"Número de Pensión\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombre de pensionado\",
        case when cl.estatus = 'A' then 'Activa'
        when cl.estatus = 'I' then 'Inactiva'
        end as estado,
        if(pd.contrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as contrato,
        if(pd.solicitudContrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as solicitudContrato,
        if(pd.comprobanteDomicilio is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as comprobanteDomicilio,
        if(pd.ine is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as ine,
        if(pd.licencia is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as licencia,
        if(pd.rfc is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as rfc,
        if(pd.tarjetaCirculacion is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as tarjetaCirculacion
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id = cl.id_pension inner join distritales dis on dis.id = grt.id_distritales
        where  grt.id = $id_est";
        $result1 = DB::connection('mysql2')->SELECT($pensionActiva);
        //dd($result1);

        return view('pensionactivas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PensionActiva  $pensionActiva
     * @return \Illuminate\Http\Response
     */
    public function show(PensionActiva $pensionActiva)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PensionActiva  $pensionActiva
     * @return \Illuminate\Http\Response
     */
    public function edit(PensionActiva $pensionActiva)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PensionActiva  $pensionActiva
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PensionActiva $pensionActiva)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PensionActiva  $pensionActiva
     * @return \Illuminate\Http\Response
     */
    public function destroy(PensionActiva $pensionActiva)
    {
        //
    }
}
