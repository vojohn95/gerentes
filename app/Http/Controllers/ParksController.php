<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateParksRequest;
use App\Http\Requests\UpdateParksRequest;
use App\Repositories\ParksRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ParksController extends AppBaseController
{
    /** @var  ParksRepository */
    private $parksRepository;

    public function __construct(ParksRepository $parksRepo)
    {
        $this->parksRepository = $parksRepo;
    }

    /**
     * Display a listing of the Parks.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $parks = $this->parksRepository->all();

        return view('parks.index')
            ->with('parks', $parks);
    }

    /**
     * Show the form for creating a new Parks.
     *
     * @return Response
     */
    public function create()
    {
        return view('parks.create');
    }

    /**
     * Store a newly created Parks in storage.
     *
     * @param CreateParksRequest $request
     *
     * @return Response
     */
    public function store(CreateParksRequest $request)
    {
        $input = $request->all();

        $parks = $this->parksRepository->create($input);

        Flash::success('Parks saved successfully.');

        return redirect(route('parks.index'));
    }

    /**
     * Display the specified Parks.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $parks = $this->parksRepository->find($id);

        if (empty($parks)) {
            Flash::error('Parks not found');

            return redirect(route('parks.index'));
        }

        return view('parks.show')->with('parks', $parks);
    }

    /**
     * Show the form for editing the specified Parks.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $parks = $this->parksRepository->find($id);

        if (empty($parks)) {
            Flash::error('Parks not found');

            return redirect(route('parks.index'));
        }

        return view('parks.edit')->with('parks', $parks);
    }

    /**
     * Update the specified Parks in storage.
     *
     * @param int $id
     * @param UpdateParksRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParksRequest $request)
    {
        $parks = $this->parksRepository->find($id);

        if (empty($parks)) {
            Flash::error('Parks not found');

            return redirect(route('parks.index'));
        }

        $parks = $this->parksRepository->update($request->all(), $id);

        Flash::success('Parks updated successfully.');

        return redirect(route('parks.index'));
    }

    /**
     * Remove the specified Parks from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $parks = $this->parksRepository->find($id);

        if (empty($parks)) {
            Flash::error('Parks not found');

            return redirect(route('parks.index'));
        }

        $this->parksRepository->delete($id);

        Flash::success('Parks deleted successfully.');

        return redirect(route('parks.index'));
    }
}
