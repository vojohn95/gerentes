<?php

namespace App\Repositories;

use App\Models\facturas;
use App\Repositories\BaseRepository;

/**
 * Class facturasRepository
 * @package App\Repositories
 * @version October 22, 2019, 4:16 pm UTC
*/

class facturasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_ticketAF',
        'serie',
        'tipo_doc',
        'folio',
        'fecha_timbrado',
        'uuid',
        'subtotal_factura',
        'iva_factura',
        'total_factura',
        'XML',
        'PDF',
        'estatus'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return facturas::class;
    }
}
