<?php

namespace App\Repositories;

use App\Models\ImgIngresos;
use App\Repositories\BaseRepository;

/**
 * Class ImgIngresosRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:30 pm UTC
*/

class ImgIngresosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_ingreso',
        'foto',
        'firma'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ImgIngresos::class;
    }
}
