<?php

namespace App\Repositories;

use App\Models\MetodoPago;
use App\Repositories\BaseRepository;

/**
 * Class MetodoPagoRepository
 * @package App\Repositories
 * @version June 2, 2021, 9:50 am UTC
*/

class MetodoPagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'claveSat',
        'descricion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MetodoPago::class;
    }
}
