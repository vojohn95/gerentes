<?php

namespace App\Repositories;

use App\Models\CentralUser;
use App\Repositories\BaseRepository;

/**
 * Class CentralUserRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:31 pm UTC
*/

class CentralUserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CentralUser::class;
    }
}
