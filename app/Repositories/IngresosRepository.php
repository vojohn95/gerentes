<?php

namespace App\Repositories;

use App\Models\Ingresos;
use App\Repositories\BaseRepository;

/**
 * Class IngresosRepository
 * @package App\Repositories
 * @version October 18, 2019, 7:29 pm UTC
*/

class IngresosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_empresa',
        'id_banco',
        'numero',
        'venta',
        'fecha_recoleccion',
        'importe',
        'valido',
        'coment',
        'tipo',
        'id_gerente'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ingresos::class;
    }
}
