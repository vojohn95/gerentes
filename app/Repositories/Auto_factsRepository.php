<?php

namespace App\Repositories;

use App\Models\Auto_facts;
use App\Repositories\BaseRepository;

/**
 * Class Auto_factsRepository
 * @package App\Repositories
 * @version October 22, 2019, 3:09 pm UTC
*/

class Auto_factsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_ticketAF',
        'serie',
        'tipo_doc',
        'folio',
        'fecha_timbrado',
        'uuid',
        'subtotal_factura',
        'iva_factura',
        'total_factura',
        'XML',
        'PDF',
        'estatus'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Auto_facts::class;
    }
}
