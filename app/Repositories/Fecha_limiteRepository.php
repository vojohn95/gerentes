<?php

namespace App\Repositories;

use App\Models\Fecha_limite;
use App\Repositories\BaseRepository;

/**
 * Class Fecha_limiteRepository
 * @package App\Repositories
 * @version September 20, 2019, 6:41 pm UTC
*/

class Fecha_limiteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Fecha_limite::class;
    }
}
