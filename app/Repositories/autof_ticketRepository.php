<?php

namespace App\Repositories;

use App\Models\autof_ticket;
use App\Repositories\BaseRepository;

/**
 * Class autof_ticketRepository
 * @package App\Repositories
 * @version October 22, 2019, 3:07 pm UTC
*/

class autof_ticketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_cliente',
        'id_est',
        'id_tipo',
        'id_CentralUser',
        'factura',
        'no_ticket',
        'total_ticket',
        'fecha_emision',
        'imagen',
        'estatus',
        'UsoCFDI',
        'metodo_pago',
        'forma_pago',
        'RFC',
        'Razon_social',
        'email',
        'calle',
        'no_ext',
        'no_int',
        'colonia',
        'municipio',
        'estado',
        'cp',
        'coment',
        'timb'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return autof_ticket::class;
    }
}
