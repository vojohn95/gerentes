<?php

namespace App\Repositories;

use App\Models\EmpresaColect;
use App\Repositories\BaseRepository;

/**
 * Class EmpresaColectRepository
 * @package App\Repositories
 * @version September 19, 2019, 7:26 pm UTC
*/

class EmpresaColectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'empresa'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EmpresaColect::class;
    }
}
