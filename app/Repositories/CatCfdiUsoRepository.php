<?php

namespace App\Repositories;

use App\Models\CatCfdiUso;
use App\Repositories\BaseRepository;

/**
 * Class CatCfdiUsoRepository
 * @package App\Repositories
 * @version June 2, 2021, 9:38 am UTC
*/

class CatCfdiUsoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'claveSat',
        'descricion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CatCfdiUso::class;
    }
}
