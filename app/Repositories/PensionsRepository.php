<?php

namespace App\Repositories;

use App\Models\Pensions;
use App\Repositories\BaseRepository;

/**
 * Class PensionsRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:24 pm UTC
*/

class PensionsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_cliente',
        'id_tipo_pen',
        'id_tipo_pago',
        'id_forma_pago',
        'id_fecha_limite',
        'num_pen',
        'foto_comprobante',
        'no_est',
        'factura',
        'costo_pension',
        'recargos',
        'venta_tarjeta',
        'repo_tarjeta',
        'impor_pago',
        'mes_pago',
        'cargado_por'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pensions::class;
    }
}
