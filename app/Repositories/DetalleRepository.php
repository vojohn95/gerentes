<?php

namespace App\Repositories;

use App\Models\Detalle;
use App\Repositories\BaseRepository;

/**
 * Class DetalleRepository
 * @package App\Repositories
 * @version May 17, 2021, 3:57 am UTC
*/

class DetalleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_estacionamiento',
        'montoPension',
        'tipoPension',
        'contrato',
        'solicitudContrato',
        'comprobanteDomicilio',
        'ine',
        'licencia',
        'rfc',
        'tarjetaCirculacion',
        'noTarjeta',
        'status',
        'fecha_limite'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Detalle::class;
    }
}
