<?php

namespace App\Repositories;

use App\Models\Estacionamientos;
use App\Repositories\BaseRepository;

/**
 * Class EstacionamientosRepository
 * @package App\Repositories
 * @version October 22, 2019, 3:33 pm UTC
*/

class EstacionamientosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_marca',
        'id_org',
        'nombre',
        'dist_regio',
        'direccion',
        'calle',
        'no_ext',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'cp',
        'latitud',
        'longitud',
        'Facturable',
        'Automatico',
        'cajones',
        'folio',
        'serie',
        'correo',
        'pensiones',
        'cortesias',
        'observaciones',
        'escuela'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Estacionamientos::class;
    }
}
