<?php

namespace App\Repositories;

use App\Models\Parks;
use App\Repositories\BaseRepository;

/**
 * Class ParksRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:28 pm UTC
*/

class ParksRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_marca',
        'id_org',
        'nombre',
        'dist_regio',
        'direccion',
        'calle',
        'no_ext',
        'colonia',
        'municipio',
        'estado',
        'cp',
        'latitud',
        'longitud',
        'Facturable',
        'Automatico',
        'cajones',
        'folio',
        'serie',
        'correo',
        'pensiones',
        'cortesias',
        'cobro',
        'observaciones'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Parks::class;
    }
}
