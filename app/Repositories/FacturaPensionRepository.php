<?php

namespace App\Repositories;

use App\Models\FacturaPension;
use App\Repositories\BaseRepository;

/**
 * Class FacturaPensionRepository
 * @package App\Repositories
 * @version May 17, 2021, 3:24 am UTC
*/

class FacturaPensionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_pen',
        'serie',
        'folio',
        'fecha_timbrado',
        'uuid',
        'subtotal_factura',
        'iva_factura',
        'total_factura',
        'XML',
        'PDF',
        'estatus',
        'id_usoCFDI',
        'id_metodoPago'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FacturaPension::class;
    }
}
