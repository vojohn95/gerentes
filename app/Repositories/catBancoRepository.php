<?php

namespace App\Repositories;

use App\Models\catBanco;
use App\Repositories\BaseRepository;

/**
 * Class catBancoRepository
 * @package App\Repositories
 * @version May 17, 2021, 7:05 am UTC
*/

class catBancoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'banco'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return catBanco::class;
    }
}
