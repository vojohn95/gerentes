<?php

namespace App\Repositories;

use App\Models\ticket;
use App\Repositories\BaseRepository;

/**
 * Class ticketRepository
 * @package App\Repositories
 * @version October 22, 2019, 4:15 pm UTC
*/

class ticketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_cliente',
        'id_est',
        'id_tipo',
        'id_CentralUser',
        'factura',
        'no_ticket',
        'total_ticket',
        'fecha_emision',
        'imagen',
        'estatus',
        'UsoCFDI',
        'metodo_pago',
        'forma_pago',
        'RFC',
        'Razon_social',
        'email',
        'calle',
        'no_ext',
        'no_int',
        'colonia',
        'municipio',
        'estado',
        'cp',
        'coment',
        'timb'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ticket::class;
    }
}
