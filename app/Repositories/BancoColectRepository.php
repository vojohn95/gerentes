<?php

namespace App\Repositories;

use App\Models\BancoColect;
use App\Repositories\BaseRepository;

/**
 * Class BancoColectRepository
 * @package App\Repositories
 * @version September 19, 2019, 7:25 pm UTC
*/

class BancoColectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'banco'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BancoColect::class;
    }
}
