<?php

namespace App\Repositories;

use App\Models\ClienteInfos;
use App\Repositories\BaseRepository;

/**
 * Class ClienteInfosRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:19 pm UTC
*/

class ClienteInfosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_cliente',
        'RFC',
        'Razon_social',
        'calle',
        'no_ext',
        'no_int',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'cp',
        'act_na',
        'ine',
        'tar_circ',
        'placas',
        'tel',
        'pension'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ClienteInfos::class;
    }
}
