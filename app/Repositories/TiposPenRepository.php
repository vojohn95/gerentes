<?php

namespace App\Repositories;

use App\Models\TiposPen;
use App\Repositories\BaseRepository;

/**
 * Class TiposPenRepository
 * @package App\Repositories
 * @version September 20, 2019, 6:37 pm UTC
*/

class TiposPenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TiposPen::class;
    }
}
