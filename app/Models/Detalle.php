<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Detalle
 * @package App\Models
 * @version May 17, 2021, 3:57 am UTC
 *
 * @property \App\Models\Estacionamiento $idEstacionamiento
 * @property \Illuminate\Database\Eloquent\Collection $clientes
 * @property \Illuminate\Database\Eloquent\Collection $facturaPensiones
 * @property \Illuminate\Database\Eloquent\Collection $pagos
 * @property integer $id_estacionamiento
 * @property number $montoPension
 * @property string $tipoPension
 * @property string $contrato
 * @property string $solicitudContrato
 * @property string $comprobanteDomicilio
 * @property string $ine
 * @property string $licencia
 * @property string $rfc
 * @property string $tarjetaCirculacion
 * @property integer $noTarjeta
 * @property string $status
 * @property string $fecha_limite
 */
class Detalle extends Model
{
    //use SoftDeletes;

    public $table = 'pensionesDet';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];

    public $connection = "mysql2";

    public $fillable = [
        'id_estacionamiento',
        'id_Cliente',
        'montoPension',
        'tipoPension',
        'contrato',
        'solicitudContrato',
        'comprobanteDomicilio',
        'ine',
        'licencia',
        'rfc',
        'tarjetaCirculacion',
        'noTarjeta',
        'status',
        'fecha_limite'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_estacionamiento' => 'integer',
        'montoPension' => 'float',
        'tipoPension' => 'string',
        'contrato' => 'string',
        'solicitudContrato' => 'string',
        'comprobanteDomicilio' => 'string',
        'ine' => 'string',
        'licencia' => 'string',
        'rfc' => 'string',
        'tarjetaCirculacion' => 'string',
        'noTarjeta' => 'integer',
        'status' => 'string',
        'fecha_limite' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_estacionamiento' => 'nullable|integer',
        'montoPension' => 'nullable|numeric',
        'tipoPension' => 'nullable|string|max:15',
        'contrato' => 'nullable',
        'solicitudContrato' => 'nullable',
        'comprobanteDomicilio' => 'nullable',
        'ine' => 'nullable',
        'licencia' => 'nullable',
        'rfc' => 'nullable|string',
        'tarjetaCirculacion' => 'nullable',
        'noTarjeta' => 'nullable',
        'status' => 'nullable|string|max:2',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'fecha_limite' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idEstacionamiento()
    {
        return $this->belongsTo(\App\Models\Estacionamiento::class, 'id_estacionamiento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function clientes()
    {
        return $this->hasMany(\App\Models\Cliente::class, 'id_pension');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function facturaPensiones()
    {
        return $this->hasMany(\App\Models\FacturaPensione::class, 'id_pen');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pagos()
    {
        return $this->hasMany(\App\Models\Pago::class, 'idPension');
    }
}
