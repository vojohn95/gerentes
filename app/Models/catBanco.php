<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class catBanco
 * @package App\Models
 * @version May 17, 2021, 7:05 am UTC
 *
 * @property integer $id
 * @property string $banco
 */
class catBanco extends Model
{
    //use SoftDeletes;

    public $table = 'catBancos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];

    public $connection = "mysql2";

    public $fillable = [
        'id',
        'banco'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'banco' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'banco' => 'nullable|string|max:45',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


}
