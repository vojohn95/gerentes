<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GerentesPark
 *
 * @property int $id
 * @property int $id_gerentes
 * @property int $id_parks
 * @property int|null $id_grupos
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Gerente $gerente
 * @property GruposProyecto|null $grupos_proyecto
 * @property Park $park
 *
 * @package App\Models
 */
class GerentesPark extends Model
{
	protected $table = 'gerentes_parks';

	protected $casts = [
		'id_gerentes' => 'int',
		'id_parks' => 'int',
		'id_grupos' => 'int'
	];

	protected $fillable = [
		'id_gerentes',
		'id_parks',
		'id_grupos'
	];

	public function gerente()
	{
		return $this->belongsTo(Gerente::class, 'id_gerentes');
	}

	public function grupos_proyecto()
	{
		return $this->belongsTo(GruposProyecto::class, 'id_grupos');
	}

	public function park()
	{
		return $this->belongsTo(Park::class, 'id_parks');
	}
}
