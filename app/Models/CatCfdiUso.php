<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class CatCfdiUso
 * @package App\Models
 * @version June 2, 2021, 9:38 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $facturaPensiones
 * @property string $claveSat
 * @property string $descricion
 */
class CatCfdiUso extends Model
{

    public $table = 'cat_CfdiUso';

    public $connection = "mysql2";

    public $fillable = [
        'claveSat',
        'descricion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'claveSat' => 'string',
        'descricion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function facturaPensiones()
    {
        return $this->hasMany(\App\Models\FacturaPensione::class, 'id_usoCFDI');
    }
}
