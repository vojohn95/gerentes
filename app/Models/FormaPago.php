<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FormaPago
 * @package App\Models
 * @version September 20, 2019, 6:40 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection pensions
 * @property string forma
 */
class FormaPago extends Model
{


    public $table = 'cat_formaPago';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'claveSat',
        'descricion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'claveSat' => 'string',
        'descricion' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    /*public static $rules = [
        'forma' => 'required'
    ];*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pensions()
    {
        return $this->hasMany(\App\Models\Pension::class, 'id_forma_pago');
    }
}
