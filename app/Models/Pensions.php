<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pensions
 * @package App\Models
 * @version September 19, 2019, 5:24 pm UTC
 *
 * @property \App\Models\User idCliente
 * @property \App\Models\FechaLimite idFechaLimite
 * @property \App\Models\FormaPago idFormaPago
 * @property \App\Models\TipoPago idTipoPago
 * @property \App\Models\TipoPen idTipoPen
 * @property integer id_cliente
 * @property integer id_tipo_pen
 * @property integer id_tipo_pago
 * @property integer id_forma_pago
 * @property integer id_fecha_limite
 * @property integer num_pen
 * @property string foto_comprobante
 * @property integer no_est
 * @property boolean factura
 * @property number costo_pension
 * @property number recargos
 * @property number venta_tarjeta
 * @property number repo_tarjeta
 * @property number impor_pago
 * @property integer mes_pago
 * @property string cargado_por
 */
class Pensions extends Model
{
    use SoftDeletes;

    public $table = 'pensions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'id_cliente',
        'id_tipo_pen',
        'id_tipo_pago',
        'id_formaPago',
        'id_fecha_limite',
        'num_pen',
        'foto_comprobante',
        'no_est',
        'factura',
        'costo_pension',
        'recargos',
        'venta_tarjeta',
        'repo_tarjeta',
        'impor_pago',
        'mes_pago',
        'cargado_por'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_cliente' => 'integer',
        'id_tipo_pen' => 'integer',
        'id_tipo_pago' => 'integer',
        'id_forma_pago' => 'integer',
        'id_fecha_limite' => 'integer',
        'num_pen' => 'integer',
        'foto_comprobante' => 'string',
        'no_est' => 'integer',
        'factura' => 'boolean',
        'costo_pension' => 'float',
        'recargos' => 'float',
        'venta_tarjeta' => 'float',
        'repo_tarjeta' => 'float',
        'impor_pago' => 'float',
        'mes_pago' => 'integer',
        'cargado_por' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_cliente' => 'required',
        'id_tipo_pen' => 'required',
        'id_tipo_pago' => 'required',
        'id_forma_pago' => 'required',
        'id_fecha_limite' => 'required',
        'num_pen' => 'required',
        'no_est' => 'required',
        'factura' => 'required',
        'costo_pension' => 'required',
        'recargos' => 'required',
        'venta_tarjeta' => 'required',
        'repo_tarjeta' => 'required',
        'impor_pago' => 'required',
        'mes_pago' => 'required',
        'cargado_por' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idCliente()
    {
        return $this->belongsTo(\App\Models\User::class, 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idFechaLimite()
    {
        return $this->belongsTo(\App\Models\FechaLimite::class, 'id_fecha_limite');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idFormaPago()
    {
        return $this->belongsTo(\App\Models\FormaPago::class, 'id_forma_pago');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idTipoPago()
    {
        return $this->belongsTo(\App\Models\TipoPago::class, 'id_tipo_pago');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idTipoPen()
    {
        return $this->belongsTo(\App\Models\TipoPen::class, 'id_tipo_pen');
    }
}
