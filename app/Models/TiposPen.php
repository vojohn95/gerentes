<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TiposPen
 * @package App\Models
 * @version September 20, 2019, 6:37 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection pensions
 * @property string tipo
 */
class TiposPen extends Model
{


    public $table = 'tipo_pen';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'tipo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tipo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tipo' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pensions()
    {
        return $this->hasMany(\App\Models\Pension::class, 'id_tipo_pen');
    }
}
