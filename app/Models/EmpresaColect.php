<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EmpresaColect
 * @package App\Models
 * @version September 19, 2019, 7:26 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property string empresa
 */
class EmpresaColect extends Model
{
    use SoftDeletes;

    public $table = 'empresa_colect';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'empresa'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'empresa' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'empresa' => 'required'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ingresos()
    {
        return $this->hasMany(\App\Models\Ingreso::class, 'id_empresa');
    }
}
