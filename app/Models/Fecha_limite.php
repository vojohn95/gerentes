<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Fecha_limite
 * @package App\Models
 * @version September 20, 2019, 6:41 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection pensions
 * @property string fecha
 */
class Fecha_limite extends Model
{


    public $table = 'fecha_limite';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'fecha'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fecha' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pensions()
    {
        return $this->hasMany(\App\Models\Pension::class, 'id_fecha_limite');
    }
}
