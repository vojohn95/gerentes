<?php

namespace App\Models;

use App\Models\GerentesPark;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class CentralUser
 * @package App\Models
 * @version September 19, 2019, 5:31 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection autofTickets
 * @property \Illuminate\Database\Eloquent\Collection logs
 * @property \Illuminate\Database\Eloquent\Collection roles
 * @property string name
 * @property string email
 * @property string|\Carbon\Carbon email_verified_at
 * @property string password
 * @property string remember_token
 */
class CentralUser extends Authenticatable
{

    public $table = 'gerente';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'password',
        'id_est'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'remember_token' => 'string'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_est' => 'required',
        'name' => 'required',
        'email' => 'required',
        'password' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function autofTickets()
    {
        return $this->hasMany(\App\Models\AutofTicket::class, 'id_CentralUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function logs()
    {
        return $this->hasMany(\App\Models\Log::class, 'id_central_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function roles()
    {
        return $this->hasMany(\App\Models\Role::class, 'id_central_user');
    }

    public function especial()
    {
        $especial = GerentesPark::where('id_gerentes', Auth::user()->id)->count();
        return $especial;
    }

    public function especialData()
    {
        $especial = GerentesPark::where('id_gerentes', Auth::user()->id)->get();
        return $especial;
    }
}
