<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FacturaPension
 * @package App\Models
 * @version May 17, 2021, 3:24 am UTC
 *
 * @property \App\Models\CatCfdiUso $idUsocfdi
 * @property \App\Models\CatMetodoPago $idMetodopago
 * @property \App\Models\PensionesDet $idPen
 * @property integer $id_pen
 * @property string $serie
 * @property string $folio
 * @property string|\Carbon\Carbon $fecha_timbrado
 * @property string $uuid
 * @property number $subtotal_factura
 * @property number $iva_factura
 * @property number $total_factura
 * @property string $XML
 * @property string $PDF
 * @property string $estatus
 * @property integer $id_usoCFDI
 * @property integer $id_metodoPago
 */
class FacturaPension extends Model
{
    //use SoftDeletes;

    public $table = 'facturaPensiones';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];

    public $connection = "mysql2";

    public $fillable = [
        'id_pen',
        'serie',
        'folio',
        'fecha_timbrado',
        'uuid',
        'subtotal_factura',
        'iva_factura',
        'total_factura',
        'XML',
        'PDF',
        'estatus',
        'id_usoCFDI',
        'id_metodoPago'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_pen' => 'integer',
        'serie' => 'string',
        'folio' => 'string',
        'fecha_timbrado' => 'datetime',
        'uuid' => 'string',
        'subtotal_factura' => 'decimal:2',
        'iva_factura' => 'decimal:2',
        'total_factura' => 'decimal:2',
        'XML' => 'string',
        'PDF' => 'string',
        'estatus' => 'string',
        'id_usoCFDI' => 'integer',
        'id_metodoPago' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_pen' => 'required|integer',
        /*'serie' => 'required|string|max:255',
        'folio' => 'required|string|max:255',*/
        'fecha_timbrado' => 'nullable',
        'uuid' => 'nullable|string|max:255',
        'subtotal_factura' => 'nullable|numeric',
        'iva_factura' => 'nullable|numeric',
        'total_factura' => 'nullable|numeric',
        'XML' => 'nullable|string',
        'PDF' => 'nullable|string',
        'estatus' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'id_usoCFDI' => 'nullable|integer',
        'id_metodoPago' => 'nullable|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idUsocfdi()
    {
        return $this->belongsTo(\App\Models\CatCfdiUso::class, 'id_usoCFDI');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idMetodopago()
    {
        return $this->belongsTo(\App\Models\CatMetodoPago::class, 'id_metodoPago');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idPen()
    {
        return $this->belongsTo(\App\Models\PensionesDet::class, 'id_pen');
    }
}
