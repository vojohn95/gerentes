<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class MetodoPago
 * @package App\Models
 * @version June 2, 2021, 9:50 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $facturaPensiones
 * @property integer $claveSat
 * @property string $descricion
 */
class MetodoPago extends Model
{

    public $table = 'cat_Metodopago';

    public $connection = "mysql2";

    public $fillable = [
        'clave',
        'Descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'clave' => 'string',
        'Descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'clave' => 'nullable|string',
        'Descripcion' => 'nullable|string|max:30',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function facturaPensiones()
    {
        return $this->hasMany(\App\Models\FacturaPensione::class, 'id_metodoPago');
    }
}
