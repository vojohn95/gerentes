<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ImgIngresos;
use Faker\Generator as Faker;

$factory->define(ImgIngresos::class, function (Faker $faker) {

    return [
        'id_ingreso' => $faker->word,
        'foto' => $faker->text,
        'firma' => $faker->text
    ];
});
