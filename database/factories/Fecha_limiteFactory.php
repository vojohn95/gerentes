<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Fecha_limite;
use Faker\Generator as Faker;

$factory->define(Fecha_limite::class, function (Faker $faker) {

    return [
        'fecha' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
