<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EmpresaColect;
use Faker\Generator as Faker;

$factory->define(EmpresaColect::class, function (Faker $faker) {

    return [
        'empresa' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
