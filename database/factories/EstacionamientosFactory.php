<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Estacionamientos;
use Faker\Generator as Faker;

$factory->define(Estacionamientos::class, function (Faker $faker) {

    return [
        'id_marca' => $faker->word,
        'id_org' => $faker->word,
        'nombre' => $faker->word,
        'dist_regio' => $faker->word,
        'direccion' => $faker->word,
        'calle' => $faker->word,
        'no_ext' => $faker->word,
        'colonia' => $faker->word,
        'municipio' => $faker->word,
        'estado' => $faker->word,
        'pais' => $faker->word,
        'cp' => $faker->word,
        'latitud' => $faker->word,
        'longitud' => $faker->word,
        'Facturable' => $faker->word,
        'Automatico' => $faker->word,
        'cajones' => $faker->randomDigitNotNull,
        'folio' => $faker->randomDigitNotNull,
        'serie' => $faker->word,
        'correo' => $faker->word,
        'pensiones' => $faker->randomDigitNotNull,
        'cortesias' => $faker->randomDigitNotNull,
        'observaciones' => $faker->word,
        'escuela' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
