<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\catBanco;
use Faker\Generator as Faker;

$factory->define(catBanco::class, function (Faker $faker) {

    return [
        'id' => $faker->randomDigitNotNull,
        'banco' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
