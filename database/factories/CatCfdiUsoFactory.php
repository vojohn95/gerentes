<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CatCfdiUso;
use Faker\Generator as Faker;

$factory->define(CatCfdiUso::class, function (Faker $faker) {

    return [
        'claveSat' => $faker->word,
        'descricion' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
