<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pensions;
use Faker\Generator as Faker;

$factory->define(Pensions::class, function (Faker $faker) {

    return [
        'id_cliente' => $faker->word,
        'id_tipo_pen' => $faker->word,
        'id_tipo_pago' => $faker->word,
        'id_forma_pago' => $faker->word,
        'id_fecha_limite' => $faker->word,
        'num_pen' => $faker->randomDigitNotNull,
        'foto_comprobante' => $faker->text,
        'no_est' => $faker->randomDigitNotNull,
        'factura' => $faker->word,
        'costo_pension' => $faker->word,
        'recargos' => $faker->word,
        'venta_tarjeta' => $faker->word,
        'repo_tarjeta' => $faker->word,
        'impor_pago' => $faker->word,
        'mes_pago' => $faker->randomDigitNotNull,
        'cargado_por' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
