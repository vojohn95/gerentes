<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Detalle;
use Faker\Generator as Faker;

$factory->define(Detalle::class, function (Faker $faker) {

    return [
        'id_estacionamiento' => $faker->randomDigitNotNull,
        'montoPension' => $faker->randomDigitNotNull,
        'tipoPension' => $faker->word,
        'contrato' => $faker->text,
        'solicitudContrato' => $faker->text,
        'comprobanteDomicilio' => $faker->text,
        'ine' => $faker->text,
        'licencia' => $faker->text,
        'rfc' => $faker->text,
        'tarjetaCirculacion' => $faker->text,
        'noTarjeta' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'fecha_limite' => $faker->word
    ];
});
