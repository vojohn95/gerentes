<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TiposPen;
use Faker\Generator as Faker;

$factory->define(TiposPen::class, function (Faker $faker) {

    return [
        'tipo' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
